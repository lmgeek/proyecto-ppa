<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promotion extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'description', 'active', 'delete'
    ];

    public function getPromotionArticle($id)
    {
        $promotion = Promotion::find($id);
        if(isset($promotion->name)){
            $promotion_name = $promotion->name;
        }else{
            $promotion_name = 0;
        }
        return $promotion_name;
    }
}
