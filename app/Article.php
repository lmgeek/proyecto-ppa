<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class Article extends Model
{

    use SoftDeletes;

    protected $table = 'articles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'quantity', 'description', 'new', 'id_promition', 'active', 'due_date'
    ];

    protected $dates = ['deleted_at'];

    public function getPrimaryPhotoAttribute() {
        return \App\Photo::getArticlesPrimaryPhoto($this->id);
    }
}
