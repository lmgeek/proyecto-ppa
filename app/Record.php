<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Record extends Model
{
    protected $table = "users_records";


    public static function addRecord($id_article)
    {
        if(isset(Auth::guard('web')->user()->id)){
            $id = Auth::guard('web')->user()->id;
            $record = new Record();
            $exist = Record::where('id_user', $id)->where('id_article', $id_article)->first();
            if($exist === null){
                $record->id_user = $id;
                $record->id_article = $id_article;

                $record->save();
            }
        }
    }
}
