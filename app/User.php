<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use SoftDeletes;

    use Notifiable;
    protected $guard = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($value)
    {
        if (!empty($value)){
            $this->attributes['password'] = \Hash::make($value);
        }
    }

    public function setHashAttribute($value)
    {
        if (!empty($value)){
            $this->attributes['password'] = \Hash::make($value);
        }
    }

    public function getFullNameAttribute() {
        return ucfirst($this->name) . ' ' . ucfirst($this->last_name);
    }

    public function getFullAddressAttribute() {
        if($this->zip_code != null){
            return ucfirst($this->address) . ', ' . ucfirst($this->zip_code);
        }else{
            return "";
        }
    }
}
