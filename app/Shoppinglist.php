<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shoppinglist extends Model
{
    protected $table = 'shoppinglist';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_article', 'id_user', 'quantity'
    ];

    public static function checkItemsAvailability($items)
    {
        $mensaje = array();
        foreach ($items as $item){
            $availability = Article::where('id', $item->id_article)->select('available')->first();
            if($availability->available <= 0 ){
                $_item = Shoppinglist::find($item->itemid);
                $mensaje[$item->id_article] = $item->name;
                $_item->delete();
            }
        }

        return $mensaje;
    }
}
