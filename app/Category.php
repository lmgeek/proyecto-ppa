<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'active', 'delete'
    ];

    public static function getCategoryIdByName($name){
        return Category::where('name', $name)->select('id')->first();
    }

    public static function getAllCategories(){
        return Category::all();
    }
}
