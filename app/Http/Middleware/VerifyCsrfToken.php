<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'save/article',
        'save/category',
        'save/subcategory',
        'save/transport',
        'save/user',
        'adminxxs/login',
        'login',
        'updated/list',
        'usuario/userphoto',
        'addtocar',
        'registerPurchases',
        'removefromcar',
        'changequantityitem',
        'adminxxs/loadmore',
        'adminxxs/save/config/layout'
    ];
}
