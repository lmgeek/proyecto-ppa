<?php

namespace App\Http\Controllers;

use App\Article;
use App\Orders;
use App\Purchase;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $id = Auth::guard('web')->user()->id;
        $records = DB::table('users_records as UR')
            ->join('articles as A', 'UR.id_article', '=', 'A.id')
            ->where('UR.id_user', $id)
            ->orderBy('UR.created_at','desc')
            ->limit(9)
            ->get();
        $user = Auth::guard('web')->user();
        return view('users/profile/profile', compact('records', 'user'));
    }

    public function purchasesShow()
    {
        $id = Auth::guard('web')->user()->id;
        $orders = Orders::where('id_user', $id)->orderBy('id', 'desc')->get();
        $purchases = DB::table('users_purchases as UP')
            ->join('articles as A', 'UP.id_article', '=', 'A.id')
            ->where('UP.id_user', $id)
            ->orderBy('UP.created_at', 'desc')
            ->select('UP.*', 'UP.created_at as created', 'A.*')
            ->get();


        return view('users/profile/purchases', compact('purchases', 'orders'));
    }

    public function recordsShow()
    {
        $id = Auth::guard('web')->user()->id;
        $records = DB::table('users_records as UR')
            ->join('articles as A', 'UR.id_article', '=', 'A.id')
            ->where('UR.id_user', $id)
            ->orderBy('UR.created_at','desc')
            ->select('UR.*', 'UR.created_at as created', 'A.*')
            ->get();
        $user = Auth::guard('web')->user();
        return view('users/profile/records', compact('records', 'user'));
    }

    public function shoppingListShow()
    {
        $id = Auth::guard('web')->user()->id;
        $articles = DB::table('shoppinglist as SL')
            ->join('articles as A', 'SL.id_article', '=', 'A.id')
            ->where('SL.id_user', $id)
            ->select('SL.id as itemid', 'SL.*', 'SL.created_at as created', 'A.*', 'SL.quantity as cantidad')
            ->get();
        $cant_articles = DB::table('shoppinglist as SL')
            ->join('articles as A', 'SL.id_article', '=', 'A.id')
            ->where('SL.id_user', $id)
            ->select('SL.*', 'SL.created_at as created', 'A.*')
            ->count();
        $totalPrice = 0;
        $user = Auth::guard('web')->user();
        return view('users/profile/shoppinglist', compact('articles', 'user', 'cant_articles', 'totalPrice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $id = Auth::guard('web')->user()->id;
        $user = User::find($id);

        return view('users/profile/config', compact('user'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if ($request->input('name') != "") {
            $user->name = $request->input('name');
        }
        if ($request->input('lastname') != "") {
            $user->lastname = $request->input('lastname');
        }
        if ($request->input('dni') != "") {
            $user->dni = $request->input('dni');
        }
        if ($request->input('birthday') != "") {
            $user->birthday = $request->input('birthday');
        }
        if ($request->input('address') != "") {
            $user->address = $request->input('address');
        }
        if ($request->input('zip_code') != "") {
            $user->zip_code = $request->input('zip_code');
        }
        $user->update();
        return view('users/profile/config', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updatePhotoUser(Request $request)
    {
        $user = Auth::guard('web')->user();
        $_photo = $request->file('croppedImage');
        $photoname = uniqid() . time() . '.jpg';

        $img = Image::make($_photo->getRealPath());
        $img->stream();
        Storage::disk('local')->put('public/userphotos' . '/' . $photoname, $img, 'public');
        $user->photo = $photoname;
        $user->update();

    }

    public function getProfilePicture(){

    }

}
