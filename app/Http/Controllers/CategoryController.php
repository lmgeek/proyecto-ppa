<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::paginate(20);
        return view('admin.categories.categories', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.categories.category");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category();
        $category->name = $request->input('title');
        $category->description = $request->input('description');
        $category->active = $request->input('active');

        $category->save();
        return redirect('/adminxxs/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $category = Category::find($id);
        return view('admin.categories.category', compact('category', 'edit'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->name = $request->input('title');
        $category->description = $request->input('description');
        $category->active = $request->input('active');

        $category->save();

        return redirect('adminxxs/categories')->with('successMsg', 'Se actulizo exitosamente la categoria');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroy($id);
        return redirect('/adminxxs/categories')->with('status', 'Categoría eliminada correctamente');;
    }

    public function deactive($id)
    {
        $category = Category::find($id);
        $category->active = 0;
        $category->update();
        return redirect('/adminxxs/categories')->with('status', 'Categoría desactivada correctamente');
    }

    public function active($id)
    {
        $category = Category::find($id);
        $category->active = 1;
        $category->update();
        return redirect('/adminxxs/categories')->with('status', 'Categoría activada correctamente');;
    }
}
