<?php

namespace App\Http\Controllers;

use App\Orders;
use App\Purchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Orders::all();
        foreach ($orders as $order) {
            $items[$order->ordernumber] = DB::table('users_purchases as UP')
                ->join('articles as A', 'UP.id_article', '=', 'A.id')
                ->where('UP.ordernumber', $order->ordernumber)
                ->orderBy('UP.created_at', 'desc')
                ->orderBy('UP.ordernumber', 'desc')
                ->select('UP.*', 'UP.created_at as created', 'A.*', 'A.id as id_article')
                ->get();
        }
        return view('admin.orders.orders', compact('orders', 'items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
