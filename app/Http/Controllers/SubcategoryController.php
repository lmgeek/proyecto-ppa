<?php

namespace App\Http\Controllers;

use App\Category;
use App\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategories = Subcategory::paginate(20);
        $categories = Category::all();

        return view('admin.subcategories.subcategories', compact('subcategories', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.subcategories.subcategory', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subcategory = new Subcategory();
        $subcategory->name = $request->input('title');
        $subcategory->description = $request->input('description');
        $subcategory->active = $request->input('active');
        $subcategory->save();

        DB::table('categories_subcategories')->insert([
            'id_category' => $request->input('category'),
            'id_subcategory' => $subcategory->id,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime(),
        ]);

        return redirect('/adminxxs/subcategories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $subcategory = Subcategory::find($id);
        $categories = Category::all();
        $c_subcategory = DB::table('categories_subcategories')->where('id_subcategory', $id)->get();
        return view('admin.subcategories.subcategory', compact('subcategory', 'edit', 'c_subcategory', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subcategory = Subcategory::find($id);
        $subcategory->name = $request->input('title');
        $subcategory->description = $request->input('description');
        $subcategory->active = $request->input('active');
        $subcategory->save();
         DB::table('categories_subcategories')->where('id_subcategory', $id)->update([
            'id_category' => $request->input('category'),
            'updated_at' => new \DateTime(),
        ]);
        return redirect('adminxxs/subcategories')->with('successMsg', 'Se actulizo exitosamente la categoria');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Subcategory::destroy($id);
        return redirect('/adminxxs/subcategories')->with('status', 'Subcategoría eliminada correctamente');;
    }

    public function deactive($id)
    {
        $subcategory = Subcategory::find($id);
        $subcategory->active = 0;
        $subcategory->update();
        return redirect('/adminxxs/subcategories')->with('status', 'Subcategoría desactivada correctamente');
    }

    public function active($id)
    {
        $subcategory = Subcategory::find($id);
        $subcategory->active = 1;
        $subcategory->update();
        return redirect('/adminxxs/subcategories')->with('status', 'Subcategoría activada correctamente');;
    }
}
