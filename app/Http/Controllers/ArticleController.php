<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Article;
use App\Category;
use App\Photo;
use App\Promotion;
use App\Subcategory;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::paginate(20);
        return view('admin.articles.articles', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Category();
        $subcategory = new Subcategory();
        $promotion = new Promotion();
        $categories = $category::all();
        $subcategories = $subcategory::all();
        $promotions = $promotion::all();

        return view("admin.articles.article", compact('categories', 'subcategories', 'promotions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article = new Article();
        $article->name = $request->input('title');
        $article->model = $request->input('model');
        $article->price = $request->input('price');
        $article->quantity = 100;
        $article->description = $request->input('description');
        $article->new = 1;
        $article->id_promotion = $request->input('id_promotion');
        $article->active = 1;
        $article->due_date = $request->input('due_date');
        $article->save();

        for ($i = 1; $i < 6; $i++) {
            $photo = new Photo();
            $photo->id_article = $article->id;
            if ($request->hasFile("img$i")) {
                $photo->order = $i;
                $_photo = $request->file("img$i");
                $photoname = uniqid() . time() . '.' . $_photo->getClientOriginalExtension();

                $img = Image::make($_photo->getRealPath());
                $img->resize(800, 800, function ($constrain) {
                    $constrain->aspectRatio();
                });
                $img->stream();
                Storage::disk('local')->put('articles/photos' . '/' . $photoname, $img, 'public');
                $photo->photo = $photoname;
                $photo->save();
            }
        }
        return redirect('/adminxxs/articles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::find($id);
        $category = DB::table("articles_categories")->where("id_article", $id)->first();
        $subcategory = DB::table("articles_subcategories")->where("id_article", $id)->first();
        $photos = Photo::getArticlePhotos($id);
        $principal_photo = (!empty($photos[0]) && $photos[0]->photo != null) ? $photos[0]->photo : "";
        $articlesRelatedCategory = $this->ArticlesCategoryRelated($category->id_category);
        return view('articles.article', compact('article', 'category', 'subcategory', 'photos', 'articlesRelatedCategory', 'principal_photo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $categories = Category::all();
        $subcategories = Subcategory::all();
        $promotions = Promotion::all();
        $article = Article::find($id);
        $category = DB::table("articles_categories")->where("id_article", $id)->first();
        $subcategory = DB::table("articles_subcategories")->where("id_article", $id)->first();
        $photos = DB::table("photos")->where("id_article", $id)->get();
        return view('admin.articles.article', compact('article', 'edit', 'categories', 'subcategories', 'promotions', 'category', 'subcategory', 'photos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article = Article::find($id);
        $article->name = $request->input('title');
        $article->model = $request->input('model');
        $article->price = $request->input('price');
        $article->quantity = 100;
        $article->description = $request->input('description');
        $article->new = 1;
        $article->id_promotion = $request->input('id_promotion');
        $article->active = 1;
        $article->due_date = $request->input('due_date');
        $article->save();

        for ($i = 1; $i < 6; $i++) {
            $photo = new Photo();
            $photo->id_article = $id;
            if ($request->hasFile("img$i")) {
                $photo->order = $i;
                $_photo = $request->file("img$i");
                if ($_photo != null) {
                    DB::table("photos")->where("id_article", $id)->where('order', $i)->delete();
                }
                $photoname = uniqid() . time() . '.' . $_photo->getClientOriginalExtension();

                $img = Image::make($_photo->getRealPath());
                $img->resize(800, 800, function ($constrain) {
                    $constrain->aspectRatio();
                });
                $img->stream();
                Storage::disk('public')->put('articles/photos' . '/' . $photoname, $img, 'public');
                $photo->photo = $photoname;
                $photo->save();
            }
        }
        return redirect('/adminxxs/articles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Article::destroy($id);
        return redirect('/adminxxs/articles')->with('status', 'Articulo eliminado correctamente');
    }

    public function deactive($id)
    {
        $article = Article::find($id);
        $article->active = 0;
        $article->update();
        return redirect('/adminxxs/articles')->with('status', 'Articulo desactivado correctamente');
    }

    public function active($id)
    {
        $article = Article::find($id);
        $article->active = 1;
        $article->update();
        return redirect('/adminxxs/articles')->with('status', 'Articulo activado correctamente');;
    }

    public function search(Request $request)
    {

        $word = ($request->input('search') != null) ? $request->input('search') : '';
        $order = ($request->input('order') != null) ? $request->input('order') : 'new';
        $minprice = ($request->input('minprice') != null) ? $request->input('minprice') : 0;
        $maxprice = ($request->input('maxprice') != null) ? $request->input('maxprice') : 0;
        $category = Category::getCategoryIdByName($request->input('categoria'));
        $subcategory = Subcategory::getSubcategoryIdByName($request->input('subcategoria'));
        $sword = "%$word%";
        $id_category = ($category != null) ? $category->id : 0;
        $id_subcategory = ($subcategory != null) ? $subcategory->id : 0;
        $native_url = $request->url();

//       Url para la busqueda
        $url = $request->fullUrl();
        $rcategory = url()->current() . '?' . http_build_query($request->except('categoria'));
        $rsubcategory = url()->current() . '?' . http_build_query($request->except('subcategoria'));
        $rminprice = url()->current() . '?' . http_build_query($request->except('minprice'));
        $rmaxprice = url()->current() . '?' . http_build_query($request->except('maxprice'));
        $rtwoprices = url()->current() . '?' . http_build_query($request->except('minprice', 'maxprice'));
        $rorder = url()->current() . '?' . http_build_query($request->except('order'));

//        Listado de Articulos
        $articles = $this->getArticlesSearch($sword, $id_category, $id_subcategory, $minprice, $maxprice, $order);
        $categories = $this->getCategoriesSearch($sword);
        $subcategories = $this->getSubcategoriesSearch($sword);

        $maxprice = DB::table('articles')->max('price');
        $minprice = DB::table('articles')->min('price');
        $mediaprice = round(($maxprice + $minprice) / 2);
        $quaterprice = round(($maxprice + $minprice) / 4);

        return view('search.search', compact('articles', 'word', 'count', 'categories', 'subcategories', 'maxprice', 'minprice', 'mediaprice', 'quaterprice', 'url', 'rcategory', 'rsubcategory', 'rminprice', 'rmaxprice', 'rorder', 'rtwoprices'));
    }


    public function getArticlesSearch($keyword = "", $id_category = 0, $id_subcategory = 0, $minprice = 0, $maxprice = 0, $order = 'new')
    {

        $desc = ($order == 'minprice') ? 'ASC' : 'DESC';
        $order = ($order == 'minprice' || $order == 'maxprice') ? 'articles.price' : $order;
        $articles = DB::table('articles')
            ->join('articles_categories', 'articles.id', '=', 'articles_categories.id_article');

        if ($keyword != "") {
            $articles = $articles->where('articles.name', 'like', $keyword);
        }

        if ($id_category != 0) {
            $articles = $articles->where('articles_categories.id_category', $id_category);
        }
        if ($id_subcategory != 0) {
            $articles = $articles->where('articles_categories.id_subcategory', $id_subcategory);
        }
        if ($minprice != 0 && $maxprice != 0) {
            $articles = $articles->whereBetween('articles.price', [$minprice, $maxprice]);
        } else if ($minprice != 0) {
            $articles = $articles->where('articles.price', '>', $minprice);
        }
        $articles = $articles->orderBy($order, $desc)->paginate(21)->appends(Input::except('page'));

        return $articles;
    }

    public function getArticlesSearchMinMax($keyword, $minprice, $maxprice, $id_category)
    {
        $articles = DB::table('articles')
            ->join('articles_categories', 'articles.id', '=', 'articles_categories.id_article')
            ->orwhere('articles.name', 'like', $keyword)
            ->orwhere('articles.description', 'like', $keyword)
            ->orwhere('articles_categories.id_category', $id_category)
            ->orderBy('new', 'DESC')->paginate(21)
            ->appends(Input::except('page'));

        return $articles;
    }

    public function getCategoriesSearch($keyword)
    {
        $categories = DB::table('articles')
            ->leftjoin('articles_categories', 'articles.id', '=', 'articles_categories.id_article')
            ->leftjoin('categories', 'articles_categories.id_category', '=', 'categories.id')
            ->orwhere('articles.name', 'like', $keyword)
            ->orwhere('articles.description', 'like', $keyword)
            ->where('categories.name', '<>', '')
            ->orderBy('articles.new', 'DESC')
            ->select('categories.name as cat')
            ->distinct('categories.id')
            ->get();

        return $categories;
    }

    public function getSubcategoriesSearch($keyword)
    {
        $subcategories = DB::table('articles')
            ->leftjoin('articles_categories', 'articles.id', '=', 'articles_categories.id_article')
            ->leftjoin('subcategories', 'articles_categories.id_subcategory', '=', 'subcategories.id')
            ->orwhere('articles.name', 'like', $keyword)
            ->orwhere('articles.description', 'like', $keyword)
            ->where('subcategories.name', '<>', '')
            ->orderBy('articles.new', 'DESC')
            ->select('subcategories.name as sub')
            ->distinct('subcategories.id')
            ->get();


        return $subcategories;
    }

    public function ArticlesCategoryRelated($id_category)
    {
        $articles = DB::table('articles')
            ->join('articles_categories', 'articles.id', '=', 'articles_categories.id_article')
            ->where('articles_categories.id_category', $id_category)
            ->orderBy('new', 'DESC')
            ->limit(4)
            ->get();
        return $articles;
    }
}
