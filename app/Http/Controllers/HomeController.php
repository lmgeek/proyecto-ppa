<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticlesIndex;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = $this->getArticlesIndexByPosition(1);
        $topalone = Article::orderBy('new', 'DESC')->where('active', '1')->offset(8)->first();
        $articles2 = $this->getArticlesIndexByPosition(3);
        return view('welcome', compact('articles', 'topalone', 'articles2'));
    }

    public function changeLayout()
    {
        $articles_p1 = $this->getArticlesIndexByPosition(1);
        $articles_p2 = $this->getArticlesIndexByPosition(2);
        $articles_p3 = $this->getArticlesIndexByPosition(3);
        $_articles = Article::where('active', '1')->limit(20)->get();
        return view('admin.layout.config', compact('articles_p1', 'articles_p2', 'articles_p3', '_articles'));
    }

    public function addMoreItems(Request $request)
    {
        $offset = $request->input('offset');
        $articles = Article::where('active', '1')->skip($offset)->take(8)->get();
        return json_encode($articles);
    }

    /**
     * @param Request $request
     */
    public function saveArticleIndex(Request $request)
    {
        $array = $request->input('arreglos');
//        dd($array);
        if ($array != null) {
            $this->clearArticlesIndex();
            foreach ($array as $key => $item_p) {
                if ($key == 0) {
                    $this->saveArticleIndexPosition($item_p, 1);
                } elseif ($key == 1) {
                    $this->saveArticleIndexPosition($item_p, 2);
                } elseif ($key == 2) {
                    $this->saveArticleIndexPosition($item_p, 3);
                }
            }
        }
    }

    /**
     * @param $item_p
     * @param $position
     */
    private function saveArticleIndexPosition($item_p, $position)
    {
        foreach ($item_p as $item){
            $add = new ArticlesIndex();
            $add->id_article = $item;
            $add->position = $position;
            $add->save();
        }
    }


    /**
     * @param $position
     * @return mixed
     */
    public function getArticlesIndexByPosition($position)
    {
        return DB::table('articles as a')->join('articles_index as ai', 'a.id', '=', 'ai.id_article')->where('a.active', '1')->where('ai.position', $position)->get();
    }
    /**
     *
     */
    private function clearArticlesIndex()
    {
        ArticlesIndex::truncate();
    }
}
