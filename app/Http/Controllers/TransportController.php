<?php

namespace App\Http\Controllers;

use App\Transport;
use Illuminate\Http\Request;

class TransportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transports = Transport::paginate(20);
        return view('admin.transport.transports', compact('transports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.transport.transport');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transport = new Transport();
        $transport->name = $request->input('title');
        $transport->amount = $request->input('carga');
        $transport->description = $request->input('description');
        $transport->active = $request->input('active');
        $transport->save();

        return redirect('/adminxxs/transports');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $transport = Transport::find($id);
        return view('admin.transport.transport', compact('transport', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transport = Transport::find($id);
        $transport->name = $request->input('title');
        $transport->amount = $request->input('carga');
        $transport->description = $request->input('description');
        $transport->active = $request->input('active');
        $transport->save();

        return redirect('/adminxxs/transports')->with('successMsg', 'Se actulizo exitosamente la categoria');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Transport::destroy($id);
        return redirect('/adminxxs/transports')->with('status', 'Transporte eliminado correctamente');;
    }

    public function deactive($id)
    {
        $transport = Transport::find($id);
        $transport->active = 0;
        $transport->update();
        return redirect('/adminxxs/transports')->with('status', 'Transporte desactivado correctamente');
    }

    public function active($id)
    {
        $transport = Transport::find($id);
        $transport->active = 1;
        $transport->update();
        return redirect('/adminxxs/transports')->with('status', 'Transporte activado correctamente');;
    }
}
