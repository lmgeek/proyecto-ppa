<?php

namespace App\Http\Controllers;

use App\Promotion;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotions = Promotion::paginate(20);
        return view('admin.promotions.promotions', compact('promotions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.promotions.promotion');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $promotion = new Promotion();
        $promotion->name = $request->input('title');
        $promotion->description = $request->input('description');
        $promotion->active = $request->input('active');
        $promotion->save();

        return redirect('/adminxxs/promotions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $promotion = Promotion::find($id);
        return view('admin.promotions.promotion', compact('promotion', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $promotion = Promotion::find($id);
        $promotion->name = $request->input('title');
        $promotion->description = $request->input('description');
        $promotion->active = $request->input('active');
        $promotion->save();

        return redirect('/adminxxs/promotions')->with('successMsg', 'Se actualizo exitosamente la promocion');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Promotion::destroy($id);
        return redirect('/adminxxs/promotions')->with('status', 'Promoción eliminada correctamente');;
    }

    public function deactive($id)
    {
        $promotion = Promotion::find($id);
        $promotion->active = 0;
        $promotion->update();
        return redirect('/adminxxs/promotions')->with('status', 'Promoción desactivada correctamente');
    }

    public function active($id)
    {
        $promotion = Promotion::find($id);
        $promotion->active = 1;
        $promotion->update();
        return redirect('/adminxxs/promotions')->with('status', 'Promoción activada correctamente');;
    }
}
