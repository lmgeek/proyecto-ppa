<?php

namespace App\Http\Controllers\AdminAuth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $guard = 'admin';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email','password');
        if (Auth::guard('admin')->attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('adminxxs/panel');
        }

        return view('admin.login')->with('errorMsg', 'Las credenciales son incorrectas, pruebe nuevamente');
    }

    public function loginPage()
    {
        if (view()->exists('admin.login')) {
            return view('admin.login');
        }

        return view('admin.login');
    }

    public function registrationPage()
    {
        return view('admin.auth.register');
    }

    public function logOut(){

        if(Auth::guard('admin')->logout()){
            return view('admin.login');
        }

        return view('admin.login');
    }
}
