<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $guard = 'web';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logOut');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->intended('usuario/perfil');
        }
        return view('login.login')->with('errorMsg', 'Las credenciales son incorrectas, pruebe nuevamente');
    }

    public function loginPage()
    {
        if (view()->exists('login.login')) {
            return  view('login.login');
        }

        return view('login.login');
    }

    public function registrationPage()
    {
        return view('register.register');
    }

    public function logOut(){
        if(Auth::guard('web')->logout()) {
            return redirect()->to('login.login');
        }
        return view('login.login');
    }
}
