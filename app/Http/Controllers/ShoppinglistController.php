<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shoppinglist;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ShoppinglistController extends Controller
{
    public function setValueList(Request $request)
    {
        $id_item = $request->input('id');
        $id_user = Auth::guard('web')->user()->id;
        $value = $request->input('value');
        $listItem = Shoppinglist::find($id_item);
        $listItem->quantity = $value;
        $listItem->update();

        $articles = DB::table('shoppinglist as SL')
            ->join('articles as A', 'SL.id_article', '=', 'A.id')
            ->where('SL.id_user', $id_user)
            ->select('SL.id as itemid', 'SL.*', 'SL.created_at as created', 'A.*', 'SL.quantity as cantidad')
            ->get();
        $cant_articles = DB::table('shoppinglist as SL')
            ->join('articles as A', 'SL.id_article', '=', 'A.id')
            ->where('SL.id_user', $id_user)
            ->select('SL.*', 'SL.created_at as created', 'A.*')
            ->sum('SL.quantity');

        $totalPrice = 0;

        return view('users.profile.partials.shoppinglisttable', compact('articles', 'user', 'cant_articles', 'totalPrice'));
    }

    public function addToShoppingList(Request $request)
    {
        if (isset(Auth::guard('web')->user()->id)) {
            $id_article = $request->input('id_article');
            $quantity = $request->input('quantity');
            $id = Auth::guard('web')->user()->id;
            $Shoppinglist = new Shoppinglist();
            $exist = Shoppinglist::where('id_user', $id)->where('id_article', $id_article)->first();
            if ($exist === null) {
                $Shoppinglist->id_user = $id;
                $Shoppinglist->id_article = $id_article;
                $Shoppinglist->quantity = $quantity;
                $Shoppinglist->save();
            } else {
                $exist->quantity += $quantity;
                $exist->save();
            }
        }
        return "Se agregó el artículo a su carrito de compra.";
    }

    public function RemoveFromShoppingList(Request $request)
    {
        if (isset(Auth::guard('web')->user()->id)) {
            $id_article = $request->input('id_article');
            $id = Auth::guard('web')->user()->id;
            $exist = Shoppinglist::where('id_user', $id)->where('id_article', $id_article)->first();
            if ($exist === null) {
                return "Hubo en error al eliminar el artículo";
            } else {
                $exist->delete();
            }
        }
        return "Se elimino el artículo a su carrito de compra.";
    }

    public function ChangeQuantityItem(Request $request)
    {
        $id_user = Auth::guard('web')->user()->id;
        $id_article = $request->input('article');
        $quantity = $request->input('quantity');

        $shoppinglist = Shoppinglist::where('id_user', $id_user)->where('id_article', $id_article)->first();
        $shoppinglist->quantity = $quantity;

        if ($shoppinglist->update()) {
            return 'cambiado';
        } else {
            return "";
        }
    }

}
