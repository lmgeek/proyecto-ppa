<?php

namespace App\Http\Controllers;

use App\Article;
use App\Orders;
use App\Purchase;
use App\Shoppinglist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CheckoutController extends Controller
{

    public function showOnShoppinglist()
    {
        $id = Auth::guard('web')->user()->id;

        $articles = DB::table('shoppinglist as SL')
            ->join('articles as A', 'SL.id_article', '=', 'A.id')
            ->where('SL.id_user', $id)
            ->select('SL.id as itemid', 'SL.*', 'SL.created_at as created', 'A.*', 'SL.quantity as cantidad')
            ->get();

        $years = date("Y") + 20;
        $single = 0;
        $months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

        return view('checkout.checkout', compact('articles', 'years', 'months', 'single'));

    }

    public function showOnSingle($id_article)
    {
        $id = Auth::guard('web')->user()->id;

        $articles = DB::table('articles')->where('id', $id_article)->select('*', 'id as id_article')->get();

        $years = date("Y") + 20;
        $months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        $single = 1;
        return view('checkout.checkout', compact('articles', 'years', 'months' , 'single'));

    }

    public function showBuyAgain($ordernumber)
    {
        $articles = DB::table('users_purchases as UP')
            ->join('articles as A', 'UP.id_article', '=', 'A.id')
            ->where('UP.ordernumber', $ordernumber)
            ->select('UP.*', 'UP.created_at as created', 'A.*', 'A.id as id_article')
            ->get();
        $id = Auth::guard('web')->user()->id;
        foreach ($articles as $article) {
            $Shoppinglist = new Shoppinglist();
            $exist = Shoppinglist::where('id_user', $id)->where('id_article', $article->id_article)->first();
            if ($exist === null) {
                $Shoppinglist->id_user = $id;
                $Shoppinglist->id_article = $article->id_article;
                $Shoppinglist->quantity = $article->count;
                $Shoppinglist->save();
            } else {
                $exist->quantity += $article->count;
                $exist->save();
            }
        }

        return redirect("comprar");

    }


    public function registerPurchases(Request $request)
    {
        $id_user = Auth::guard('web')->user()->id;
        $articles = $request->input('articles');
        $single = $request->input('single');
        $transit = $this->checkAvailability($articles);
        if ($transit) {
            $order = $this->orderNumber();
            $orders = Orders::find($order);
            $orders->ordernumber = $order;
            $orders->update();
            foreach ($articles as $article) {
                $available = Article::find($article['id_article']);
                if ($article['cantidad'] != 0) {
                    if ($available->available >= $article['cantidad']) {
                        $available->available -= $article['cantidad'];
                    } else {
                        return "Cantindad invalidad";
                        break;
                    }
                } else {
                    if ($available->available >= $article['cantidad']) {
                        $available->available -= $article['cantidad'];
                    } else {
                        return "Cantindad invalidad";
                        break;
                    }
                }
                $available->update();
                $purchase = new Purchase();
                $purchase->id_article = $article['id_article'];
                $purchase->id_user = $id_user;
                if ($article['cantidad'] != 0) {
                    $purchase->count = $article['cantidad'];
                } else {
                    $purchase->count = $article['cantidad'];
                }
                $purchase->ordernumber = $order;
                $purchase->save();
            }
            $this->emptyCar($id_user, $single);
            return "Su compra ha sido procesada correctamente, será re direccionado en unos instantes a sus compras.";
        } else {
            return "Error";
        }
    }

    public function checkAvailability($articles)
    {
        $transit = true;
        foreach ($articles as $article) {
            $available = Article::find($article['id_article']);
            if ($article['cantidad'] != 0) {
                if ($available->available < $article['cantidad']) {
                    $transit = false;
                }
            }
        }
        return $transit;
    }

    public function orderNumber(){
        $order = new Orders();
        $order->id_user = Auth::guard('web')->user()->id;
        $order->save();
        return $order->id;

    }

    public function emptyCar($id_user, $single = 1){
        if($single == 0){
            Shoppinglist::where("id_user", $id_user)->delete();
        }
    }
}
