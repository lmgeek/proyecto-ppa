<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = 'orders';

    public function setOrdernumberAttribute($value)
    {
        if (!empty($value)){
            $length = strlen($value);
            $form = "AA-0000-0000-0000";
            $sub_form = substr($form, 0 , -$length);
            $this->attributes['ordernumber'] = $sub_form . $value;
        }
    }
}
