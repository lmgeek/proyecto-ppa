<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property  position
 */
class ArticlesIndex extends Model
{
    protected $table = "articles_index";
}
