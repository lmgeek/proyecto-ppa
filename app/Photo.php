<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Photo extends Model
{
    protected $table = 'photos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    public static function getArticlePhotos($id_article){
        return DB::table("photos")->where("id_article", $id_article)->get();
    }

    public static function getArticlesPrimaryPhoto($id_article){
        $primary_photo = DB::table("photos")->where("id_article", $id_article)->where("order", 1)->get();
        if(isset($primary_photo{0}->photo)){
            $primary_photo = $primary_photo{0}->photo;
        }else{
            $primary_photo =  "";
        }
        return $primary_photo;
    }
}
