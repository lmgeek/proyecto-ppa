<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $guard = 'admin';

    public function setPasswordAttribute($value)
    {
        if (!empty($value)){
            $this->attributes['password'] = \Hash::make($value);
        }
    }
}
