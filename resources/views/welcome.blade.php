@include('navbar/navbar')
<div class="container">
    <div class="row">
        <div class="col col-md-12 cover text-center bg-green">
            <h1 class="titutlo-cover1 _align-middle">Productos de primera necesidad</h1>
        </div>
    </div>
    <div class="row justify-content-md-center margin overlap">
        <div class="col col-10 offset-1 col-sm-10 col-md-8 offset-md-0">
            <div class="row justify-content-md-center">
                @foreach($articles as $article)
                    <?php $principal_photo = \App\Photo::getArticlesPrimaryPhoto($article->id);?>
                    <div class=" col col-12 col-sm-6 col-md-3 margin">
                        <div class="card">
                            <div class="card-img-top">
                                <span class="helper"></span>
                                <img src="@if($principal_photo != ""){{asset("uploads/articles/photos/".$principal_photo)}} @else {{ asset("img/picture.png") }} @endif"
                                     alt="Card image cap" @if($principal_photo == "") class="no-image" @endif>
                            </div>
                            @if($article->id_promotion != 0)
                                <?php $promotion = new \App\Promotion(); $promotion_name = $promotion->getPromotionArticle($article->id_promotion) ?>
                                <div class="tag-descuento"><span>{{  $promotion_name }}%</span></div>
                            @endif
                            @if($article->new == 1)
                                <div class="@if($article->id_promotion == 0) tag @else tag-nuevo @endif">
                                    <span>NUEVO</span>
                                </div>
                            @endif
                            <div class="card-body">

                                <a href="{{ url("producto/$article->id") }}"><p
                                            class="card-text">{{ $article->name }}</p>
                                </a>
                                <p class="card-title">$ {{ $article->price }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row my-2">
        <div class="col wrapper text-center">
        </div>
    </div>
    <div class="row justify-content-md-center margin">
        <div class="row col col-10 offset-1 col-sm-10 col-md-8 col-md-offset-2 offset-md-0 col-lg-8 ">
            <div class="col-12 col-sm-12 col-md-4 offset-md-0 col-lg-4 margin">
                <div class="card2">
                    <?php $principal_photo = \App\Photo::getArticlesPrimaryPhoto($topalone->id);?>
                    <div class="card-img-top2">
                        <span class="helper"></span><img
                                src="@if($principal_photo != ""){{asset("uploads/articles/photos/".$principal_photo)}} @else {{ asset("img/picture.png") }} @endif"
                                alt="Card image cap" @if($principal_photo == "") class="no-image" @endif alt="">
                    </div>
                    <div class="card-body">
                        <a href="{{ url("producto/$topalone->id") }}"><p class="card-text">{{$topalone->name}}</p></a>
                        <h5 class="card-title">$ {{$topalone->price}}</h5>
                    </div>
                </div>
            </div>
            <div class="row col-sm-10 col-10 offset-1 col-md-8 offset-md-0 col-md-8 my-2">
                @foreach($articles2 as $article)
                    <?php $principal_photo = \App\Photo::getArticlesPrimaryPhoto($article->id);?>
                    <div class="col-12 col-sm-6 col-md-3 col-lg-3 my-2">
                        <div style="box-shadow:  0px 2px 2px 0px #cccccc; width: 100%">
                            <div class="img-card">
                                <span class="helper"></span>
                                <img src="@if($principal_photo != ""){{asset("uploads/articles/photos/".$principal_photo)}} @else {{ asset("img/picture.png") }} @endif"
                                     alt="Card image cap" @if($principal_photo == "") class="no-image" @endif alt="">
                            </div>
                            <div class="text-card">
                                <a href="{{ url("producto/$article->id") }}">
                                    <p class="">{{ $article->name }}</p>
                                </a>
                            </div>
                            <div class="text-card">
                                <a href="{{ url("producto/$article->id") }}">
                                    <p class="">$ {{ $article->price }}</p>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
</div>
@include('footer/footer')
</body>
<script>
    $(document).ready(function () {
        $('.carousel').carousel();
    });
</script>
</html>
