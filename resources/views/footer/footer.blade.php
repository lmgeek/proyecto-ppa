<footer class="justify-content-md-center">
    <div class="col-10 offset-1 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2">
        <div class="row">
            <div class="col-md-3 col-lg-3 margin">
                <ul class="">
                    <li class=""><h5>PRODUCTOS</h5></li>
                    <li class="">Ofertas</li>
                    <li class="">Novedades</li>
                    <li class="">Los mas vendidos</li>
                </ul>
            </div>
            <div class="col-md-3 col-lg-3 margin">
                <ul class="">
                    <li class=""><h5>NUESTRA EMPRESA</h5></li>
                    <li class="">Delivery</li>
                    <li class="">Terminos y condiciones de uso</li>
                    <li class="">Sobre nosotros</li>
                    <li class="">Secure payment</li>
                    <li class="">Contactenos</li>
                    <li class="">Mapa del sitio</li>
                </ul>
            </div>
            <div class="col-md-3 col-lg-3 margin">
                <ul class="">
                    <li class=""><h5>SU CUENTA</h5></li>
                    <li class="">Informacion personal</li>
                    <li class="">Pedidos</li>
                    <li class="">Factura por abono</li>
                    <li class="">Direcciones</li>
                </ul>
            </div>
            <div class="col-md-3 col-lg-3 margin">
                <ul class="">
                    <li class=""><h5>INFORMACION DE LA TIENDA</h5></li>
                    <li class="">El Salvador</li>
                    <li class="">Envienos un correo</li>
                    <li class="">sv@tiendasv.com</li>
                </ul>
            </div>

            <div class="row justify-content-md-center col-md-12">
                <div class="col-md-12 text-center mb-3">
                    <span>Copyright @ 2018</span>
                </div>
            </div>
        </div>
    </div>
</footer>