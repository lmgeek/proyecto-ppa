@include('head/head')
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light text-center justify-content-md-center col-12">
        <div class="col-2 col-md-2 col-lg-2">
            <a href="{{ url('/') }}">
                <img src="{{asset('img/logo/tiendasv.png')}}" alt="" class="logo">
            </a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse col-md-9 col-sm-10 align-content-center" id="navbarNavDropdown">
            <div class="container-fluid">
                <form class="col-md-7 col-sm-12 d-md-block d-none" style="padding-left: 5.5px;"
                      method="get"
                      action="{{ url('busqueda') }}">
                    <div class="input-group mb-1">
                        <input type="text" class="form-control" placeholder="Buscar por palabra clave" name="search"
                               autocomplete="off">
                        <div class="input-group-append">
                            <button class="input-group-text" id="basic-addon2" type="submit"><i
                                        class="fas fa-search"></i></button>
                        </div>
                    </div>
                </form>
                <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <ul class="navbar-nav mr-auto col-md-7 col-sm-8 myfont-11">
                            <li class="nav-item dropdown" data-toggle="dropdown" id="dropdown2">
                                <a class="nav-link d-md-block d-none" href="#" target="_blank">Categorías</a>
                            </li>
                            <?php $categories = \App\Category::getAllCategories(); ?>
                            <div class="dropdown-menu" role="menu">
                                @foreach($categories as $category)
                                    <a class="dropdown-item" href="{{ url('busqueda/?categoria='.$category->name) }}"
                                       style="z-index: 1">{{$category->name}}</a>
                                @endforeach
                            </div>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url("/") }}">Ofertas de la semana</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/usuario/historial')}}">Mi historial</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/usuario/compras')}}">Mis compras</a>
                            </li>
                            @if(Auth::user())
                                <li class="nav-item d-md-none">
                                    <a class="dropdown-item" href="{{ url('usuario/perfil/') }}">Mi cuenta</a>
                                </li>
                                <li class="nav-item d-md-none">
                                    <a class="dropdown-item" href="{{ url('logout/usuario') }}">Cerrar sesión</a>
                                </li>
                                <li class="nav-item d-md-none">
                                    <a class="nav-link" href="{{ url('/usuario/carrito') }}"> <i
                                                class="fas fa-shopping-cart"></i> Carrito</a>
                                </li>
                            @else
                                <li class="nav-item d-md-none">
                                    <a class="nav-link" href="{{ url('/login/usuario') }}"> <i class="fas fa-user"></i>
                                        Iniciar sección</a>
                                </li>
                            @endif
                        </ul>
                        <ul class="navbar-nav mr-auto col-md-4 col-sm-4 col-lg-4 justify-content-end myfont-11 d-md-flex d-none">
                            @if(Auth::user())
                                <li class="nav-item dropdown" data-toggle="dropdown" id="dropdown">
                                    <a class="nav-link" href="{{ url('/login/usuario') }}"> <i class="fas fa-user"></i>
                                        {{Auth::user()->name . " " . Auth::user()->lastname}}</a>
                                </li>
                                <div class="dropdown-menu" style="left: 150px;">
                                    <a class="dropdown-item" href="{{ url('usuario/perfil/') }}">Mi cuenta</a>
                                    <a class="dropdown-item" href="{{ url('logout/usuario') }}">Cerrar sesión</a>
                                </div>
                            @else

                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/login/usuario') }}"> <i class="fas fa-user"></i>
                                        Iniciar sección</a>
                                </li>
                            @endif
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/usuario/carrito') }}"> <i
                                            class="fas fa-shopping-cart"></i> Carrito</a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </nav>
    <div class="text-center col-12 d-md-none bg-light mb-2">
        <div class="row bg-light">
            <form class="col-10 offset-1 col-sm-10 col-md-8 offset-md-0 col-lg-8 offset-lg-0" style="padding-left: 5.5px;" method="get"
                  action="{{ url('busqueda') }}">
                <div class="input-group mb-1">
                    <input type="text" class="form-control" placeholder="Buscar por palabra clave" name="search"
                           autocomplete="off">
                    <div class="input-group-append">
                        <button class="input-group-text" id="basic-addon2" type="submit"><i
                                    class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.dropdown').dropdown();
        });
    </script>
</header>