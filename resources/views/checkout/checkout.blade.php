@include('head/head')
<?php $delete = \App\Shoppinglist::checkItemsAvailability($articles);?>
<div class="container-fluid">
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="row mt-3">
        <div class="col-12 col-sm-12 offset-md-3 col-md-3">
            <a href="{{ url('/') }}">
                <img src="{{asset('img/logo/tiendasv.png')}}" alt="" class="logo" width="125px">
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-5 mt-4 offset-md-3">
            <h4>Metodo de Pago</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-10 offset-1 col-sm-10 offset-sm-1 col-md-5 offset-md-3">
            <div class="row">
                <div class="col-12 mt-4" style="border: 1px solid #D4D4D4; border-radius: 3px; padding: 10px;">
                    <div class="col-12">
                        <img class="mr-2" src="{{ asset('img//payment/visamaster.png') }}" alt="" width="90px">
                        <img class="mr-2" src="{{ asset('img/payment/american.png') }}" alt="" width="50px">
                        <img class="mr-2" src="{{ asset('img//payment/paypal.png') }}" alt="" width="35px">
                    </div>
                    <div class="col-md-12">
                        <a href="#" class="myfont-11" id="ccard">Agregar metodo de pago</a>
                        <form action="" class="mt-2 animated" id="ccardform" style="display: none;">
                            <p class="myfont-11">Informacion de la tarjeta:</p>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4">
                                    <label for="" class="myfont-11">Nombre</label>
                                    <input name="namec" type="text" class="form-control myfont-10" id="namec"
                                           style="height: 30px;">
                                </div>
                                <div class="col-12 col-sm-12 col-md-4">
                                    <label for="" class="myfont-11">Numeros</label>
                                        <input name="numberc" type="text" class="form-control myfont-10" id="numberc"
                                               style="height: 30px; background: url('{{asset("img/cc/card.png")}}') no-repeat scroll 4px 1px; background-size: 25px 25px; padding-left:30px;" autocomplete="disable">
                                    <p class="log"></p>
                                </div>
                                <div class="col-12 col-sm-12 col-md-4">
                                    <label for="" class="myfont-11">Expiracion</label>
                                    <div class="row">
                                        <div class="col-6">
                                            <select name="mothnc" id="" class="form-control myfont-11" id="monthnc"
                                                    style="height: 30px;">
                                                @foreach($months as $month)
                                                    <option value="{{$month}}">{{$month}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-6" style="padding-left: 0px">
                                            <select name="yearc" id="" class="form-control myfont-11" id="yearc"
                                                    style="height: 30px;">
                                                @for($i = date('Y'); $i <= $years; $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mt-4" style="border: 1px solid #D4D4D4; border-radius: 3px; padding: 10px;">
                    <div class="col-md-12">
                        <a href="#" class="myfont-11" id="ccard">Entrega</a>
                        <form action="" class="mt-2">
                            <p class="myfont-11">Direccion</p>
                            <p class="myfont-11" id="address">{{ Auth::guard('web')->user()->full_address  }}</p>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 mt-4 mb-4" style="border: 1px solid #D4D4D4; border-radius: 3px;">
                    <div class="row">
                        <?php $totalPrice = 0;?>
                        @foreach($articles as $article)
                                <?php $principal_photo = \App\Photo::getArticlesPrimaryPhoto($article->id_article)?>
                            <div class="col-12 col-sm-12 col-md-12">
                                <div class="row" style="padding: 10px; padding-top: 15px;">
                                    <div class="col-12 col-sm-12 col-md-2 text-left">
                                        <img src="@if($principal_photo != ""){{asset("uploads/articles/photos/".$principal_photo)}} @else {{ asset("img/picture.png") }} @endif"
                                             alt="Card image cap"
                                             @if($principal_photo == "") class="no-image" @endif alt="" width="100px">
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-5 text-center-r">
                                        <strong class="myfont-11">{{$article->name}}</strong>
                                        <p class="myfont-10 mt-2" class=""
                                           @if(!isset($article->cantidad)) data-value="1" @endif>
                                            Cantidad: @if(isset($article->cantidad)){{ $article->cantidad }}@else
                                                1 @endif
                                            <a href="#" class="quantity myfont-10"> cambiar</a>
                                        </p>
                                        <div class="changequantity animated">
                                            <input type="text" class="form-control myfont-10" class="value">
                                            <input type="hidden" class="id_article" value="{{ $article->id }}">
                                            <p class="myfont-10 font-italic mt-1">
                                                <strong>Disponible: {{ $article->available }} </strong>
                                                <a href="#" style="font-size: 10px;" class="process ml-2">
                                                    cambiar
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-2">

                                    </div>
                                    <div class="col-12 col-sm-12 col-md-3 text-right">
                                        <strong class="blue">
                                            @if(isset($article->cantidad))
                                                $ {{ number_format((float)($article->price * $article->cantidad),2,'.','')}}
                                            @else
                                                $ {{ number_format((float)($article->price * 1),2,'.','')}}
                                            @endif
                                        </strong>
                                    </div>
                                </div>
                            </div>
                            <?php
                            if (isset($article->cantidad)) {
                                $totalPrice = $totalPrice + number_format((float)($article->price * $article->cantidad), 2, '.', '');
                            } else {
                                $totalPrice = $totalPrice + number_format((float)($article->price * 1), 2, '.', '');
                            }
                            ?>
                        @endforeach
                        <div class="col-md-5 offset-7 text-right my-4">
                            <h5>Total: </h5><strong
                                    class="blue">$ {{ number_format((float)($totalPrice),2,'.','')}} </strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-10 offset-1 col-sm-10 offset-sm-1 col-md-2 margin-r-buy">
            <div class="row">
                <div class="col-12 mt-4 text-center"
                     style="border: 1px solid #D4D4D4; border-radius: 3px; padding: 10px;">
                    <button class="btn btn-success myfont-11 col-12" id="purchases">Realizar compra</button>
                    <p class="myfont-10 mt-2">Al realizar la compra acepta los <a href="">Terminos y Condiciones</a> de
                        <strong>TiendsSv</strong></p>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        @if(!empty($delete))
            var mensaje = "Se ha eliminado el producto, ";
            @foreach($delete as $del)
                mensaje = mensaje + " {{ $del }} ";
            @endforeach
            mensaje = mensaje + ", por falta de disponibilidad";
            $.toaster({message: mensaje, title: 'Mensaje', priority: 'info', settings:{'timeout':2500,}});
            setTimeout(function () {
                location.reload();
            }, 2500);
        @endif
        $('#ccardform').hide();
        $('.changequantity').hide();
        var valid ="";
        $(document).on('click', '#ccard', function () {
            if ($('#ccardform').attr('data-show') == 'mostrar') {
                if (!$('#ccardform').hasClass('fadeOutUp')) {
                }
                $('#ccardform').fadeOut('slow');
                $('#ccardform').attr('data-show', 'nomostrar');
            } else {
                if (!$('#ccardform').hasClass('fadeInDown')) {
                }
                $('#ccardform').fadeIn('slow');
                $('#ccardform').attr('data-show', 'mostrar');
            }
        });

        $(".quantity").on('click', function () {
            $(this).hide();
            var changequantity = $(this).parent().next();
            if (changequantity.attr('data-show') == 'mostrar') {
                if (!changequantity.hasClass('fadeOutUp')) {
                }
                changequantity.fadeOut('slow');
                changequantity.attr('data-show', 'nomostrar');
            } else {
                if (!changequantity.hasClass('fadeInDown')) {
                }
                changequantity.fadeIn('slow');
                changequantity.attr('data-show', 'mostrar');
            }
        });

        $(".process").on('click', function () {
            var article = $(this).parent().prev().val();
            var quantity = $(this).parent().prev().prev().val();
            $.ajax({
                method: "POST",
                url: "{{url('changequantityitem')}}",
                data: {'article': article, 'quantity': quantity},
                success: function (data) {
                    console.log(data);
                    if (data == "cambiado") {
                        $.toaster({
                            message: "Item actualizado correctamente",
                            title: 'Mensaje',
                            priority: 'success',
                            settings: {'timeout': 2000}
                        });
                        setTimeout(function () {
                            location.reload();
                        }, 250);
                    } else {
                        $.toaster({
                            message: "El artículo no tiene disponibilidad suficiente",
                            title: 'Mensaje',
                            priority: 'danger',
                            settings: {'timeout': 10000}
                        });
                    }
                },
                error: function (response) {
                },
            });
        });


        $('#numberc').validateCreditCard(function(result) {
            var card_type = (result.card_type == null ? 'card' : result.card_type.name);
            valid = result.valid;
            console.log("/public/img/cc/"+card_type+".png");
            $('#numberc').css({'background':''});
            $('#numberc').css({'background':'url("img/cc/'+card_type+'.png")', "background-repeat": "no-repeat",
                "background-position": "4px 1px", "background-size": "25px 25px"});
        });

        $('#purchases').on('click', function () {
            var address = $('#address').text();
            var namec = $("#namec").val();
            var quantity = $('#quantity').attr('data-value');
            var array = "{{$articles}}";
            var articles = JSON.parse(array.replace(/&quot;/g, '"'));
            console.log(valid);
                if(valid){
                    if (address != "" && namec != "") {
                        $.ajax('{{url('registerPurchases')}}', {
                            method: "POST",
                            data: {'articles': articles, 'single': "{{ $single }}"},
                            success: function (response) {
                                if (response == "Error") {
                                    $.toaster({
                                        message: "Uno de los productos seleccionados, no tiene disponibilidad suficiente",
                                        title: 'Mensaje',
                                        priority: 'danger',
                                        settings: {'timeout': 10000}
                                    });
                                } else {
                                    $.toaster({
                                        message: response,
                                        title: 'Mensaje',
                                        priority: 'success',
                                        settings: {'timeout': 10000}
                                    });
                                    setTimeout(function () {
                                        window.location.replace("{{url('usuario/compras')}}");
                                    }, 2000);
                                }
                            },
                            error: function () {
                                $.toaster({
                                    message: "No se puedo agregar su compra al carrito",
                                    title: 'Mensaje',
                                    priority: 'danger'
                                });
                            },
                        });
                    } else if (namec == "" || numberc == "") {
                        $.toaster({
                            message: "Metodo de pago no acepto, verifique nuevamente los datos",
                            title: 'Mensaje',
                            priority: 'danger',
                            settings: {
                                timeout: 5000
                            },
                        });
                    } else if (address == "") {
                        $.toaster({
                            message: "Su direccion es invalidad, verifiquela en su perfil de usuario antes de continuar.",
                            title: 'Mensaje',
                            priority: 'danger',
                            settings: {
                                timeout: 5000
                            },
                        });
                    }
                }else{
                    $.toaster({
                        message: "Tarjeta de credito invalida, verifique su informacion.",
                        title: 'Mensaje',
                        priority: 'danger',
                        settings: {'timeout': 10000}
                    });
                }
        });
    });
</script>
