@include('navbar/navbar')
<div class="container">
    <div class="row offset-md-1 offset-lg-2 offset-0">
        <div class="col-12 col-md-12 col-lg-10 col-sm-12 busqueda">
            @if(count($articles) > 0)
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 col-lg-3 d-md-block d-none row">
                        <div class="filters">
                            <div class="mb-4">
                                <h3 class="light-blue">Filtros</h3>
                                @if(isset($_GET['categoria']))
                                    <a href="{{ "$rcategory" }}"><span class="badge badge-secondary">{{ app('request')->input('categoria')}}
                                            x</span></a>
                                @endif
                                @if(isset($_GET['subcategoria']))
                                    <a href="{{ "$rsubcategory" }}"><span class="badge badge-secondary">{{ app('request')->input('subcategoria')}}
                                            x</span></a>
                                @endif
                                @if(isset($_GET['minprice']))
                                    <a href="{{ "$rtwoprices" }}"><span class="badge badge-secondary">Precio
                                        x</span></a>
                                @endif
                            </div>
                            <h5>Categorias</h5>
                            <ul class="list-unstyled ml-4 myfont-11">
                                @foreach($categories as $key => $category)
                                    @if($key == 10)
                                        <?php break; ?>
                                    @endif

                                    <a href="{{ "$rcategory&categoria=$category->cat" }}">
                                        <li>{{ $category->cat }}</li>
                                    </a>
                                @endforeach
                            </ul>
                            <br>
                            <h5>Subategorias</h5>
                            <ul class="list-unstyled ml-4 myfont-11">
                                @foreach($subcategories as $key => $subcategory)
                                    @if($key == 10)
                                        <?php break; ?>
                                    @endif
                                    <a href="{{url("$rsubcategory&subcategoria=$subcategory->sub")}}">
                                        <li>{{ $subcategory->sub }}</li>
                                    </a>
                                @endforeach
                            </ul>
                            <br>
                            <h5>Precio</h5>
                            <ul class="list-unstyled ml-4 myfont-11">
                                <a href="{{ "$rtwoprices&minprice=$minprice&maxprice=$quaterprice" }}">
                                    <li>{{$minprice}}$ a {{$quaterprice}}$</li>
                                </a>
                                <a href="{{  "$rtwoprices&minprice=$quaterprice&maxprice=$mediaprice" }}">
                                    <li>{{$quaterprice}}$ a {{$mediaprice}}$</li>
                                </a>
                                <a href="{{ "$rtwoprices&minprice=$mediaprice" }}">
                                    <li>mas de {{$mediaprice}}$</li>
                                </a>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-9 col-lg-9 col-sm-12 col">
                        <div class="row">
                            <div class="col-10 offset-1 col-md-12 col-lg-12 mt-4">
                                @if(count($articles) > 0)
                                    <h3 class="text-capitalize float-left">hay {{ $articles->total() }} productos
                                        - {{ $word }}</h3>
                                    <div class="float-right">
                                        <label for="order">Ordernar por</label>
                                        <select name="order" id="order">
                                            <option value="new" @if(isset($_GET['order']) && $_GET['order'] == 'new') {{ "selected" }} @endif>
                                                Nuevo
                                            </option>
                                            {{--<option value="promotion" @if(isset($_GET['order']) && $_GET['order'] == 'promotion') {{ "selected" }} @endif>--}}
                                            {{--Descuento--}}
                                            {{--</option>--}}
                                            <option value="minprice" @if(isset($_GET['order']) && $_GET['order'] == 'minprice') {{ "selected" }} @endif>
                                                Precio Menor
                                            </option>
                                            <option value="maxprice" @if(isset($_GET['order']) && $_GET['order'] == 'maxprice') {{ "selected" }} @endif>
                                                Precio Mayor
                                            </option>
                                        </select>
                                    </div>
                                @else
                                @endif
                            </div>
                            @if(count($articles) > 0)
                                @foreach($articles as $article)
                                    <?php $principal_photo = \App\Photo::getArticlesPrimaryPhoto($article->id);?>
                                    <?php $principal_photo = \App\Photo::getArticlesPrimaryPhoto($article->id);?>
                                    <div class="col-10 offset-1 col-md-4 offset-md-0 col-sm-6 offset-sm-0 margin">
                                        <div class="card">
                                            <div class="card-img-top">
                                                <span class="helper"></span><img
                                                        src="@if($principal_photo != ""){{asset("uploads/articles/photos/".$principal_photo)}} @else {{ asset("img/picture.png") }} @endif"
                                                        alt="Card image cap" @if($principal_photo == "") class="no-image" @endif>
                                            </div>
                                            @if($article->id_promotion != 0)
                                                <?php $promotion = new \App\Promotion(); $promotion_name = $promotion->getPromotionArticle($article->id_promotion) ?>
                                                <div class="tag-descuento"><span>{{ $promotion_name }}%</span></div>
                                            @endif
                                            @if($article->new == 1)
                                                <div class="@if($article->id_promotion == 0) tag @else tag-nuevo @endif">
                                                    <span>NUEVO</span>
                                                </div>
                                            @endif
                                            <div class="card-body">
                                                <a href="{{ url("producto/$article->id") }}"><p
                                                            class="card-text">{{ $article->name }}</p></a>
                                                <h5 class="card-title">$ {{ $article->price }}</h5>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="col-12 col-md-12 col-lg-12 mb-5">
                                    <div class="float-left">
                                        <p>Mostrado {{ $articles->firstItem() }} - {{ $articles->lastItem() }} de
                                            {{ $articles->total() }}</p>
                                        <p>Articulo (s)</p>
                                    </div>
                                    <nav aria-label="Page navigation example" class="float-right">
                                        {{$articles->links() }}
                                    </nav>
                                </div>
                                @else
                                <div class="col-10 offset-1 col-md-12 col-sm-12 margin text-center">
                                    <h5>No se consiguieron resultados con: <strong class="text-capitalize"> {{ $word }}</strong></h5>
                                    <img src="{{ asset('img/looking.png') }}" alt="" width="100px">
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @else
                <div class="col-12 col-md-12 col-sm-12 margin text-center my-4 nofound">
                    <h5>No se consiguieron resultados con: <strong class="text-capitalize"> {{ $word }}</strong></h5>
                    <img src="{{ asset('img/looking.png') }}" alt="" width="100px">
                </div>
            @endif
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#order').change(function () {
            console.log(encodeURIComponent("{{ $rorder }}&order=" + $(this).val()));
            var text = "{{ $rorder }}&order=" + $(this).val();
            window.location.href = text.replace("&amp;", "&");
        })
    });
</script>

@include('footer/footer')