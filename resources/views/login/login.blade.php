@include('head/head')
<style>
    body {
        background: #ebebeb;
    }
</style>
<div class="row justify-content-center center-screen">
    <div class="col-10 offset-1 col-sm-10 col-md-3 offset-md-0 col-lg-3 offset-lg-0 login">
        <div class="mb-4 mt-2">
            <a href="{{ url('/') }}">
                <img src="{{asset('img/logo/tiendasv.png')}}" width="60%" alt="">
            </a>
        </div>
        @if(!empty($successMsg))
            <div class="alert alert-success"> {{ $successMsg }}</div>
        @endif
        @if(!empty($errorMsg))
            <div class="alert alert-danger"> {{ $errorMsg }}</div>
        @endif
        <form action="{{ url('/login') }}" method="post" class="col-12  col-sm-12">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                </div>
                <input type="text" class="form-control" placeholder="Username" name="email"
                       aria-describedby="basic-addon1">
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-key"></i></span>
                </div>
                <input type="password" class="form-control" placeholder="Contraseña" name="password"
                       aria-describedby="basic-addon1">
            </div>
            <button type="submit" class="btn btn-primary mt-1 mb-2">Iniciar sesion</button>
            <br>
            <div class="mb-3 my-2 text-center">
                <a href="{{ url('register') }}"><span>Registrate</span></a>
            </div>
            <div class="mb-3 text-center">
                <a href=""><span>Olvidaste tu contraseña?</span></a>
            </div>
        </form>
    </div>
</div>