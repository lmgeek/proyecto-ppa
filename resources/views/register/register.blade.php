@include('head/head')
<style>
    body {
        background: #ebebeb;
    }
</style>
<div class="row justify-content-center center-screen">
    <div class="col-10 offset-1 col-sm-10 col-md-3 offset-md-0 col-lg-3 offset-lg-0 login">
        <div class="mb-4 mt-2">
            <a href="{{ url('/') }}">
                <img src="{{asset('img/logo/tiendasv.png')}}" width="60%" alt="">
            </a>
        </div>
        <div class="alert alert-danger" id="errors">

        </div>
        <form action="{{ url('save/user') }}" method="post" id="register">
            <label for="">Nombre</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                </div>
                <input type="text" class="form-control" placeholder="Nombre" aria-label="Username" id="name" name="name"
                       aria-describedby="basic-addon1">
            </div>
            <label for="">Apellido</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                </div>
                <input type="text" class="form-control" placeholder="Apellido" aria-label="Username" id="lastname   " name="lastname"
                       aria-describedby="basic-addon1">
            </div>
            <label for="">Username</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                </div>
                <input type="text" class="form-control" placeholder="Username" aria-label="Username" name="username" id="username"
                       aria-describedby="basic-addon1">
            </div>
            <label for="">Correo Electronico</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-envelope"></i></span>
                </div>
                <input type="email" class="form-control" placeholder="Email" aria-label="Username" id="email" name="email"
                       aria-describedby="basic-addon1">
            </div>
            <label for="">Contraseña</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-key"></i></span>
                </div>
                <input type="password" class="form-control" placeholder="Contraseña" aria-label="Username" id="password" name="password"
                       aria-describedby="basic-addon1">
            </div>
            <label for="">Repita Contraseña</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-key"></i></span>
                </div>
                <input type="password" class="form-control" placeholder="Confirm contraseña" aria-label="Username" id="cpassword" name="cpassword"
                       aria-describedby="basic-addon1">
            </div>
            <button type="submit" class="btn btn-primary mt-1 mb-2" name="registarme" id="submit">Registrarme</button>
            <br>
            <a href="{{ url('login') }}"><span>Ya tengo cuenta</span></a>
        </form>
    </div>
</div>

<script !src="">
    $('document').ready(function(){
        $("#errors").hide();

        $.fn.validpass = function(){
            $(this).on('click change focusout', function(){
                var pass = $("#password").val();
                var cpass = $("#cpassword").val();
                var errors = $("#errors");
                console.log(pass + " " + cpass);
                if(pass != cpass){
                    $('#password').addClass('border-danger');
                    $('#cpassword').addClass('border-danger');
                    $("#submit").prop('disabled', true);
                    errors.show();
                    errors.empty();
                    errors.append("<lable class='danger'> Las contraseñas no coinciden</lable>")
                }else if(pass.length < 5 ){
                    $('#password').addClass('border-danger');
                    $('#cpassword').addClass('border-danger');
                    $("#submit").prop('disabled', true);
                    errors.show();
                    errors.empty();
                    errors.append("<lable class='danger'> La contraseña debe ser mayor de 5 caracteres</lable>")
                }else{
                    $('#password').removeClass('border-danger');
                    $('#cpassword').removeClass('border-danger');
                    errors.hide();
                    errors.empty();
                    $("#submit").prop('disabled', false);
                }
            });
        }
        $("#submit").validpass();
        $("#cpassword").validpass();
    });
</script>