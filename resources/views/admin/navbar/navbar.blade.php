<?php
if(auth()->guard('admin')->user()){
  $username =   auth()->guard('admin')->user()->name . " " . auth()->guard('admin')->user()->lastname ." ▼";
}
?>
<div class="row">
    <div class="col-12 col-sm-12 col-md-12 bg-light-blue navbar">
        <div class="col-6 col-sm-6 col-md-10 text-white">
            <a href="{{ url('adminxxs/panel') }}" class="mr-3">
                <img src="{{ asset('img/logo/white_tsv.png') }}" alt="" class="logo img-responsive">
            </a>
            <a href="{{ url('/') }}" class="text-white ml-3 d-md-inline-block d-none">
                Ir a la pagina <i class="fas fa-arrow-alt-circle-right"></i>
            </a>
        </div>
        <div class="col-6 col-sm-6 col-md-2">
            <span class="text-white" data-toggle="dropdown"> {{ $username }}</span>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="{{ url('adminxxs/logout') }}">Cerrar sesión</a>
            </div>
        </div>
    </div>
</div>