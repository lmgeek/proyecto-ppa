@include('head/head')
@include('admin/navbar/navbar')
<div class="container-fluid">
    <div class="row">
        @include('admin/sidebar/sidebar')
        <div class="col-md-8 offset-1 my-3">
            @if (session('successMsg'))
                <div class="alert alert-success">
                    {{ session('successMsg') }}
                </div>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row  my-4">
                <div class="col-md-6 text-left">
                    <h2>Transportes</h2>
                </div>
                <div class="col-md-6 text-right">
                    <a class="btn btn-primary" href="{{ url('/adminxxs/transports/new') }}">
                        Nuevo Transporte
                    </a>
                </div>
            </div>
            <table class="table my-3">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Carga Maxima</th>
                    <th scope="col" colspan="3">Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($transports as $key => $transport)
                    <tr>
                        <td> {{ $transport['id'] }}</td>
                        <td> {{ $transport['name'] }}</td>
                        <td> {{ $transport['amount'] }}</td>
                        <td>@if($transport['active'] == 1)<a
                                    href="{{ url('deactive/transport/'.$transport->id) }}"><i
                                        class="fas fa-ban"></i></a>@else<a
                                    href="{{ url('active/transport/'.$transport->id) }}"><i
                                        class="fas fa-check-circle"></i></a> @endif
                        </td>
                        <td><a href="{{ url('destroy/transport/'.$transport->id) }}"> <i
                                        class="fas fa-trash-alt"></i> </a>
                        </td>
                        <td>
                            <a href="{{ url('adminxxs/transport/'.$transport->id) }}">
                                <i class="fas fa-edit">
                                </i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <nav aria-label="Page navigation example" class="float-right">
                {{$transports->links() }}
            </nav>
        </div>
    </div>
</div>