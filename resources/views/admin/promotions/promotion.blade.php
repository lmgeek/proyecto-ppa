@include('head/head')
@include('admin/navbar/navbar')
<div class="container-fluid">
    <div class="row">
        @include('admin/sidebar/sidebar')
        <div class="col-10 offset-1 col-sm-10 col-md-8 offset-md-1 my-4">
            <div class="row my-4">
                <div class="col-md-6 text-left">
                    <h2>Nueva promoción</h2>
                </div>
            </div>
            <form role="form" method="POST" action="@if(isset($edit) && $edit) {{ url('update/promotion/'.$promotion->id) }}  @else {{ url('save/transport') }} @endif">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Titulo: (Ej: 20%, 10%, 5% ...)
                    </label>
                    <input type="text" class="form-control" name="title" @if(isset($promotion)) value="{{$promotion->name}}" @endif/>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">
                        Descripción:
                    </label>
                    <textarea class="form-control" rows="8" name="description">@if(isset($promotion)) {{$promotion->description}} @endif </textarea>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Activo:
                    </label>
                    <input type="radio" name="active" value="1" @if(isset($promotion) && $promotion->active == 1)  checked @endif>
                    <label>Si</label>
                    <input type="radio" name="active" value="0" @if(isset($promotion) && $promotion->active == 0)  checked @endif>
                    <label>No</label>
                </div>
                <button type="submit" class="btn btn-primary">
                    Guardar
                </button>
            </form>
        </div>
    </div>
</div>