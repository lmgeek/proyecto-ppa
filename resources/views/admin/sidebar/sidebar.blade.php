<div class="col-md-2 menu-left bg-_dark d-md-block d-none">
    {{--<div class="navbar navbar-expand-lg text-center my-1">--}}
        {{--<a href="{{ url('/') }}">--}}
            {{--<img src="{{asset('img/logo/tiendasv.png')}}" width="70%" alt="">--}}
        {{--</a>--}}
    {{--</div>--}}
    {{--<hr>--}}
    <div>
        <ul class="list-inline">
            <li class="blue my-2">
                <a class="nav-link " href="{{ url('/adminxxs/users') }}"><i class="fas fa-users"></i> Usuarios</a>
            </li>
            <li class="blue my-2">
                <a class="nav-link" href="{{ url('/adminxxs/orders') }}"><i class="fas fa-boxes"></i> Ordernes</a>
            </li>
            <li class="blue my-2">
                <a class="nav-link" href="{{ url('/adminxxs/articles') }}"><i class="fas fa-box"></i> Articulos</a>
            </li>
            <li class="blue my-2">
                <a class="nav-link" href="{{ url('/adminxxs/categories') }}"><i class="fas fa-tag"></i></i> Categorias</a>
            </li>
            <li class="blue my-2">
                <a class="nav-link" href="{{ url('/adminxxs/subcategories') }}"><i class="fas fa-tags"></i> Subategorias</a>
            </li>
            <li class="blue my-2">
                <a class="nav-link" href="{{ url('/adminxxs/promotions') }}"><i class="fas fa-trophy"></i> Promociones</a>
            </li>
            <li class="blue my-2">
                <a class="nav-link" href="{{ url('/adminxxs/transports') }}"><i class="fas fa-motorcycle"></i> Tranporte</a>
            </li>
            <li class="blue my-2">
                <a class="nav-link" href="{{ url('adminxxs/config/layout') }}"><i class="fas fa-cogs"></i> Configuracion</a>
            </li>
            {{--<li class="nav-item dropdown">--}}
            {{--<a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink"--}}
            {{--data-toggle="dropdown">Dropdown link</a>--}}
            {{--<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">--}}
            {{--<a class="dropdown-item" href="#">Action</a> <a class="dropdown-item" href="#">Another--}}
            {{--action</a> <a class="dropdown-item" href="#">Something else here</a>--}}
            {{--<div class="dropdown-divider">--}}
            {{--</div>--}}
            {{--<a class="dropdown-item" href="#">Separated link</a>--}}
            {{--</div>--}}
            {{--</li>--}}
        </ul>
    </div>
</div>

<div class="text-center col-12  mb-4 menu-bot-r d-md-none">
    <div class="row bg-light-blue">
        <div class="white-text my-2 col-2 col-sm-2 offset-1">
            <a class="nav-link " href="{{ url('/adminxxs/users') }}"><i class="fas fa-users white-text"></i></a>
        </div>
        <div class="white-text my-2 col-2 col-sm-2">
            <a class="nav-link" href="{{ url('/adminxxs/orders') }}"><i class="fas fa-boxes white-text"></i></a>
        </div>
        <div class="white-text my-2 col-2 col-sm-2">
            <a class="nav-link" href="{{ url('/adminxxs/articles') }}"><i class="fas fa-box white-text"></i></a>
        </div>
        <div class="white-text my-2 col-2 col-sm-2">
            <a class="nav-link" href="{{ url('/adminxxs/categories') }}"><i class="fas fa-tag white-text"></i></a>
        </div>
        <div class="white-text my-2 col-2 col-sm-2">
            <a class="nav-link" href="{{ url('/adminxxs/subcategories') }}"><i class="fas fa-tags white-text"></i></a>
        </div>
        {{--<div class="white-text my-2 col-2 col-sm-2">--}}
            {{--<a class="nav-link" href="{{ url('/adminxxs/promotions') }}"><i class="fas fa-trophy white-text"></i></a>--}}
        {{--</div>--}}
        {{--<div class="white-text my-2 col-2 col-sm-2">--}}
            {{--<a class="nav-link" href="{{ url('/adminxxs/transports') }}"><i class="fas fa-motorcycle white-text"></i></a>--}}
        {{--</div>--}}
    </div>
</div>