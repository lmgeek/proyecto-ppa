@include('head/head')
@include('admin/navbar/navbar')
<div class="container-fluid">
    <div class="row">
        @include('admin/sidebar/sidebar')
        <div class="col-10 offset-1 col-sm-10 col-md-8 col-md-1 my-3">
            <div class="jumbotron">
                <h2>
                    Hola, {{ $user->name . ' ' . $user->lastname }}
                </h2>
                <p>
                    <br>
                    Bienvenido al panel administraivo de TiendaSV.
                    <br>
                    Aqui puedes encontrar todo lo necesario para crear tus productos,
                    promociones, categorias, subcategorias, transporte y demas actividades que sean necesarias.
                </p>
            </div>
            <div class="row my-4">
                <div class="col-12 col-sm-12 col-md-6 text-left">
                    <h2>Articulos Nuevos</h2>
                </div>
            </div>
            <div class="row">
                @foreach($articles as $article)
                    <?php $principal_photo = \App\Photo::getArticlesPrimaryPhoto($article->id);?>
                    <div class=" col col-12 col-sm-6 col-md-3 margin">
                        <div class="card">
                            <div class="card-img-top">
                                <span class="helper"></span>
                                <img src="@if($principal_photo != ""){{asset("uploads/articles/photos/".$principal_photo)}} @else {{ asset("img/picture.png") }} @endif"
                                     alt="Card image cap" @if($principal_photo == "") class="no-image" @endif>
                            </div>
                            @if($article->id_promotion != 0)
                                <?php $promotion = new \App\Promotion(); $promotion_name = $promotion->getPromotionArticle($article->id_promotion) ?>
                                <div class="tag-descuento"><span>{{  $promotion_name }}%</span></div>
                            @endif
                            @if($article->new == 1)
                                <div class="@if($article->id_promotion == 0) tag @else tag-nuevo @endif">
                                    <span>NUEVO</span>
                                </div>
                            @endif
                            <div class="card-body">

                                <a href="{{ url("producto/$article->id") }}"><p
                                            class="card-text">{{ $article->name }}</p>
                                </a>
                                <p class="card-title">$ {{ $article->price }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row my-4">
                <div class="col-12 col-sm-12 col-md-6 text-left">
                    <h2>Categorias y subcategorias</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-6 col-sm-6 col-md-6">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th scope="col" colspan="3">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $key => $category)
                            @if($key > 4)
                                <?php break ?>
                            @endif
                            <tr>
                                <td> {{ $category['id'] }}</td>
                                <td> {{ $category['name'] }}</td>
                                <td> <i class="fas fa-ban"></i> </td>
                                <td> <i class="fas fa-trash-alt"></i> </td>
                                <td>
                                    <a href="{{ url('adminxxs/category/'.$category->id) }}">
                                        <i class="fas fa-edit">
                                        </i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-6 col-sm-6 col-md-6">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th scope="col" colspan="3">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($subcategories as $key => $subcategory)
                            @if($key > 4)
                                <?php break ?>
                            @endif
                            <tr>
                                <td> {{ $subcategory['id'] }}</td>
                                <td> {{ $subcategory['name'] }}</td>
                                <td> <i class="fas fa-ban"></i> </td>
                                <td> <i class="fas fa-trash-alt"></i> </td>
                                <td>
                                    <a href="{{ url('adminxxs/category/'.$subcategory->id) }}">
                                        <i class="fas fa-edit">
                                        </i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>