@include('head/head')
@include('admin/navbar/navbar')
<div class="container-fluid">
    <div class="row">
        @include('admin/sidebar/sidebar')
        <div class="col-10 offset-1 col-sm-10 col-md-8 offset-1 my-4">
            <div class="row my-4">
                <div class="col-md-6 text-left">
                    <h2>Nueva subcategoría</h2>
                </div>
            </div>
            <form role="form" method="POST" action="@if(isset($edit) && $edit) {{ url('update/subcategory/'.$subcategory->id) }}  @else {{ url('save/subcategory') }} @endif">
                 {{ csrf_field() }}
                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Titulo: (Ej: Licor, Granos, Semillas ...)
                    </label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="title" @if(isset($subcategory)) value="{{$subcategory->name}}" @endif/>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Subcategoria de la categoria:
                    </label>
                    <select name="category" id="" class="form-control" name="category">
                        @foreach ($categories as $categorie)
                            <option value="{{ $categorie['id'] }}" @if(isset($c_subcategory) && $c_subcategory{0}->id_category == $categorie['id']) selected @endif>{{ $categorie['name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">
                        Descripción:
                    </label>
                    <textarea class="form-control" rows="8" name="description"> @if(isset($subcategory)) {{$subcategory->description}} @endif</textarea>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Activo:
                    </label>
                    <input type="radio" name="active" value="1" @if(isset($subcategory) && $subcategory->active == 1)  checked @endif>
                    <label>Si</label>
                    <input type="radio" name="active" value="0" @if(isset($subcategory) && $subcategory->active == 0)  checked @endif>  
                    <label>No</label>
                </div>
                <button type="submit" class="btn btn-primary mb-5">
                    Guardar
                </button>
            </form>
        </div>
    </div>
</div>