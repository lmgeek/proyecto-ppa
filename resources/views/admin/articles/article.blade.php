@include('head/head')
@include('admin/navbar/navbar')
<div class="container-fluid">
    <div class="row">
        @include('admin/sidebar/sidebar')
        <div class="col-10 offset-1 col-sm-10 col-md-8 offset-md-1 my-4">
            <div class="row my-4">
                <div class="col-md-6 text-left">
                    <h2>Nuevo articulo</h2>
                </div>
            </div>
            <form role="form" method="POST" action="@if(isset($edit) && $edit) {{ url('update/article/'.$article->id) }}  @else {{ url('save/article') }} @endif" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Titulo: (Ej: Cerveza, granos, cemillas ...)
                    </label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="title"  @if(isset($article)) value="{{$article->name}}" @endif/>
                </div>
                <div class="checkbox">
                    <span class="switch">
                      <input type="checkbox" name="new">
                      <label for="switch-id">Nuevo</label>
                    </span>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">
                      #  Codigo, Modelo, Numeracion, etc..
                    </label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="model"  @if(isset($article)) value="{{$article->model}}" @endif/>
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">
                        Imagenes:
                    </label>
                    <div class="row">
                        @for($i = 1 ; $i <= 5 ; $i++)
                            <div class="col-md-2 img-article">
                                <img src="@if(isset($photos[$i-1])){{asset("uploads/articles/photos/".$photos[$i-1]->photo)}} @else {{ asset("img/picture.png") }} @endif" id="img{{$i}}"  class="img-fluid">
                                <input id="foto{{$i}}" name="img{{$i}}" type="file" class="form-control" onchange="readURL(this)">
                            </div>
                        @endfor
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">
                        Fecha de vencimiento:
                    </label>
                    <input type="date" class="form-control" name="due_date" @if(isset($article)) value="{{$article->due_date}}" @endif>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">
                        Descripción:
                    </label>
                    <textarea class="form-control" rows="8" name="description"> @if(isset($article)) {{$article->description}} @endif</textarea>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-9">
                            <label for="exampleInputPassword1">
                                Categoría:
                            </label>
                            <select name="category" id="" class="form-control" name="category">
                                @foreach ($categories as $categorie)
                                    <option value="{{ $categorie['id'] }}" @if(isset($category) && $category->id_category == $categorie['id']) selected @endif>{{ $categorie['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12 col-sm-12 col-md-3">
                            <a href="{{ url('/adminxxs/categories/new') }}">
                                <button class="btn btn-success align-bottom btn-bottom">
                                    Nueva categoría
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-9">
                            <label for="exampleInputPassword1">
                                Subcategoría:
                            </label>
                            <select name="category" id="" class="form-control" name="subcategory">
                                @foreach ($subcategories as $subcategorie)
                                    <option value="{{ $subcategorie['id'] }}" @if(isset($subcategory) && $subcategory->id_subcategory == $subcategorie['id']) selected @endif>{{ $subcategorie['name'] }}</option>
                                @endforeach
                            </select>
                            </select>
                        </div>
                        <div class="col-12 col-sm-12 col-md-3 align-bottom">
                            <button type="submit" class="btn btn-success btn-bottom">
                                Nueva subcategoría
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">
                        Promoción:
                    </label>
                    <select id="" class="form-control" name="id_promotion">
                        @foreach ($promotions as $promotion)
                            <option value="{{ $promotion['id'] }}"  @if(isset($article->id_promotion) && $article->id_promotion == $promotion['id']) selected @endif>{{ $promotion['name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">
                        Precío:
                    </label>
                    <input type="number" class="form-control" id="exampleInputPassword1" name="price" @if(isset($article)) value="{{$article->price}}" @endif/>
                </div>

                <button type="submit" class="btn btn-primary mb-5">
                    Guardar
                </button>
            </form>
        </div>
    </div>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#'+input.name)
                        .attr('src', e.target.result)
                        .css('maxWidth', '150px')
                        .css('maxHeight', '150px');
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
</div>