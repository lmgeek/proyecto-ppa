@include('head/head')
@include('admin/navbar/navbar')
<div class="container-fluid">
    <div class="row">
        @include('admin/sidebar/sidebar')
        <div class="col-10 offset-1 col-sm-10 col-md-8 offset-md-1 my-3">
            @if (session('successMsg'))
                <div class="alert alert-success">
                    {{ session('successMsg') }}
                </div>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row my-4">
                <div class="col-12 col-sm-12 col-md-6 text-left">
                    <h2>Articulos</h2>
                </div>
                <div class="col-12 col-sm-12 col-md-6 text-right">
                    <a class="btn btn-primary" href="{{ url('/adminxxs/articles/new') }}">
                        Nuevo artículo
                    </a>
                </div>
            </div>
            <table class="table my-3">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Título</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Disponible</th>
                    <th scope="col">Fecha de vencimiento</th>
                    <th scope="col" colspan="3">Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($articles as $key => $article)
                    <tr class="d-md-table-row d-none">
                        <td>
                            {{ $article['id'] }}
                        </td>
                        <td>
                            {{ $article['name'] }}
                        </td>
                        <td>
                            {{ $article['price'] }}
                        </td>
                        <td>
                            {{ $article['available'] }}
                        </td>
                        <td>
                            {{ $article['due_date'] }}
                        </td>
                        <td>@if($article['active'] == 1)<a href="{{ url('deactive/article/'.$article->id) }}"><i
                                        class="fas fa-ban"></i></a>@else<a
                                    href="{{ url('active/article/'.$article->id) }}"><i class="fas fa-check-circle"></i></a> @endif
                        </td>
                        <td><a href="{{ url('destroy/article/'.$article->id) }}"> <i class="fas fa-trash-alt"></i> </a>
                        </td>
                        <td><a href="{{ url('adminxxs/article/'.$article->id) }}">
                                <i class="fas fa-edit">
                                </i>
                            </a></td>
                    </tr>

                    <tr class="border my-4 d-md-none">
                        <td>
                            <strong>ID</strong>
                            <p>
                                {{ $article['id'] }}
                            </p>
                        </td>
                        <td>
                            <strong>Título</strong>
                            <p>
                                {{ $article['name'] }}
                            </p>
                        </td>
                        <td>
                            <strong>Precio</strong>
                            <p>
                                {{ $article['price'] }}
                            </p>
                        </td>
                        <td>
                            <strong>Disponible</strong>
                            <p>
                                {{ $article['available'] }}
                            </p>
                        </td>
                        <td>
                            <strong>Fecha de vencimiento</strong>
                            <p>
                                {{ $article['due_date'] }}
                            </p>
                        </td>
                        <td>
                            @if($article['active'] == 1)
                                <strong>Desactivar</strong>
                                <p>
                                    <a href="{{ url('deactive/article/'.$article->id) }}"><i class="fas fa-ban"></i></a>
                                </p>
                            @else
                                <strong>Activar</strong>
                                <p>
                                    <a href="{{ url('active/article/'.$article->id) }}"><i
                                                class="fas fa-check-circle"></i></a>
                                </p>
                            @endif
                        </td>
                        <td>
                            <strong>Eliminar</strong>
                            <p>
                                <a href="{{ url('destroy/article/'.$article->id) }}"> <i class="fas fa-trash-alt"></i>
                                </a>
                            </p>
                        </td>
                        <td>
                            <strong>Editar</strong>
                            <p>
                                <a href="{{ url('adminxxs/article/'.$article->id) }}"><i class="fas fa-edit"></i></a>
                            </p>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <nav aria-label="Page navigation example" class="float-right">
                {{$articles->links() }}
            </nav>
        </div>
    </div>
</div>