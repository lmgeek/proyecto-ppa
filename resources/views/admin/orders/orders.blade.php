@include('head/head')
@include('admin/navbar/navbar')
<div class="container-fluid">
    <div class="row">
        @include('admin/sidebar/sidebar')
        <div class="col-10 offset-1 col-sm-10 col-md-8 offset-md-1 my-3">
            @if (session('successMsg'))
                <div class="alert alert-success">
                    {{ session('successMsg') }}
                </div>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row  my-4">
                <div class="col-md-6 text-left">
                    <h2>Ordernes</h2>
                </div>
                {{--<div class="col-md-6 text-right">--}}
                    {{--<a class="btn btn-primary" href="{{ url('/adminxxs/articles/new') }}">--}}
                        {{--Nuevo uartículo--}}
                    {{--</a>--}}
                {{--</div>--}}
            </div>
            <div class="accordion" id="accordionExample">
                @foreach($orders as $key => $order)
                    <div class="card-accordion">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#collapse{{$key}}" aria-expanded="false"
                                        aria-controls="collapseTwo">
                                    Numero de orden: <p>{{$order->ordernumber}} </p>
                                </button>
                            </h5>
                        </div>
                        <div id="collapse{{$key}}" class="collapse" aria-labelledby="headingTwo"
                             data-parent="#accordionExample">
                            <div class="card-body-accordiong">
                                @foreach($items[$order->ordernumber] as $key=>$item)
                                    <?php $principal_photo = \App\Photo::getArticlesPrimaryPhoto($item->id_article)?>
                                    <div class="container my-4">
                                        <div class="row">
                                            <div class="col-10 offset-1 col-sm-10 col-md-2 offset-md-0">
                                                <div class="img-card">
                                                    <span class="helper"></span>
                                                    <img src="@if($principal_photo != ""){{asset("uploads/articles/photos/".$principal_photo)}} @else {{ asset("img/picture.png") }} @endif" alt="Card image cap" @if($principal_photo == "") class="no-image" @endif alt="">
                                                </div>
                                            </div>
                                            <div class="col-10 offset-1 col-sm-10 col-md-10 offset-md-0">
                                                <a href="{{ url("producto/$item->id_article") }}">
                                                    <strong>{{$item->name}} ${{$item->price}}</strong>
                                                </a>
                                                <p class="float-right myfont-10 d-md-block d-none">
                                                    Comprado el: {{ $item->created }}
                                                </p>
                                                <hr>
                                                <p> {{$item->description}}</p>
                                                <p><strong>Cantidad: {{$item->count}}</strong></p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>