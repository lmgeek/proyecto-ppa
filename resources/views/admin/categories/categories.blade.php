@include('head/head')
@include('admin/navbar/navbar')
<div class="container-fluid">
    <div class="row">
        @include('admin/sidebar/sidebar')
        <div class="col-10 offset-1 col-sm-10 col-md-8 offset-1 my-3">
            @if (session('successMsg'))
                <div class="alert alert-success">
                    {{ session('successMsg') }}
                </div>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row  my-4">
                <div class="col-12 col-sm-12 col-md-6 text-left">
                    <h2>Categorías</h2>
                </div>
                <div class="col-12 col-sm-12 col-md-6 text-right">
                    <a class="btn btn-primary" href="{{ url('/adminxxs/categories/new') }}">
                        Nueva categoría
                    </a>
                </div>
            </div>
            <table class="table my-3">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col" colspan="3">Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $key => $category)
                    <tr class="d-md-table-row d-none">
                        <td> {{ $category['id'] }}</td>
                        <td> {{ $category['name'] }}</td>
                        <td>@if($category['active'] == 1)<a href="{{ url('deactive/category/'.$category->id) }}"><i
                                        class="fas fa-ban"></i></a>@else<a
                                    href="{{ url('active/category/'.$category->id) }}"><i
                                        class="fas fa-check-circle"></i></a> @endif
                        </td>
                        <td><a href="{{ url('destroy/category/'.$category->id) }}"> <i class="fas fa-trash-alt"></i>
                            </a>
                        </td>
                        <td>
                            <a href="{{ url('adminxxs/category/'.$category->id) }}">
                                <i class="fas fa-edit">
                                </i>
                            </a>
                        </td>
                    </tr>
                    <tr class="d-md-none border my-4">
                        <td><strong> ID </strong>
                            <p>{{ $category['id'] }}</p></td>
                        <td><strong> Nombre </strong>
                            <p>{{ $category['name'] }}</p></td>
                        <td>@if($category['active'] == 1)
                                <strong> Desactivar </strong>
                                <p>
                                    <a href="{{ url('deactive/subcategory/'.$category->id) }}"><i
                                                class="fas fa-ban"></i></a>
                                </p>
                            @else
                                <strong> Activar </strong>
                                <p>
                                    <a href="{{ url('active/subcategory/'.$category->id) }}"><i
                                                class="fas fa-check-circle"></i></a>
                                </p>
                            @endif
                        </td>
                        <td>
                            <strong> Eliminar </strong>
                            <p>
                                <a href="{{ url('destroy/subcategory/'.$category->id) }}"><i
                                            class="fas fa-trash-alt"></i> </a>
                            </p>
                        </td>
                        <td>
                            <strong> Editar </strong>
                            <p>
                                <a href="{{ url('adminxxs/subcategory/'.$category->id) }}"><i
                                            class="fas fa-edit"></i></a>
                            </p>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <nav aria-label="Page navigation example" class="float-right">
                {{$categories->links() }}
            </nav>
        </div>
    </div>
</div>