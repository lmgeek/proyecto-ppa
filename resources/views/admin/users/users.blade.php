@include('head/head')
@include('admin/navbar/navbar')
<div class="container-fluid">
    <div class="row">
        @include('admin/sidebar/sidebar')
        <div class="col-10 offset-1 col-sm-10 col-md-8 offset-1 my-3">
            @if (session('successMsg'))
                <div class="alert alert-success">
                    {{ session('successMsg') }}
                </div>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row  my-4">
                <div class="col-12 col-sm-12 col-md-6 text-left">
                    <h2>Usuarios</h2>
                </div>
                <div class="col-12 col-sm-12 col-md-6 text-right">
                    {{--<button class="btn btn-primary">--}}
                        {{--Nuevo Usuario--}}
                    {{--</button>--}}
                </div>
            </div>
            <table class="table my-3">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellido</th>
                    <th scope="col">Usuario</th>
                    <th scope="col">Email</th>
                    <th scope="col">Activo</th>
                    <th scope="col">Creado</th>
                    <th scope="col">Actualizado</th>
                    <th scope="col" colspan="3">Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $key => $user)
                    <tr class="d-md-table-row d-none">
                        <td> {{ $user['id'] }}</td>
                        <td> {{ $user['name']  }}</td>
                        <td> {{ $user['lastname'] }}</td>
                        <td> {{ $user['username'] }}</td>
                        <td> {{ $user['email'] }}</td>
                        <td>
                            @if($user['active'] == 1)
                                Si
                            @else
                                No
                            @endif
                        </td>
                        <td> {{ $user['created_at'] }}</td>
                        <td> {{ $user['updated_at'] }}</td>
                        <td> @if($user['active'] == 1)
                                <a href="{{ url('deactive/user/'.$user->id) }}"><i class="fas fa-ban"></i></a>
                            @else
                                <a href="{{ url('active/user/'.$user->id) }}"><i class="fas fa-check-circle"></i></a>
                            @endif</td>
                        <td><a href="{{ url('destroy/user/'.$user->id) }}"> <i class="fas fa-trash-alt"></i> </a></td>
                        {{--<td><a href="{{ url('adminxxs/user/'.$user->id) }}"><i class="fas fa-edit"></i></a></td>--}}
                    </tr>
                    <tr class="d-md-none border my-4">
                        <td>
                            <strong>ID</strong>
                            <p>
                                {{ $user['id'] }}
                            </p>
                        </td>
                        <td>
                            <strong>Nombre</strong>
                            <p>
                                {{ $user['name'] }}
                            </p>
                        </td>
                        <td>
                            <strong>Apellido</strong>
                            <p>
                                {{ $user['lastname'] }}
                            </p>
                        </td>
                        <td>
                            <strong>Usuario</strong>
                            <p>
                                {{ $user['username'] }}
                            </p>
                        </td>
                        <td>
                            <strong>Email</strong>
                            <p>
                                {{ $user['email'] }}
                            </p>
                        </td>
                        <td>
                            <strong>Activo</strong>
                            <p>
                                @if($user['active'] == 1)
                                    Si
                                @else
                                    No
                                @endif
                            </p>
                        </td>
                        <td>
                            @if($user['active'] == 1)
                                <strong>Desactivar</strong>
                                <p>
                                    <a href="{{ url('deactive/user/'.$user->id) }}"><i class="fas fa-ban"></i></a>
                                </p>
                            @else
                                <strong>Activar</strong>
                                <p>
                                    <a href="{{ url('active/user/'.$user->id) }}"><i class="fas fa-check-circle"></i></a>
                                </p>
                            @endif
                        </td>
                        <td>
                            <strong>Eliminar</strong>
                            <p>
                                <a href="{{ url('destroy/user/'.$user->id) }}"> <i class="fas fa-trash-alt"></i> </a>
                            </p>
                        </td>
                        {{--<td>--}}
                            {{--<strong>Editar</strong>--}}
                            {{--<p>--}}
                                {{--<a href="{{ url('adminxxs/user/'.$user->id) }}"><i class="fas fa-edit"></i></a>--}}
                            {{--</p>--}}
                        {{--</td>--}}
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>