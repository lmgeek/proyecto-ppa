@include('head/head')
<style>
    body {
        background: #ebebeb;
    }
</style>
<div class="row justify-content-center center-screen animated fadeIn">
    <div class="col-10 offset-1 col-sm-10 col-md-3 offset-md-0 col-lg-3 col-lg-0 login">
        <div class="mb-4 mt-2">
            <a href="{{ url('/') }}">
                <img src="{{asset('img/logo/tiendasv.png')}}" width="60%" alt="">
            </a>
        </div>
        @if(!empty($errorMsg))
            <div class="alert alert-danger"> {{ $errorMsg }}</div>
        @endif
        <form action="{{ url('/adminxxs/login') }}" method="post">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                </div>
                <input type="text" class="form-control" placeholder="Email" name="email">
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-key"></i></span>
                </div>
                <input type="password" class="form-control" placeholder="Contraseña" name="password">
            </div>
            <button type="submit" class="btn btn-primary mt-1 mb-2">Iniciar sesion</button>
        </form>
    </div>
</div>