<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 banner text-center">
    @if(Auth::guard('web')->user()->photo != "")
        <img class="user-profile" src="{{ asset('storage/app/public/userphotos/'.Auth::guard('web')->user()->photo) }}" alt="">
        @else
        <img class="user-profile" src="{{asset("img/user.jpg")}}" alt="">
    @endif
</div>