<div class="col-md-2 menu-left bg-light d-md-block d-none">
    <div>
        <ul class="list-inline">
            <li class="blue my-2">
                <a class="nav-link myfont-12" href="{{ url('/usuario/perfil') }}"><i class="fas fa-users"></i>  Mi cuenta</a>
            </li>
            <li class="blue my-2">
                <a class="nav-link myfont-12" href="{{ url('/usuario/compras') }}"><i class="fas fa-box"></i>  Compras</a>
            </li>
            <li class="blue my-2">
                <a class="nav-link myfont-12" href="{{ url('/usuario/carrito') }}"><i class="fas fa-shopping-cart"></i>  Carrito</a>
            </li>
            <li class="blue my-2">
                <a class="nav-link myfont-12" href="{{ url('/usuario/historial') }}"><i class="fas fa-history"></i>  Historial</a>
            </li>
            <li class="blue my-2">
                <a class="nav-link myfont-12" href="{{ url('/usuario/config') }}"><i class="fas fa-cogs"></i> Configuracion</a>
            </li>
        </ul>
    </div>
</div>
<div class="text-center col-12  mb-4 menu-bot-r d-md-none">
    <div class="row bg-light-blue">
            <div class="white-text my-2 col-2 col-sm-2 offset-1">
                <a class="nav-link myfont-12" href="{{ url('/usuario/perfil') }}"><i class="fas fa-users white-text"></i></a>
            </div>
            <div class="white-text my-2 col-2 col-sm-2">
                <a class="nav-link myfont-12" href="{{ url('/usuario/compras') }}"><i class="fas fa-box white-text"></i></a>
            </div>
            <div class="white-text my-2 col-2 col-sm-2">
                <a class="nav-link myfont-12" href="{{ url('/usuario/carrito') }}"><i class="fas fa-shopping-cart white-text"></i></a>
            </div>
            <div class="white-text my-2 col-2 col-sm-2">
                <a class="nav-link myfont-12" href="{{ url('/usuario/historial') }}"><i class="fas fa-history white-text"></i></a>
            </div>
            <div class="white-text my-2 col-2 col-sm-2">
                <a class="nav-link myfont-12" href="{{ url('/usuario/config') }}"><i class="fas fa-cogs white-text"></i></a>
            </div>
        </ul>
    </div>
</div>