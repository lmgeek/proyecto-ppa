@include('head/head')
@include('navbar/navbar')
<div class="container-fluid">
    <div class="row">
        @include('users/sidebar/sidebar')
        <div class="col-12 col-sm-12 col-md-10">
            @include('users/banner/banner')
            <div class="col-10 offset-1 col-sm-10 offset-sm-1 col-md-10 offset-md-1">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <div class="row  my-4">
                    <div class="col-12 col-sm-12 col-md-6 text-left">
                        <h4 class="my-2">Mi cuenta</h4>
                        <div class="col-12 col-sm-12 col-md-12 my-4">
                            <p class="myfont-13 light-blue">Información personal</p>
                            <hr>
                            <p class="ml-4 myfont-11">
                                <strong>Nombre:</strong> {{ $user->name .  " " . $user->lastname }}</p>
                            <p class="ml-4 myfont-11"><strong>DNI:</strong> {{ $user->dni }}</p>
                            <p class="ml-4 myfont-11"><strong>Fecha de nacimiento:</strong> {{ $user->birthday }}</p>
                        </div>
                        <div class="col-md-12 my-4">
                            <p class="myfont-13 light-blue">Información del perfil</p>
                            <hr>
                            <p class="ml-4 myfont-11"><strong>Usuario:</strong> {{ $user->username }}</p>
                            <p class="ml-4 myfont-11"><strong>Email:</strong> {{ $user->email }}</p>
                        </div>
                        <div class="col-md-12 my-4">
                            <p class="myfont-13 light-blue">Dirección</p>
                            <hr>
                            <p class="ml-4 myfont-11"><strong>Dirección:</strong>{{ $user->address }}</p>
                            <p class="ml-4 myfont-11"><strong>Codigo Postal:</strong> {{ $user->zip_code }}</p>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 text-left">
                        <h4 class="my-2">Visto última vez</h4>
                        <div class="row">
                            @foreach($records as $record)
                                <?php $principal_photo = \App\Photo::getArticlesPrimaryPhoto($record->id_article)?>
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 my-2">
                                    <div class="img-card">
                                        <span class="helper"></span>
                                        <img src="@if($principal_photo != ""){{asset("uploads/articles/photos/".$principal_photo)}} @else {{ asset("img/picture.png") }} @endif" alt="Card image cap" @if($principal_photo == "") class="no-image" @endif alt="">
                                    </div>
                                    <div class="text-card title-profile-article">
                                        <a href="{{ url("producto/$record->id_article") }}">
                                            <p >{{ $record->name }}</p>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer/footer')