@include('head/head')
@include('navbar/navbar')
<div class="container-fluid">
    <div class="row">
        @include('users/sidebar/sidebar')
        <div class="col-12 col-sm-12 col-md-10">
            @include('users/banner/banner')
            <div class="col-12 col-sm-12 col-md-10 offset-md-1">
                <div class="row my-4">
                    <div class="col-12 col-sm-12 col-md-10 offset-md-1">
                        <h4 class="my-2 mb-4">Ultimos productos vistos</h4>
                        @foreach($records as $record)
                            <?php $principal_photo = \App\Photo::getArticlesPrimaryPhoto($record->id_article)?>
                            <div class="jumbotron jumbotron-fluid" style="background: white; height: auto; border: 1px solid #e2e3e5;">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-2">
                                            <div class="img-card">
                                                <span class="helper"></span>
                                                <img src="@if($principal_photo != ""){{asset("uploads/articles/photos/".$principal_photo)}} @else {{ asset("img/picture.png") }} @endif"
                                                     alt="Card image cap"
                                                     @if($principal_photo == "") class="no-image" @endif alt="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-10">
                                            <a href="{{ url("producto/$record->id_article") }}">
                                                <strong>{{$record->name}}  ${{$record->price}}</strong>
                                            </a>
                                            <hr>
                                            <p> {{$record->description}}</p>
                                            <button class="btn btn-primary float-right">
                                                Comprar
                                            </button>
                                            <button href="#" class="btn btn-outline-primary float-right mr-2">
                                                <i class="fas fa-shopping-cart"></i> Carrito
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer/footer')