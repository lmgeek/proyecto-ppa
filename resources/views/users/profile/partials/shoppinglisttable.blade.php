<tbody>
@foreach($articles as $key => $article)
    <?php $principal_photo = \App\Photo::getArticlesPrimaryPhoto($article->id_article)?>
    <tr class="my-4 tr-shoppinglist">
        <td width="16.666667%" class="text-center">
            <div class="img-card">
                <span class="helper"></span>
                <img src="@if($principal_photo != ""){{asset("uploads/articles/photos/".$principal_photo)}} @else {{ asset("img/picture.png") }} @endif"
                     alt="Card image cap"
                     @if($principal_photo == "") class="no-image" @endif alt="">
            </div>
        </td>
        <td width="53.333333%">
            <p>{{$article->name}} - ${{ $article->price }}</p>
            <a href="{{url('comprar/'.$article->id)}}" class="mr-4">
                <small>Comprar Ahora</small>
            </a>
            <a href="#" class="mr-4 removeArticle">
                <small>Eliminar</small>
            </a>
            <input type="hidden" value="{{$article->id}}">

        </td>
        <td width="10%">
            <div class="input-group counter-shoppinglist">
                <button type="button" class="btn-number btn-shoppinglist"
                        data-type="minus"
                        data-field="quant[{{$key}}]">
                    <small><i class="fas fa-minus"></i></small>
                </button>
                <input type="text" name="quant[{{$key}}]"
                       class="form-control input-number input-shoppinglist"
                       value="{{ $article->cantidad }}" min="1"
                       max="{{$article->available}}"
                       data-id="{{$article->itemid}}">
                <button type="button" class="btn-number btn-shoppinglist"
                        data-type="plus"
                        data-field="quant[{{$key}}]" data-id="{{$article->itemid}}">
                    <small><i class="fas fa-plus"></i></small>
                </button>
            </div>
            <small class="available-shoppinglist mt-3 ml-3"> {{$article->available}}
                Disponible
            </small>
        </td>
        <td width="30%" class="text-right"><p class="mr-4">
                <?php $totalPrice = $totalPrice + number_format((float)($article->price * $article->cantidad), 2, '.', '');?>
                ${{ number_format((float)($article->price*$article->cantidad), 2, '.', '')}}</p>
        </td>
    </tr>

@endforeach
<tr>
    <td width="16.666667%" class="text-center"></td>
    <td width="53.333333%"></td>
    <td width="40%" class="text-right">
        <table width="100%" cellpadding="10">
            <tbody>
            <tr>
                <td width="50%"><h5 class="float-left">Precio: </h5></td>
                <td width="50%"><h5 class="float-right mr-4">
                        ${{ number_format((float)$totalPrice, 2, '.', '')}} </h5></td>
            </tr>
            <tr>
                <td width="50%"><h5 class="float-left">Envio: </h5></td>
                <td width="50%"><h5 class="float-right mr-4"> $125.00 </h5></td>
            </tr>
            <tr>
                <td width="50%"><h5 class="float-left">Total: </h5></td>
                <td width="50%"><h5 class="float-right mr-4">
                        ${{ number_format((float)($totalPrice+125), 2, '.', '')}} </h5>
                </td>
            </tr>
            <tr class="my-4" style="border-top: 1px solid #ebebeb;">
                <td width="50%"></td>
                <td width="50%" class="offset-6 col-6">
                    <a href="{{ url('comprar') }}" class="btn btn-primary float-right">Terminar
                        compra</a>
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

</tbody>

<script>
    $(document).ready(function () {

        $('.btn-number').click(function (e) {
            e.preventDefault();

            fieldName = $(this).attr('data-field');
            type = $(this).attr('data-type');
            var input = $("input[name='" + fieldName + "']");
            var currentVal = parseInt(input.val());
            if (!isNaN(currentVal)) {
                if (type == 'minus') {

                    if (currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }

                } else if (type == 'plus') {

                    if (currentVal < input.attr('max')) {
                        input.val(currentVal + 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('max')) {
                        $(this).attr('disabled', true);
                    }

                }
            } else {
                input.val(0);
            }
        });


        $('.input-number').focusin(function () {
            $(this).data('oldValue', $(this).val());
        });
        $('.input-number').change(function () {

            minValue = parseInt($(this).attr('min'));
            maxValue = parseInt($(this).attr('max'));
            valueCurrent = parseInt($(this).val());
            var list_id =  parseInt($(this).attr('data-id'));
            console.log(list_id);
            name = $(this).attr('name');
            if (valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if (valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }

            $.post("{{ url("updated/list/") }}", {id: list_id, value: valueCurrent},
                function (data, status) {
                    $('#table').empty();
                    $('#table').append(data);
                });

        });
        $(".input-number").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    });
</script>