@include('head/head')
@include('navbar/navbar')
<div class="container-fluid">
    <div class="row">
        @include('users/sidebar/sidebar')
        <div class="col-12 col-sm-12 col-md-10">
            @include('users/banner/banner')
            <div class="col-12 col-sm12 col-md-10 offset-md-1">
                <div class="row my-4">
                    <div class="col-12 col-sm-10 offset-sm-1 col-md-10 offset-md-1">
                        <h4 class="my-2 mb-4">Compras realizadas</h4>
                        <?php $purchasenumber = ""; ?>
                        @foreach($orders as $order)
                            <div class="jumbotron jumbotron-fluid"
                                 style="background: white; height: auto; border: 1px solid #e2e3e5;">
                                <div class="col-12 col-sm-12 col-md-12 mt-4">
                                    <div class="row">
                                        <h6 class="col-12 col-sm-12 col-md-6"><strong>Número de orden:</strong>{{$order->ordernumber}}</h6>
                                        <a href="{{ url('recomprar/'.$order->ordernumber) }}" class="btn btn-primary col-3 text-center d-md-block d-none col-md-3 offset-md-3">
                                            Volver a comprar
                                        </a>
                                    </div>
                                </div>
                                <hr>
                                @foreach($purchases as $purchase)
                                    <?php $principal_photo = \App\Photo::getArticlesPrimaryPhoto($purchase->id_article)?>
                                    @if($order->ordernumber == $purchase->ordernumber)
                                        <div class="container my-4">
                                            <div class="row">
                                                <div class="col-12 col-sm-12 col-md-2">
                                                    <div class="img-card">
                                                        <span class="helper"></span>
                                                        <img src="@if($principal_photo != ""){{asset("uploads/articles/photos/".$principal_photo)}} @else {{ asset("img/picture.png") }} @endif"
                                                             alt="Card image cap"
                                                             @if($principal_photo == "") class="no-image" @endif alt="">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-10">
                                                    <a href="{{ url("producto/$purchase->id_article") }}">
                                                        <strong>{{$purchase->name}} ${{$purchase->price}}</strong>
                                                    </a>
                                                    <p class="float-right myfont-10 d-md-block d-none">
                                                        Comprado el: {{ $purchase->created }}
                                                    </p>
                                                    <hr>
                                                    <p> {{$purchase->description}}</p>
                                                    <p><strong>Cantidad: {{$purchase->count}}</strong></p>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer/footer')