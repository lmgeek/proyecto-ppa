@include('head/head')
@include('navbar/navbar')
<div class="container-fluid">
    <div class="row">
        @include('users/sidebar/sidebar')
        <div class="col-12 col-sm-12 col-md-10">
            @include('users/banner/banner')
            <div class="col-12 col-sm-12 col-md-10 offset-md-1">
                <div class="row my-4">
                    <form action="{{ url("usuario/update/".$user->id) }}" class="my-4 col-12 col-sm-12 col-md-12">
                        <div class="row">
                            <h4 class="my-2 col-12 col-sm-12 col-md-12">Mi cuenta</h4>
                            <div class="col-12 col-sm-12 col-md-6 text-left">
                                <div class="col-12 mb-4">
                                    <p class="myfont-13 light-blue">Información personal</p>
                                    <hr>
                                    <small><label for="">Nombre</label></small>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">@</span>
                                        </div>
                                        <small><input type="text" class="form-control myfont-12" style="height: 100%"
                                                      value="{{$user->name}}" placeholder="" aria-label=""
                                                      aria-describedby="basic-addon1" name="name"></small>
                                    </div>
                                    <small><label for="">Apellido</label></small>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">@</span>
                                        </div>
                                        <small><input type="text" class="form-control myfont-12" style="height: 100%"
                                                      value="{{$user->lastname}}" placeholder="" aria-label=""
                                                      aria-describedby="basic-addon1" name="lastname"></small>
                                    </div>

                                    <small><label for="">DNI</label></small>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">@</span>
                                        </div>
                                        <small><input type="text" class="form-control myfont-12" style="height: 100%"
                                                      value="{{$user->dni}}" placeholder="" aria-label=""
                                                      aria-describedby="basic-addon1" name="DNI"></small>
                                    </div>

                                    <small><label for="">Fecha de nacimiento</label></small>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">@</span>
                                        </div>
                                        <small><input type="date" class="form-control myfont-12" style="height: 100%"
                                                      value="{{$user->birthday}}" placeholder="" aria-label=""
                                                      aria-describedby="basic-addon1" name="birthday"></small>
                                    </div>
                                    <br>
                                    <p class="myfont-13 light-blue">Dirección</p>
                                    <hr>
                                    <small><label for="">Direccion</label></small>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">@</span>
                                        </div>
                                        <small><input type="text" class="form-control myfont-12" style="height: 100%"
                                                      value="{{$user->address}}" placeholder="" aria-label=""
                                                      aria-describedby="basic-addon1" name="address"></small>
                                    </div>

                                    <small><label for="">Codigo Postal</label></small>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">@</span>
                                        </div>
                                        <small><input type="text" class="form-control myfont-12" style="height: 100%"
                                                      value="{{$user->zip_code}}" placeholder="" aria-label=""
                                                      aria-describedby="basic-addon1" name="zip_code"></small>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Actualizar</button>
                                </div>
                            </div>
                            <div class="col-10 offset-1 col-sm-10 col-md-6 offset-md-0 text-center">
                                <p class="myfont-13 light-blue">Foto de perfil</p>
                                <hr>
                                <div class="row">
                                    <div class="col-11 col-sm-11 col-md-6 offset-md-3">
                                        <div id="result"></div>
                                        <img id="image" src="{{asset('img/product1.jpg')}}" alt=""
                                             style="max-height: 300px;"
                                             name="photo">
                                        <div class="box">
                                            <input class="my-4 inputfile inputfile-1" type="file" name="file-1[]"
                                                   id="file-1" data-multiple-caption="{count} files selected" multiple
                                                   onchange=" readURL(this)">
                                            <label for="file-1">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17"
                                                     viewBox="0 0 20 17">
                                                    <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                                </svg>
                                                <span>Elegir archivo&hellip;</span></label>
                                        </div>
                                    </div>
                                </div>
                                <button class="my-4 btn btn-primary" type="button" id="button">Recortar y guardar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('DOMContentLoaded', function () {
        var image = document.querySelector('#image');
        var button = document.getElementById('button');
        var data = document.querySelector('#data');
        var result = document.getElementById('result');
        var cropper = new Cropper(image, {
            aspectRatio: 4 / 3,
            ready: function (event) {
                // Zoom the image to its natural size
                cropper.zoomTo(1);
            },

            crop: function (event) {
                data.textContent = JSON.stringify(cropper.getData());
            },

            zoom: function (event) {
                // Keep the image in its natural size
                if (event.detail.oldRatio === 1) {
                    event.preventDefault();
                }
            },
        });

        button.onclick = function () {
            result.innerHTML = '';
            result.appendChild(cropper.getCroppedCanvas({}));


            $("canvas").css({
                "max-height": "250px",
                "max-width": "250px",
                "border-radius": "50%",
                "margin-top": "30px",
                "margin-bottom": "50px"
            });
            cropper.getCroppedCanvas().toBlob(function (blob) {
                const formData = new FormData();

                formData.append('croppedImage', blob);

                // Use `jQuery.ajax` method
                $.ajax('{{url('usuario/userphoto')}}', {
                    method: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function () {
                        console.log('Upload success');
                        console.log(formData);
                    },
                    error: function () {
                        console.log('Upload error');
                        console.log(formData);
                    },
                });
            });
        };
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image')
                    .attr('src', e.target.result)
                    .css('maxWidth', '300px')
                    .css('maxHeight', '300px');

                var image = document.querySelector('#image');
                var data = document.querySelector('#data');
                var cropper = new Cropper(image, {
                    ready: function (event) {
                        // Zoom the image to its natural size
                        cropper.zoomTo(1);
                    },

                    zoom: function (event) {
                        // Keep the image in its natural size
                        if (event.detail.oldRatio === 1) {
                            event.preventDefault();
                        }
                    },
                });

                $('.cropper-container').remove();

                cropper.destroy();

                cropper = new Cropper(image, {
                    aspectRatio: 4 / 3,
                    ready: function (event) {
                        // Zoom the image to its natural size
                        cropper.zoomTo(1);
                    },

                    crop: function (event) {
                        data.textContent = JSON.stringify(cropper.getData());
                    },

                    zoom: function (event) {
                        // Keep the image in its natural size
                        if (event.detail.oldRatio === 1) {
                            event.preventDefault();
                        }
                    },
                });

                button.onclick = function () {
                    result.innerHTML = '';
                    result.appendChild(cropper.getCroppedCanvas({}));
                    $("canvas").css({
                        "max-height": "250px",
                        "max-width": "250px",
                        "border-radius": "50%",
                        "margin-top": "30px",
                        "margin-bottom": "50px"
                    });
                    cropper.getCroppedCanvas().toBlob(function (blob) {
                        const formData = new FormData();

                        formData.append('croppedImage', blob);

                        // Use `jQuery.ajax` method
                        $.ajax('{{url('usuario/userphoto')}}', {
                            method: "POST",
                            data: formData,
                            processData: false,
                            contentType: false,
                            success: function () {
                                console.log('Upload success');
                                console.log(formData);
                            },
                            error: function () {
                                console.log('Upload error');
                                console.log(formData);
                            },
                        });
                    });
                };


            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@include('footer/footer')