@include('head/head')
@include('navbar/navbar')

<?php \App\Record::addRecord($article->id);?>
<!------ Include the above in your HEAD tag ---------->

<div class="container-fluid">
    <div class="my-3">
        <div class="row">
            <div class="justify-content-md-center row">
                <div class="col-12 col-sm-12 col-md-8 offset-md-0">
                    <div class="row">
                        <div class="col-10 offset-1 col-sm-10 col-md-5 offset-md-0">
                            <article class="gallery-wrap">
                                <div class="img-big-wrap">
                                    {{--<div class="img">--}}
                                    <img src="@if($principal_photo != ""){{asset("uploads/articles/photos/".$principal_photo)}} @else {{ asset("img/noimage.png") }} @endif"
                                         id="img" data-zoom-image="{{asset("uploads/articles/photos/".$principal_photo)}}"
                                         @if($principal_photo == "") class="noimage" @endif>
                                    {{--</div>--}}
                                </div> <!-- slider-product.// -->

                                <div class="img-small-wrap">
                                    @if(!empty($photos))
                                        @foreach($photos as $key => $photo)
                                            <div class="item-gallery" id="{{ "photo$key" }}"
                                                 @if(isset($photo)) data-image="{{asset("uploads/articles/photos/".$photo->photo)}}"
                                                 data-zoom-image="{{asset("uploads/articles/photos/".$photo->photo)}}" @endif>
                                            </div>
                                        @endforeach
                                    @endif
                                </div> <!-- slider-nav.// -->
                            </article> <!-- gallery-wrap .end// -->
                        </div>
                        <div class="col-12 col-sm-12 col-md-7">
                            <article class="p-5">
                                <h3 class="title mb-3">{{ $article->name }}</h3>

                                <p class="price-detail-wrap">
	<span class="price h3 text-warning">
		<span class="currency">US $</span><span class="num">{{ floatval($article->price) }}</span>
	</span>
                                    {{--<span>/per kg</span>--}}
                                </p> <!-- price-detail-wrap .// -->
                                <dl class="item-property">
                                    <dt>Descrición</dt>
                                    <dd><p>{{ $article->description }} </p></dd>
                                </dl>
                                <dl class="param param-feature">
                                    <dt># Codigo</dt>
                                    <dd>{{ $article->model }}</dd>
                                </dl>  <!-- item-property-hor .// -->
                                {{--<dl class="param param-feature">--}}
                                {{--<dt>Color</dt>--}}
                                {{--<dd>Black and white</dd>--}}
                                {{--</dl>  <!-- item-property-hor .// -->--}}
                                {{--<dl class="param param-feature">--}}
                                {{--<dt>Delivery</dt>--}}
                                {{--<dd>Russia, USA, and Europe</dd>--}}
                                {{--</dl>  <!-- item-property-hor .// -->--}}

                                {{--<hr>--}}
                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <dl class="param param-inline">
                                            <dt>Cantidad:</dt>
                                            <dd>
                                                @if($article->available > 0)
                                                    <select class="form-control form-control-sm" style="width:70px;"
                                                            id="quantity">
                                                        @for($i = 1 ;  $i <= $article->available ; $i++)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                @else
                                                    <p class="font-italic myfont-12 red">Agotado</p>
                                                @endif
                                            </dd>
                                        </dl>  <!-- item-property .// -->
                                    </div> <!-- col.// -->
                                    {{--<div class="col-sm-7 col-md-7">--}}
                                    {{--<dl class="param param-inline">--}}
                                    {{--<dt>Size:</dt>--}}
                                    {{--<dd>--}}
                                    {{--<label class="form-check form-check-inline">--}}
                                    {{--<input class="form-check-input" type="radio" name="inlineRadioOptions"--}}
                                    {{--id="inlineRadio2" value="option2">--}}
                                    {{--<span class="form-check-label">SM</span>--}}
                                    {{--</label>--}}
                                    {{--<label class="form-check form-check-inline">--}}
                                    {{--<input class="form-check-input" type="radio" name="inlineRadioOptions"--}}
                                    {{--id="inlineRadio2" value="option2">--}}
                                    {{--<span class="form-check-label">MD</span>--}}
                                    {{--</label>--}}
                                    {{--<label class="form-check form-check-inline">--}}
                                    {{--<input class="form-check-input" type="radio" name="inlineRadioOptions"--}}
                                    {{--id="inlineRadio2" value="option2">--}}
                                    {{--<span class="form-check-label">XXL</span>--}}
                                    {{--</label>--}}
                                    {{--</dd>--}}
                                    {{--</dl>  <!-- item-property .// -->--}}
                                    {{--</div> <!-- col.// -->--}}
                                </div> <!-- row.// -->
                                {{--<hr>--}}
                                <div class="row my-3">
                                    <a href="{{url('comprar/'.$article->id)}}"
                                       class="btn btn btn-primary text-uppercase col-5 ml-3 myfont-11 @if($article->available <= 0) disabled @endif">
                                        Comprar </a>
                                    <a href="#"
                                       class="btn btn btn-outline-primary text-uppercase col-5  ml-3 myfont-11 @if($article->available <= 0) disabled @endif"
                                       id="car">
                                        <i class="fas fa-shopping-cart"></i> Carrito
                                    </a>
                                </div>
                            </article> <!-- card-body.// -->
                        </div> <!-- col.// -->
                        <div class="col-10 offset-1 col-sm-10 offset-sm-1 col-md-12 offset-md-0 ">
                            <p class="myfont-14">Articulos relacionados</p>
                            <div class="row justify-content-md-center">
                                    @foreach($articlesRelatedCategory as $article)
                                        <?php $principal_photo = \App\Photo::getArticlesPrimaryPhoto($article->id);?>
                                        <div class=" col col-12 col-sm-6 col-md-3 margin">
                                            <div class="card">
                                                <div class="card-img-top">
                                                    <span class="helper"></span>
                                                    <img src="@if($principal_photo != ""){{asset("uploads/articles/photos/".$principal_photo)}} @else {{ asset("img/picture.png") }} @endif"
                                                         alt="Card image cap" @if($principal_photo == "") class="no-image" @endif>
                                                </div>
                                                @if($article->id_promotion != 0)
                                                    <?php $promotion = new \App\Promotion(); $promotion_name = $promotion->getPromotionArticle($article->id_promotion) ?>
                                                    <div class="tag-descuento"><span>{{  $promotion_name }}%</span></div>
                                                @endif
                                                @if($article->new == 1)
                                                    <div class="@if($article->id_promotion == 0) tag @else tag-nuevo @endif">
                                                        <span>NUEVO</span>
                                                    </div>
                                                @endif
                                                <div class="card-body">

                                                    <a href="{{ url("producto/$article->id") }}"><p
                                                                class="card-text">{{ $article->name }}</p>
                                                    </a>
                                                    <p class="card-title">$ {{ $article->price }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                            </div>
                        </div>
                        {{--<div class="col-md-12 col-sm-12">--}}
                        {{--<p class="myfont-14">Preguntas</p>--}}
                        {{--<div class="col-md-8 col-sm-8">--}}
                        {{--<form action="">--}}
                        {{--<textarea rows="4" columnes="6" class="form-control">Haz tu pregunta aca</textarea>--}}
                        {{--<div style="float: right;">--}}
                        {{--<button class="btn btn-primary my-3" type="submit">Enviar</button>--}}
                        {{--</div>--}}
                        {{--</form>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div> <!-- row.// -->
    </div>
</div> <!-- card.// -->
</div>

<script>

    $(document).ready(function () {
        $('#car').on('click', function () {
            var quantity = $('#quantity').val();
            $.ajax('{{url('addtocar')}}', {
                method: "POST",
                data: {'id_article': "{{$article->id}}", 'quantity': quantity},
                success: function (response) {
                    $.toaster({message: response, title: 'Mensaje', priority: 'success', settings: {'timeout': 10000}});
                },
                error: function () {
                    $.toaster({
                        message: "No se puedo agregar su compra al carrito",
                        title: 'Mensaje',
                        priority: 'danger'
                    });
                },
            });

        });
        @if(isset($status))
        $.toaster({message: '{{$status}}', title: 'Mensaje', priority: 'success'});
        @endif

        @if($principal_photo != null)
        $('.img').css('background', "url({{asset("uploads/articles/photos/".$principal_photo)}}) no-repeat center center");
        @else
        $('.img').css('background', "url({{ asset('img/no-image.jpg') }}) no-repeat center center");
        @endif
        $('.img').css('background-size', "contain");
        $('.img').css('height', "450px")

        @if(!empty($photos))
        @foreach($photos as $key => $photo)
        $('#{{"photo$key"}}').css('background', "url({{ asset("uploads/articles/photos/".$photo->photo) }}) no-repeat center center");
        $('#{{"photo$key"}}').css('background-size', "contain");
        $('#{{"photo$key"}}').css('cursor', "pointer");

        $('#{{"photo$key"}}').click(function () {
            $('#img').attr('src', "{{ env('IMAGES_URL').$photo->photo }}");
            $('#img').attr('data-zoom-image', "{{ asset("uploads/articles/photos/".$photo->photo) }}");

            $('.zoomContainer').remove();
            $('#img').removeData('elevateZoom');
            $('#img').attr('src', $('#{{"photo$key"}}').data('image'));
            $('#img').data('zoom-image', $('#{{"photo$key"}}').data('image'));
            $('#img').elevateZoom({
                zoomType: "inner",
                cursor: "crosshair"
            });
        });
        @endforeach
        @endif
    });
</script>
@include('footer/footer')