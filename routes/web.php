<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/busqueda', 'ArticleController@search');

Route::get('/producto/{id}', 'ArticleController@show');

//Admin
Route::get('/adminxxs', 'AdminAuth\AuthController@loginPage');
Route::post('/adminxxs/login', 'AdminAuth\AuthController@authenticate');
Route::get('/adminxxs/logout', 'AdminAuth\AuthController@logOut');

//Usuarios
Route::get('/login/usuario', 'Auth\LoginController@loginPage')->name('login');
Route::post('/login', 'Auth\LoginController@authenticate');
Route::get('/logout/usuario', 'Auth\LoginController@logOut');

Route::get('/register', 'UserController@create');
Route::post('/save/user', 'UserController@store');



Route::group(['middleware' => ['auth']], function () {
    Route::get('usuario/perfil', 'ProfileController@show');
    Route::get('usuario/config', 'ProfileController@edit');
    Route::get('usuario/update/{id}', 'ProfileController@update');
    Route::get('usuario/compras', 'ProfileController@purchasesShow');
    Route::get('usuario/historial', 'ProfileController@recordsShow');
    Route::get('usuario/carrito', 'ProfileController@shoppingListShow');
    Route::post('usuario/userphoto', 'ProfileController@updatePhotoUser');
    Route::post('updated/list', 'ShoppinglistController@setValueList');
    Route::post('addtocar', 'ShoppinglistController@addToShoppingList');
    Route::post('removefromcar', 'ShoppinglistController@RemoveFromShoppingList');
    Route::post('changequantityitem', 'ShoppinglistController@ChangeQuantityItem');
    Route::get('comprar', 'CheckoutController@showOnShoppinglist');
    Route::get('recomprar/{ordernumber}', 'CheckoutController@showBuyAgain');
    Route::get('comprar/{id}', 'CheckoutController@showOnSingle');
    Route::post('registerPurchases', 'CheckoutController@registerPurchases');
});



//Admin Panel
Route::group(['middleware' => ['admin']], function () {
    Route::get('/adminxxs/panel', 'AdminController@index');

//Articles
    Route::get('/adminxxs/articles', 'ArticleController@index');
    Route::get('/adminxxs/articles/new', 'ArticleController@create');
    Route::post('/save/article', 'ArticleController@store');
    Route::get('/adminxxs/article/{id}', 'ArticleController@edit');
    Route::post('update/article/{id}', 'ArticleController@update');
    Route::get('destroy/article/{id}', 'ArticleController@destroy');
    Route::get('deactive/article/{id}', 'ArticleController@deactive');
    Route::get('active/article/{id}', 'ArticleController@active');

//Categories
    Route::get('/adminxxs/categories', 'CategoryController@index');
    Route::get('/adminxxs/categories/new', 'CategoryController@create');
    Route::post('/save/category', 'CategoryController@store');
    Route::get('/adminxxs/category/{id}', 'CategoryController@edit');
    Route::post('update/category/{id}', 'CategoryController@update');
    Route::get('destroy/category/{id}', 'CategoryController@destroy');
    Route::get('deactive/category/{id}', 'CategoryController@deactive');
    Route::get('active/category/{id}', 'CategoryController@active');

//Subcategories
    Route::get('/adminxxs/subcategories', 'SubcategoryController@index');
    Route::get('/adminxxs/subcategories/new', 'SubcategoryController@create');
    Route::post('/save/subcategory', 'SubcategoryController@store');
    Route::get('/adminxxs/subcategory/{id}', 'SubcategoryController@edit');
    Route::post('update/subcategory/{id}', 'SubcategoryController@update');
    Route::get('destroy/subcategory/{id}', 'SubcategoryController@destroy');
    Route::get('deactive/subcategory/{id}', 'SubcategoryController@deactive');
    Route::get('active/subcategory/{id}', 'SubcategoryController@active');


//Transports
    Route::get('/adminxxs/transports', 'TransportController@index');
    Route::get('/adminxxs/transports/new', 'TransportController@create');
    Route::post('/save/transport', 'TransportController@store');
    Route::get('/adminxxs/transport/{id}', 'TransportController@edit');
    Route::post('update/transport/{id}', 'TransportController@update');
    Route::get('destroy/transport/{id}', 'TransportController@destroy');
    Route::get('deactive/transport/{id}', 'TransportController@deactive');
    Route::get('active/transport/{id}', 'TransportController@active');

//Promotions
    Route::get('/adminxxs/promotions', 'PromotionController@index');
    Route::get('/adminxxs/promotion/new', 'PromotionController@create');
    Route::post('/save/promotion', 'PromotionController@store');
    Route::get('/adminxxs/promotion/{id}', 'PromotionController@edit');
    Route::post('update/promotion/{id}', 'PromotionController@update');
    Route::get('destroy/promotion/{id}', 'PromotionController@destroy');
    Route::get('deactive/promotion/{id}', 'PromotionController@deactive');
    Route::get('active/promotion/{id}', 'PromotionController@active');

//User
    Route::get('/adminxxs/users', 'AdminController@allUsers');
    Route::get('destroy/user/{id}', 'UserController@destroy');
    Route::get('deactive/user/{id}', 'UserController@deactive');
    Route::get('active/user/{id}', 'UserController@active');

    //Ordenes
    Route::get('adminxxs/orders', 'OrdersController@index');

    //Config Layout
    Route::get('adminxxs/config/layout', 'HomeController@changeLayout');
    Route::post('adminxxs/loadmore', 'HomeController@addMoreItems');
    Route::post('adminxxs/save/config/layout', 'HomeController@saveArticleIndex');
});
