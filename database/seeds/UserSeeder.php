<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Alejandro",
            'lastname' => "Jimenez",
            'username' => "Albattle",
            'dni' => "95756293",
            'email' => "alejandrojimenezaajr@gmail.com",
            'address' => "Elias bedoya 160 4A",
            'zip_code' => "1408",
            'password' => \Hash::make("ale21jim"),
            'hash' => uniqid(),
        ]);
    }
}
