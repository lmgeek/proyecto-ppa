<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name' => "Alejandro",
            'lastname' => "Jimenez",
            'email' => "alejandrojimenezaajr@gmail.com",
            'password' => \Hash::make('ale21jim'),
            'username' => "albattle",
            'active' => 1,
            'delete' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

        ]);
    }
}
