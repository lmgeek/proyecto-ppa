<?php

use Illuminate\Database\Seeder;

class categorySubcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            DB::table('categories_subcategories')->insert([
                'id_category' =>$i,
                'id_subcategory' => rand(1, 10)
            ]);
        }
    }
}
