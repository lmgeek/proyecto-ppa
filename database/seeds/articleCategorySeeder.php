<?php

use Illuminate\Database\Seeder;

class articleCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 250; $i++) {
            DB::table('articles_categories')->insert([
                'id_article' => $i,
                'id_category' => rand(1, 10),
                'id_subcategory' => rand(1, 10)
            ]);
        }
    }
}
