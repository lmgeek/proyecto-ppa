<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

class SubcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        for($i = 0; $i < 10 ; $i++){
            DB::table('subcategories')->insert([
                'name' => $faker->name,
                'description' => $faker->text,
                'active' => 1,
                'delete' => 0,
            ]);
        }
    }
}
