<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        for($i = 0; $i < 250 ; $i++){
            $rand = rand(1,100);
            DB::table('articles')->insert([
                'name' => $faker->name(),
                'model' => $faker->ean8(),
                'price' => rand(1, 1000),
                'quantity' => $rand,
                'available' => $rand,
                'description' => $faker->text,
                'due_date' => $faker->date,
                'new' => rand(0, 1),
                'id_promotion' =>  rand(1,10),
                'rating' => rand(1, 5),
                'active' => 1,
                'delete' => 0,
            ]);
        }
    }
}
