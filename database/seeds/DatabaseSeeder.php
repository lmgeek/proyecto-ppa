<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AdminSeeder::class,
            UserSeeder::class,
            ArticleSeeder::class,
            CategorySeeder::class,
            SubcategorySeeder::class,
            PromotionSeeder::class,
            articleCategorySeeder::class,
            articleSubcategorySeeder::class,
            categorySubcategorySeeder::class,
            UserPurchasesSeeder::class,
            UserRecordsSeeder::class,
            ShoppingList::class,
            ArticleIndexSeeder::class,
        ]);
    }
}
