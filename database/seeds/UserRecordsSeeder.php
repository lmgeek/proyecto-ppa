<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserRecordsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 15 ; $i++){
            DB::table('users_records')->insert([
                'id_user' => 1,
                'id_article' => $i,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }
    }
}
