<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ShoppingList extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 4; $i++) {
            DB::table('shoppinglist')->insert([
                'id_user' => 1,
                'id_article' => ($i * 3),
                'quantity' => ($i * 2),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }
    }
}
