<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

class PromotionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        for($i = 0; $i < 10 ; $i++){
            DB::table('promotions')->insert([
                'name' => rand(10,60),
                'description' => $faker->text,
                'active' => 1,
                'delete' => 0,
            ]);
        }
    }
}
