<?php

use Illuminate\Database\Seeder;

class ArticleIndexSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 8 ; $i++){
            DB::table('articles_index')->insert([
                'id_article' => $i,
                'position' => 1
            ]);
        }
        for($i = 9; $i <= 9 ; $i++){
            DB::table('articles_index')->insert([
                'id_article' => $i,
                'position' => 2
            ]);
        }
        for($i = 10; $i <= 17 ; $i++){
            DB::table('articles_index')->insert([
                'id_article' => $i,
                'position' => 3
            ]);
        }
    }
}
