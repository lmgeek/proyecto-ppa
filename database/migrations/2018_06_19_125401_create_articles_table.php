<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('model', 255);
            $table->float('price');
            $table->integer('quantity')->default(1);
            $table->integer('available');
            $table->text('description');
            $table->date('due_date');
            $table->tinyInteger('new');
            $table->integer('id_promotion');
            $table->tinyInteger('active')->default(1);
            $table->tinyInteger('delete')->default(0);
            $table->integer('rating')->default(1);
            $table->integer('views')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
