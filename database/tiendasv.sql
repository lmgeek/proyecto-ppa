-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-11-2018 a las 13:22:04
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tiendasv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `delete` tinyint(4) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `admins`
--

INSERT INTO `admins` (`id`, `name`, `lastname`, `username`, `email`, `password`, `active`, `delete`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Alejandro', 'Jimenez', 'albattle', 'alejandrojimenezaajr@gmail.com', '$2y$10$5eHCDHNSHtq58Yg9h4MtSuKbEGAn70DnFeM5c2zCVakBP.jTOkcy2', 1, 0, NULL, '2018-10-03 21:42:28', '2018-10-03 21:42:28', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `available` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `due_date` date NOT NULL,
  `new` tinyint(4) NOT NULL,
  `id_promotion` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `delete` tinyint(4) NOT NULL DEFAULT '0',
  `rating` int(11) NOT NULL DEFAULT '1',
  `views` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `articles`
--

INSERT INTO `articles` (`id`, `name`, `model`, `price`, `quantity`, `available`, `description`, `due_date`, `new`, `id_promotion`, `active`, `delete`, `rating`, `views`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Dr. Emory Kozey', '11682035', 308.00, 100, 34, 'Perspiciatis accusamus illo libero placeat. Optio aspernatur voluptatibus velit ullam praesentium a debitis. Sed est laborum impedit cum sit. Qui laborum repellendus magni nobis fugit culpa eum.', '1995-09-18', 1, 7, 1, 0, 4, 0, NULL, '2018-10-03 22:58:49', NULL),
(2, 'Norris Rogahn', '43488193', 459.00, 100, 65, 'Culpa dolor saepe qui aut. Nostrum impedit corrupti non ad autem eaque facere. Sit molestias error itaque omnis architecto voluptates aperiam commodi. Praesentium et rerum eos totam qui sunt illum.', '1995-11-16', 1, 8, 1, 0, 5, 0, NULL, '2018-10-03 22:58:55', NULL),
(3, 'Ms. Gisselle Purdy', '04255178', 485.00, 100, 55, 'Libero nostrum reprehenderit saepe ad rerum. Vel temporibus facilis exercitationem voluptas nihil. Molestiae aliquid et sapiente sint aut sit dolorem.', '1976-01-19', 1, 7, 1, 0, 1, 0, NULL, '2018-10-03 22:59:00', NULL),
(4, 'Chris Jacobi DVM', '25627848', 598.00, 100, 47, 'Rerum ipsa ab quis dolores nihil quia. Vitae rem suscipit dolorem numquam excepturi deserunt atque sit. Ab quibusdam adipisci consequatur unde.', '1985-09-11', 1, 9, 1, 0, 1, 0, NULL, '2018-10-03 22:59:07', NULL),
(5, 'Cecelia Turner', '77630735', 815.00, 100, 42, 'In voluptas cum qui. Quia qui natus illum minus quia harum nihil vitae. Aut assumenda ut et soluta consequuntur.', '2018-04-10', 1, 6, 1, 0, 4, 0, NULL, '2018-10-03 22:59:18', NULL),
(6, 'Prof. Kaelyn Mayer III', '26688336', 119.00, 54, 54, 'Molestiae natus ea et vel nobis est molestiae. Delectus velit doloribus quo enim. At laudantium est et et earum. Sed necessitatibus commodi ea facilis dolor.', '2009-05-07', 0, 10, 1, 0, 5, 0, NULL, NULL, NULL),
(7, 'Dr. Raul Crona', '96172919', 467.00, 92, 92, 'Est animi vero sit voluptatem. Esse distinctio repellat voluptas voluptatum. Vel autem enim consequatur modi voluptatem qui consequatur.', '2000-10-02', 1, 10, 1, 0, 5, 0, NULL, NULL, NULL),
(8, 'Andreane Jerde', '84616869', 268.00, 70, 70, 'Sint sit nam ea totam. Rerum dignissimos et iste illo optio nam qui. At ut repellat et quaerat debitis.', '1993-11-21', 1, 8, 1, 0, 1, 0, NULL, NULL, NULL),
(9, 'Dandre Stoltenberg Sr.', '85327726', 453.00, 22, 22, 'Necessitatibus odio aliquam praesentium adipisci vel. Aut est amet perferendis voluptas.', '2018-08-31', 0, 2, 1, 0, 1, 0, NULL, NULL, NULL),
(10, 'Dr. Jordi McGlynn MD', '18903317', 761.00, 16, 16, 'Beatae et doloremque beatae ad. Vel fuga dolor voluptatem quod aut. Impedit molestias veniam libero. Veritatis est incidunt eos cupiditate ut totam rerum quia.', '1999-05-07', 0, 3, 1, 0, 4, 0, NULL, NULL, NULL),
(11, 'Gerald Becker', '65702918', 377.00, 49, 49, 'Suscipit sint dolorem sint velit inventore et est dolores. Molestiae in id libero beatae. Quae asperiores explicabo voluptatum autem autem.', '1987-11-23', 0, 7, 1, 0, 4, 0, NULL, NULL, NULL),
(12, 'Ruby Hammes', '99361754', 530.00, 21, 21, 'Voluptatibus alias saepe ullam officia similique voluptas dolorem. Hic et totam vero et quibusdam cumque dolores minus. Reprehenderit rerum perspiciatis itaque aut. Minus et fugiat doloribus.', '2017-08-07', 1, 7, 1, 0, 1, 0, NULL, NULL, NULL),
(13, 'Callie Kuhlman MD', '56111019', 190.00, 58, 58, 'Nostrum facilis aspernatur et animi officia sit. Est laudantium dolore nam est commodi quia. Neque excepturi expedita necessitatibus.', '2005-12-17', 1, 3, 1, 0, 4, 0, NULL, NULL, NULL),
(14, 'Miss Elza McDermott', '36645800', 479.00, 97, 97, 'Consequatur necessitatibus minima deleniti sit. Qui ab sunt expedita. Vitae molestiae quis debitis qui est. Natus pariatur atque perspiciatis nulla quo. Sequi perferendis qui dolor aut non sed fugit.', '1980-06-09', 0, 7, 1, 0, 2, 0, NULL, NULL, NULL),
(15, 'Akeem White', '09758865', 934.00, 85, 85, 'Et dolorem nihil velit fugit culpa. Praesentium vitae est voluptatem. Cum molestiae qui veritatis reprehenderit consequuntur saepe.', '1987-03-23', 1, 4, 1, 0, 4, 0, NULL, NULL, NULL),
(16, 'Iliana Kirlin', '10532904', 228.00, 20, 20, 'Qui minima beatae vel voluptatum et. Ab enim quis modi incidunt sed molestiae aut. Deleniti quis nihil labore in fugiat. Et a excepturi omnis non aut voluptatem.', '1992-10-09', 0, 4, 1, 0, 1, 0, NULL, NULL, NULL),
(17, 'Mikayla Wolff', '98807642', 495.00, 85, 85, 'Aut autem autem ea. Qui maiores nulla eos quisquam debitis. Impedit nisi reiciendis sunt accusamus qui molestiae.', '2012-10-07', 1, 1, 1, 0, 1, 0, NULL, NULL, NULL),
(18, 'Rolando Wolf', '55576710', 867.00, 95, 95, 'Aut iure veritatis impedit dolor nihil modi alias. Quidem voluptas ipsam ut fuga tenetur perspiciatis sit cumque. Quasi beatae ea consequatur occaecati veritatis velit.', '1989-12-24', 0, 4, 1, 0, 2, 0, NULL, NULL, NULL),
(19, 'Dr. Mikel Ullrich II', '17997683', 355.00, 11, 11, 'Inventore praesentium laudantium id culpa eius autem. Magni vel illo recusandae autem molestias dolorem. Placeat unde sequi omnis praesentium. Minus accusamus tempore nisi sit voluptatum omnis velit.', '1974-10-23', 0, 8, 1, 0, 1, 0, NULL, NULL, NULL),
(20, 'Mohammad Price', '58494875', 705.00, 40, 40, 'Vel dignissimos voluptatum quasi. Animi et labore et reprehenderit officiis aut sequi quo. Quas facere omnis nesciunt vel id cupiditate aliquid.', '1979-01-13', 0, 9, 1, 0, 5, 0, NULL, NULL, NULL),
(21, 'Dr. Karson Wolf IV', '77111982', 338.00, 86, 86, 'Est voluptas aut eum neque impedit laborum. Est quo eius minima et officia. Fugiat et vero hic beatae modi non. Quod minima suscipit quam dolor consequatur at voluptatem.', '2015-09-29', 1, 2, 1, 0, 4, 0, NULL, NULL, NULL),
(22, 'Marty Lowe', '04592020', 19.00, 56, 56, 'Illum culpa hic libero aperiam voluptatem eos. Amet fugiat sapiente ut exercitationem omnis. Saepe reiciendis sed facere et. Commodi consectetur architecto voluptates error ut laborum.', '1978-07-05', 1, 2, 1, 0, 2, 0, NULL, NULL, NULL),
(23, 'Marcelina Spencer', '99054588', 361.00, 84, 84, 'Similique id nihil enim. Porro exercitationem iusto et et quia. Sit et praesentium voluptatem non dignissimos non qui.', '2014-08-31', 0, 6, 1, 0, 2, 0, NULL, NULL, NULL),
(24, 'Oral Purdy', '31784641', 805.00, 3, 3, 'Sapiente debitis non facilis perferendis doloremque enim autem sint. Excepturi adipisci voluptate omnis voluptas. Unde id commodi ut quas. Est expedita vel cumque voluptatem.', '1980-07-29', 1, 9, 1, 0, 4, 0, NULL, NULL, NULL),
(25, 'Miss Elisha Trantow', '58474778', 187.00, 97, 97, 'Quisquam illo nam aut officia qui qui. Sunt autem quis modi dolores. In doloremque et aut nisi.', '1998-11-01', 0, 10, 1, 0, 3, 0, NULL, NULL, NULL),
(26, 'Merlin Moen', '44295189', 528.00, 76, 76, 'Aut debitis esse et ipsa voluptates. Ipsa natus laborum laudantium. Assumenda at veritatis ex molestiae incidunt possimus et et.', '1988-10-05', 1, 6, 1, 0, 1, 0, NULL, NULL, NULL),
(27, 'Mr. Nils Rosenbaum', '62568159', 170.00, 15, 15, 'Vel aut dignissimos enim eveniet. Cupiditate molestiae iste debitis rerum minus. Sint harum voluptatem sit tempore voluptatem sit. Soluta vel explicabo quia dicta voluptatem.', '1980-12-25', 1, 7, 1, 0, 4, 0, NULL, NULL, NULL),
(28, 'Aron Cartwright', '82182373', 872.00, 80, 80, 'Nihil possimus officia voluptate culpa. Doloremque reiciendis ea libero error magnam architecto mollitia.', '2004-04-25', 0, 3, 1, 0, 3, 0, NULL, NULL, NULL),
(29, 'Fae Schowalter', '57695013', 10.00, 1, 1, 'Sit et voluptatum et. Iste at odio sint aut. Aut magni culpa animi suscipit placeat numquam reiciendis.', '2000-01-30', 0, 1, 1, 0, 3, 0, NULL, NULL, NULL),
(30, 'Dessie Wolff', '95746753', 488.00, 4, 4, 'Cupiditate iste nulla id ut voluptatem. Velit voluptatem vel veritatis. Quos est est eos officia.', '2006-04-23', 0, 4, 1, 0, 3, 0, NULL, NULL, NULL),
(31, 'Jerrold Becker', '79986113', 500.00, 64, 64, 'Eos vel et debitis similique. Laudantium repellendus quia placeat similique non sunt excepturi ut. Dolorem commodi vel dolor qui qui eos aut.', '1987-11-25', 0, 5, 1, 0, 3, 0, NULL, NULL, NULL),
(32, 'Henderson Powlowski II', '60272492', 749.00, 94, 94, 'Totam quia eligendi hic. Odio eius eos veniam ipsa. Et voluptate laborum corrupti dolorem occaecati qui ut.', '1977-07-14', 0, 9, 1, 0, 5, 0, NULL, NULL, NULL),
(33, 'Sean Kuhlman', '12232925', 276.00, 33, 33, 'Provident at delectus deserunt incidunt non rem asperiores. Consequatur quo dolor praesentium veritatis ex qui. Animi ex consectetur ipsum doloribus. Minus iusto sed beatae culpa quis necessitatibus.', '1999-10-23', 0, 8, 1, 0, 5, 0, NULL, NULL, NULL),
(34, 'Jeanie Roob', '13133603', 494.00, 35, 35, 'Architecto ad minus quos beatae reiciendis dicta. Delectus est molestiae possimus amet. Ipsam error et in ut velit recusandae. Dolor commodi quas ipsum sit.', '1984-05-28', 1, 9, 1, 0, 1, 0, NULL, NULL, NULL),
(35, 'Christian Mohr', '33232027', 570.00, 38, 38, 'Quae delectus commodi quibusdam in rerum eligendi amet. Maxime tenetur aperiam asperiores culpa quis veniam. Ullam aspernatur eveniet laudantium sint.', '1994-06-13', 1, 1, 1, 0, 3, 0, NULL, NULL, NULL),
(36, 'Joshua Ratke', '23901469', 781.00, 17, 17, 'Voluptatem deleniti aut ratione veritatis eius. Saepe cumque dignissimos minus recusandae rerum optio. Itaque corporis vitae exercitationem architecto quam labore.', '2014-11-29', 1, 6, 1, 0, 1, 0, NULL, NULL, NULL),
(37, 'Miss Cheyanne Stoltenberg MD', '42169604', 955.00, 78, 78, 'Itaque at et qui sapiente. Quasi fugit eum qui beatae assumenda ipsam vero. Quo vel id mollitia et blanditiis consequatur.', '1987-11-16', 0, 7, 1, 0, 5, 0, NULL, NULL, NULL),
(38, 'Shirley Gorczany', '57295800', 637.00, 17, 17, 'Quos fugiat et voluptatibus autem. Atque neque quos veritatis error. Doloribus inventore ipsum qui. Aperiam quae perspiciatis voluptas molestiae nisi.', '2010-07-23', 0, 9, 1, 0, 5, 0, NULL, NULL, NULL),
(39, 'Gail Fay', '69674198', 99.00, 98, 98, 'Ut consequuntur sit error. Ipsam et autem repellat adipisci error amet iure. Voluptatibus culpa saepe repellendus corrupti voluptatem itaque. Nulla aut minus nihil laudantium at soluta harum.', '1999-07-19', 1, 8, 1, 0, 2, 0, NULL, NULL, NULL),
(40, 'Abraham Pfeffer Sr.', '61181809', 810.00, 70, 70, 'Consequuntur minus sit ut facere quod repellat. Saepe aut laudantium in eaque aliquid. Tempora labore maxime et dicta. Labore voluptate sit excepturi at aperiam itaque occaecati.', '1995-11-11', 0, 2, 1, 0, 1, 0, NULL, NULL, NULL),
(41, 'Prof. Ron Veum', '80256571', 514.00, 69, 69, 'Officia itaque enim iusto. Dolorem qui dolores corporis non aut consequatur. Nulla ut ut officiis dolorem quibusdam qui non.', '1990-04-08', 0, 2, 1, 0, 2, 0, NULL, NULL, NULL),
(42, 'Elaina Schroeder', '68504472', 514.00, 46, 46, 'Enim quo fuga ut modi et doloremque quia. Debitis enim dicta reprehenderit in. Omnis delectus voluptatem sit cumque quia velit corrupti eveniet. Nemo officiis harum dolor eum tempora doloribus harum.', '1986-09-22', 1, 9, 1, 0, 1, 0, NULL, NULL, NULL),
(43, 'Dr. Hardy Zulauf', '90069895', 306.00, 54, 54, 'Deserunt saepe dolor quos quasi illo eos ad. Non eum voluptatem expedita qui laboriosam.', '2011-07-13', 0, 8, 1, 0, 5, 0, NULL, NULL, NULL),
(44, 'Dangelo Mueller', '05116553', 182.00, 24, 24, 'Placeat cumque consequatur voluptas explicabo ducimus. Reprehenderit saepe impedit exercitationem tenetur molestiae laudantium repudiandae. Ut mollitia at alias minima.', '1985-01-23', 0, 6, 1, 0, 1, 0, NULL, NULL, NULL),
(45, 'Hipolito Armstrong', '96339824', 923.00, 91, 91, 'Maiores assumenda nemo ea voluptatem rem omnis delectus. Exercitationem voluptatibus qui non quis sunt nisi iure.', '2017-06-28', 0, 8, 1, 0, 3, 0, NULL, NULL, NULL),
(46, 'Alverta Corkery', '76611216', 216.00, 69, 69, 'Sed qui distinctio explicabo ut sit rem voluptatem. Minus atque eaque illo itaque eos modi. Aut sunt quae nostrum assumenda dolorem at. Nostrum sint blanditiis cupiditate maiores nihil et.', '2001-12-28', 0, 6, 1, 0, 5, 0, NULL, NULL, NULL),
(47, 'Norbert Barton V', '36260409', 909.00, 64, 64, 'Placeat ipsam alias et alias praesentium. Dignissimos expedita facilis ut aut est dolore quidem. Dicta facere sed qui.', '1991-01-06', 0, 3, 1, 0, 2, 0, NULL, NULL, NULL),
(48, 'Beryl Maggio', '14511967', 739.00, 3, 3, 'Ut laborum illum optio dicta praesentium sint. Repellat est assumenda id provident. Nihil illum autem non.', '2004-09-08', 0, 5, 1, 0, 3, 0, NULL, NULL, NULL),
(49, 'Prof. Stewart Jakubowski', '06636401', 651.00, 78, 78, 'Non quia dignissimos quo sit. Quos et occaecati dolorem quia. Eaque at nesciunt nesciunt qui aut. Dolore in nostrum sit eos.', '2011-12-15', 0, 9, 1, 0, 2, 0, NULL, NULL, NULL),
(50, 'Wilfrid Greenholt', '95605227', 339.00, 84, 84, 'Molestiae nam suscipit pariatur deserunt pariatur repellat. Molestiae dolores quis ut numquam. Voluptatem illum explicabo ducimus eius mollitia provident dolorem.', '1996-11-26', 0, 4, 1, 0, 4, 0, NULL, NULL, NULL),
(51, 'Devonte Goodwin DVM', '58419717', 128.00, 37, 37, 'Molestiae ullam maxime itaque quisquam. Voluptas dignissimos corporis impedit. Recusandae sed nobis minima quae.', '1986-02-23', 1, 4, 1, 0, 5, 0, NULL, NULL, NULL),
(52, 'Prof. Dino Hagenes', '65568644', 825.00, 78, 78, 'Nam neque expedita accusantium quisquam autem. Sed cupiditate nihil eum saepe qui fuga doloribus. Autem quia saepe aut temporibus blanditiis aut. Voluptatum unde cum eum unde et quisquam magnam.', '1990-01-21', 1, 10, 1, 0, 4, 0, NULL, NULL, NULL),
(53, 'Chester Hansen', '07184611', 665.00, 16, 16, 'Earum quod et modi vel voluptatum voluptatum. Nihil saepe dicta consequatur tempore. Dolorem accusamus libero accusantium non maxime. Aut id sed fuga nostrum et. Suscipit perspiciatis qui et.', '1998-09-09', 0, 1, 1, 0, 5, 0, NULL, NULL, NULL),
(54, 'Miss Emma Huel IV', '62906975', 975.00, 37, 37, 'Minus ipsum et enim aut molestias sit. Qui autem porro laborum soluta pariatur. Magnam laudantium culpa dolores consequatur accusantium quo est dolores.', '1993-05-01', 0, 1, 1, 0, 2, 0, NULL, NULL, NULL),
(55, 'Davion Kertzmann PhD', '07271854', 680.00, 34, 34, 'Accusantium sint et est quibusdam aut. Qui quas id ut rerum pariatur. Et nisi non dolorum.', '2007-10-12', 1, 7, 1, 0, 1, 0, NULL, NULL, NULL),
(56, 'Dr. Glennie Fay', '59610113', 284.00, 71, 71, 'Est consequuntur nobis iste quae. Quo aspernatur maxime omnis vitae necessitatibus ipsa placeat. Et consequatur non quia voluptatum consequuntur in.', '2016-04-11', 0, 7, 1, 0, 3, 0, NULL, NULL, NULL),
(57, 'Dr. Demarco Schaefer', '91147615', 436.00, 37, 37, 'Assumenda voluptas eligendi aliquid ut consequatur nemo. Hic quasi dicta qui assumenda. Porro autem veniam voluptas.', '1970-08-10', 0, 7, 1, 0, 3, 0, NULL, NULL, NULL),
(58, 'Shanel Christiansen', '60572820', 718.00, 25, 25, 'Voluptatem iusto quis voluptate voluptates quos quia odio. Unde sed voluptatibus eos. Quo reiciendis dolorem provident quis ipsa et consequuntur.', '1974-11-20', 0, 1, 1, 0, 3, 0, NULL, NULL, NULL),
(59, 'Chandler Lueilwitz', '97157045', 786.00, 64, 64, 'Commodi odio doloribus assumenda eum. Mollitia soluta explicabo modi iure. Nostrum neque ea repudiandae velit vel. Sed qui libero blanditiis et itaque quo.', '2006-04-19', 1, 9, 1, 0, 2, 0, NULL, NULL, NULL),
(60, 'Maudie Dickens', '49339628', 461.00, 53, 53, 'Adipisci rerum dolorum non officiis odit voluptas vel aut. Autem et at saepe. Voluptatem suscipit error pariatur ratione est. Quis voluptas id et nostrum praesentium veniam et.', '2012-11-06', 1, 6, 1, 0, 5, 0, NULL, NULL, NULL),
(61, 'Mr. Shayne Kirlin PhD', '93725798', 931.00, 27, 27, 'Eveniet non cupiditate consectetur quo facilis. Est ut voluptas temporibus praesentium. Dicta ut repudiandae voluptatem vel quia.', '1991-10-11', 0, 3, 1, 0, 2, 0, NULL, NULL, NULL),
(62, 'Danial Roberts', '97735519', 663.00, 94, 94, 'Expedita minima aut non ut. Rerum eaque eum quod est. Quae tempora et perspiciatis et est necessitatibus. Dolor porro assumenda quos dolor laudantium.', '1987-03-20', 1, 3, 1, 0, 2, 0, NULL, NULL, NULL),
(63, 'Prof. Alexandrine Auer', '08986672', 163.00, 86, 86, 'Est non facilis ea. Quibusdam sapiente illo quo nulla perspiciatis. Delectus enim magni ut soluta id corporis consectetur consequatur.', '1992-10-20', 0, 7, 1, 0, 4, 0, NULL, NULL, NULL),
(64, 'Kurtis Gleason', '36198429', 197.00, 47, 47, 'In est rem hic sapiente voluptates. Et magni cupiditate itaque fugiat quia est voluptas reiciendis. Quisquam ipsum id qui autem eos repudiandae tenetur corrupti. Debitis debitis eius voluptas.', '1990-04-11', 0, 4, 1, 0, 5, 0, NULL, NULL, NULL),
(65, 'Walker McLaughlin', '65853153', 619.00, 20, 20, 'Voluptate veniam autem quod facilis porro dolorem eum et. Commodi velit minima voluptatem velit consequatur.', '1979-06-26', 0, 3, 1, 0, 5, 0, NULL, NULL, NULL),
(66, 'Christa Kovacek', '14802973', 403.00, 43, 43, 'Autem rem et vero qui dignissimos. Repudiandae est ipsam necessitatibus hic aperiam sunt. Sint ea veritatis voluptatem. Velit dolores placeat et repudiandae et minus quasi.', '1994-10-07', 0, 9, 1, 0, 4, 0, NULL, NULL, NULL),
(67, 'Citlalli Wintheiser Jr.', '91536914', 30.00, 9, 9, 'Vel fuga beatae possimus nemo qui quam. Delectus eveniet molestias cumque voluptatem fuga animi. Aut ea consequatur tempore beatae sed.', '2003-06-03', 0, 9, 1, 0, 4, 0, NULL, NULL, NULL),
(68, 'Prof. Vivianne Nienow Jr.', '90103179', 838.00, 24, 24, 'Sed excepturi ullam quo quis. Voluptatum rerum ducimus a dicta maiores suscipit vel voluptatem. Qui est error occaecati eaque voluptatem.', '1981-02-20', 1, 2, 1, 0, 3, 0, NULL, NULL, NULL),
(69, 'Renee Little', '27915127', 469.00, 1, 1, 'Quis maxime sint est. Voluptatem qui doloribus fugiat blanditiis ipsam eaque aut. Eligendi quos voluptate qui dolorum eaque. Vel enim assumenda error magni molestiae consequatur consequatur.', '1979-06-23', 1, 3, 1, 0, 1, 0, NULL, NULL, NULL),
(70, 'Mr. Joany Jones', '81657834', 31.00, 16, 16, 'Et porro laborum eaque placeat tempore. Quos autem dicta sed vitae. Assumenda animi et et et aperiam. Itaque doloremque incidunt fugiat ullam necessitatibus aut.', '1983-05-10', 1, 8, 1, 0, 1, 0, NULL, NULL, NULL),
(71, 'Mrs. Ashtyn Mills', '73735793', 106.00, 92, 92, 'Est quidem quo qui consequuntur voluptatem. Libero dolor est error cumque tempora voluptatem natus consequuntur. Non quas est ut nihil.', '1977-03-25', 0, 1, 1, 0, 3, 0, NULL, NULL, NULL),
(72, 'Rosalinda Flatley', '63918366', 227.00, 6, 6, 'Numquam officiis rerum consequuntur itaque iure molestias. Iste aut eum suscipit consequatur distinctio culpa cum. Expedita quos labore vero iusto odio reiciendis.', '1999-08-02', 0, 5, 1, 0, 4, 0, NULL, NULL, NULL),
(73, 'Mrs. Brittany Pfeffer', '34730850', 951.00, 3, 3, 'Tempore eos corrupti incidunt quo. Cumque voluptatem quisquam nulla aut et. Velit est itaque a mollitia delectus. Dolorum et sit et impedit alias ab aliquid eligendi.', '1996-07-02', 0, 5, 1, 0, 2, 0, NULL, NULL, NULL),
(74, 'Isabelle Mante DDS', '38534218', 142.00, 13, 13, 'Officiis officiis eum velit quam tenetur. Ipsam optio totam quo. Ab voluptas ducimus blanditiis autem.', '2000-07-01', 1, 3, 1, 0, 4, 0, NULL, NULL, NULL),
(75, 'Prof. Fermin Deckow', '13382025', 610.00, 46, 46, 'Ut blanditiis eum quasi perspiciatis qui beatae. Nisi alias possimus possimus vel.', '2018-01-03', 1, 1, 1, 0, 5, 0, NULL, NULL, NULL),
(76, 'Sofia Ward', '37440763', 708.00, 66, 66, 'Assumenda quasi explicabo corrupti consequatur. Esse quos id vel consequatur. Neque optio ea totam ipsum beatae.', '1984-11-03', 0, 4, 1, 0, 2, 0, NULL, NULL, NULL),
(77, 'Cale Gleason', '52791451', 32.00, 11, 11, 'Quia aut velit autem sit optio ratione est. Corrupti earum pariatur porro tenetur dignissimos.', '2008-12-13', 0, 7, 1, 0, 1, 0, NULL, NULL, NULL),
(78, 'Colton Parker', '53491961', 829.00, 79, 79, 'Iste harum voluptatibus est itaque quas et provident. Iusto nesciunt dolor nostrum in. Ipsum reprehenderit quia hic eligendi quos.', '1977-05-05', 0, 4, 1, 0, 4, 0, NULL, NULL, NULL),
(79, 'Mr. Matteo Lakin I', '52283307', 260.00, 30, 30, 'Quam veniam non vel. Suscipit aut accusantium vitae. Et id quia quisquam. Nam sit ut sunt fuga voluptatem a.', '1972-04-04', 0, 6, 1, 0, 4, 0, NULL, NULL, NULL),
(80, 'Bria Schuster', '17013024', 124.00, 24, 24, 'Cum omnis et qui officia a. Sunt nisi nam ut. Earum omnis odit facere vero at ut. Nobis itaque sunt eos voluptatem eaque. Aut et dolorem non commodi vel.', '1984-10-22', 1, 5, 1, 0, 2, 0, NULL, NULL, NULL),
(81, 'Raheem Dibbert', '16747340', 181.00, 59, 59, 'Suscipit molestiae aut incidunt possimus aut dolorem. Hic dolor magnam necessitatibus dicta maiores et et. Fugit qui illum eveniet.', '1990-10-13', 1, 10, 1, 0, 2, 0, NULL, NULL, NULL),
(82, 'Derek Padberg', '25593051', 685.00, 10, 10, 'Rerum aut ullam qui dolor voluptatem sunt quae tempore. Eos tempore ut vel modi voluptatem autem. Est incidunt quos qui repellendus veritatis perspiciatis. Dolorem non quia repellat explicabo.', '1993-02-03', 0, 3, 1, 0, 2, 0, NULL, NULL, NULL),
(83, 'Dr. Ernestina Dooley III', '57840161', 275.00, 52, 52, 'Quia beatae accusamus veniam. Debitis rerum eveniet aliquid eos id. Eos voluptas sed voluptates culpa quis excepturi.', '2000-10-25', 0, 4, 1, 0, 4, 0, NULL, NULL, NULL),
(84, 'Flavie Upton', '33880969', 820.00, 54, 54, 'Magni officiis laborum rerum vel vitae eligendi. Magni accusantium consequuntur et qui eos aut hic. Facilis in dolore qui exercitationem ut rerum.', '1988-10-29', 1, 10, 1, 0, 5, 0, NULL, NULL, NULL),
(85, 'Dr. Porter Nolan III', '26498881', 367.00, 32, 32, 'Sit consequuntur dolor quibusdam numquam eaque ut voluptates. Reprehenderit ut possimus velit quo aut velit illo. Quia qui in voluptate tempore.', '2015-03-12', 0, 3, 1, 0, 1, 0, NULL, NULL, NULL),
(86, 'Edythe Gutmann', '79594134', 806.00, 55, 55, 'Voluptas eum error harum qui qui quod sed. Doloremque ab molestias ratione architecto et error earum. Eos inventore dolor repellendus consequuntur.', '1989-08-03', 0, 9, 1, 0, 5, 0, NULL, NULL, NULL),
(87, 'Luis Welch', '50533992', 375.00, 72, 72, 'Nisi repudiandae in quidem sint dolores non suscipit at. Similique pariatur est nemo rerum ut et. Eum aut dolores architecto iste adipisci. Deserunt ut fuga optio adipisci eaque sit nesciunt.', '1975-01-05', 1, 1, 1, 0, 4, 0, NULL, NULL, NULL),
(88, 'Annamae Schamberger', '12570720', 639.00, 76, 76, 'Tempora voluptas fugiat accusamus blanditiis dolorum qui sunt. Reiciendis aliquam delectus aperiam nesciunt quae quo nostrum quis. Autem nobis id ut quidem commodi.', '2015-11-22', 0, 4, 1, 0, 4, 0, NULL, NULL, NULL),
(89, 'Koby Jaskolski', '24923651', 60.00, 65, 65, 'Et maiores praesentium est eos voluptas qui harum. Possimus sit sed nesciunt facere eos perferendis repellendus. Incidunt consequatur quia quia voluptate.', '1976-01-06', 1, 7, 1, 0, 3, 0, NULL, NULL, NULL),
(90, 'Olaf Toy', '92937499', 784.00, 10, 10, 'Et autem atque eum. Corporis voluptatem placeat et. Ipsa officiis veritatis eos aut distinctio debitis. Recusandae doloribus maiores illum. Iusto cumque quis temporibus assumenda.', '1984-10-28', 1, 5, 1, 0, 3, 0, NULL, NULL, NULL),
(91, 'Eliza Okuneva', '33334509', 777.00, 100, 100, 'Sint atque dolores sint omnis ullam vero aut est. Voluptas ea debitis ut omnis ex. Nostrum qui perferendis labore fugiat. Corporis et id ipsa eum.', '1996-07-15', 0, 5, 1, 0, 4, 0, NULL, NULL, NULL),
(92, 'Mossie Swift', '12413843', 924.00, 63, 63, 'Recusandae consequatur qui ut quos et. Maiores excepturi atque est voluptatem dolores. Dignissimos nihil facere et modi iure voluptatibus. Assumenda nam cum provident tenetur et.', '1992-06-29', 1, 8, 1, 0, 5, 0, NULL, NULL, NULL),
(93, 'Janiya Johns', '95286303', 226.00, 41, 41, 'Qui omnis consequatur temporibus nostrum voluptate eius. Nobis vitae magni sint esse impedit voluptatum. Velit et ut dolor aliquam omnis consequatur.', '1984-10-08', 1, 9, 1, 0, 5, 0, NULL, NULL, NULL),
(94, 'George Reichert', '75933968', 713.00, 22, 22, 'Porro rem soluta et magni iure. Fuga commodi labore omnis quam nobis. Eligendi enim minus dolores dignissimos.', '2007-05-22', 1, 10, 1, 0, 2, 0, NULL, NULL, NULL),
(95, 'Zaria Mann II', '29520589', 201.00, 95, 95, 'Ut odit sit hic consequatur cumque deserunt labore. Dolorem autem labore est voluptatum amet sint in optio. Vel delectus quidem dignissimos id autem. Consequuntur modi aut esse corrupti quae dolorem.', '1980-06-27', 1, 2, 1, 0, 3, 0, NULL, NULL, NULL),
(96, 'Leatha Leannon', '68710606', 520.00, 36, 36, 'Rerum consequatur doloribus iste voluptatem. Dolores eos voluptatem omnis recusandae magni impedit. Et qui dolore voluptatem quos dolorem omnis. Possimus rerum optio dolorum veritatis eius.', '1981-06-27', 0, 9, 1, 0, 3, 0, NULL, NULL, NULL),
(97, 'Claudine Grant', '68276140', 337.00, 95, 95, 'Tempore optio ullam qui ducimus. Delectus omnis deserunt minima aut esse cumque sed. Laudantium rerum autem maiores corporis et hic dolore.', '1973-03-14', 1, 1, 1, 0, 5, 0, NULL, NULL, NULL),
(98, 'Alayna Padberg', '43807420', 187.00, 88, 88, 'Rerum odio voluptas et. Sed aut labore quia laboriosam velit. Illo odio voluptas neque sed in.', '2014-03-14', 0, 7, 1, 0, 4, 0, NULL, NULL, NULL),
(99, 'Ms. Sonia O\'Keefe', '04387398', 865.00, 67, 67, 'Rem necessitatibus quo ullam molestias et. Qui et molestiae quia id. Doloremque necessitatibus accusamus in enim nisi magnam excepturi. Suscipit ut iusto voluptatem amet.', '2012-05-29', 1, 1, 1, 0, 4, 0, NULL, NULL, NULL),
(100, 'Odie Kshlerin', '97971221', 771.00, 68, 68, 'Fuga ducimus ea nulla harum harum magnam voluptatem. Quibusdam sequi et corrupti voluptas. Ut quod voluptas facilis. Sint eligendi ratione placeat autem corporis.', '1981-11-01', 1, 6, 1, 0, 3, 0, NULL, NULL, NULL),
(101, 'Royal Durgan', '06399177', 359.00, 11, 11, 'Earum distinctio impedit rerum dolor quos nesciunt. Animi odio aspernatur quis reprehenderit explicabo neque quia. Nobis unde exercitationem quisquam fuga autem sunt quo.', '1970-07-28', 0, 2, 1, 0, 1, 0, NULL, NULL, NULL),
(102, 'Asha Schowalter Jr.', '84591234', 300.00, 54, 54, 'Sed veniam dolore nulla quia inventore. Sed ut consectetur quos rem incidunt. Magnam et sunt autem sed. Et officiis aliquid aut in ratione. Non est soluta aspernatur enim tempore.', '1992-04-21', 0, 5, 1, 0, 4, 0, NULL, NULL, NULL),
(103, 'Miss Breanne Daniel', '72001028', 488.00, 74, 74, 'Sit sed recusandae voluptas nihil doloribus repellendus. Pariatur iste nam dolorem in. Excepturi accusamus nihil adipisci porro est omnis. Iste laboriosam sed distinctio quae quia porro ea.', '1978-05-24', 0, 7, 1, 0, 4, 0, NULL, NULL, NULL),
(104, 'Maureen O\'Connell', '86942430', 519.00, 80, 80, 'Itaque ad beatae at dolor temporibus. Dolorem minus impedit aspernatur et aspernatur. Amet quas quod magnam sequi quo omnis. Nobis aut modi quibusdam quaerat omnis.', '2005-03-21', 1, 6, 1, 0, 3, 0, NULL, NULL, NULL),
(105, 'Zena Jast', '35519973', 762.00, 24, 24, 'Modi necessitatibus aliquam et. Aut illum consequuntur rerum minima. Molestiae iusto libero ut magnam quod repellendus. Nemo odit voluptates et nobis dignissimos natus.', '1971-11-22', 0, 2, 1, 0, 1, 0, NULL, NULL, NULL),
(106, 'Arnaldo O\'Keefe', '89535240', 645.00, 17, 17, 'Animi facere eveniet sed. Exercitationem eum id ut cupiditate est aliquam quia. Sint dolore dicta cumque.', '2003-12-07', 1, 6, 1, 0, 4, 0, NULL, NULL, NULL),
(107, 'Mrs. Queen Huel I', '18716610', 288.00, 26, 26, 'Minima quibusdam nostrum aut qui similique officia provident. Asperiores eveniet doloremque natus ea perspiciatis tempore. Qui facere esse esse aperiam aspernatur nam.', '1992-04-16', 0, 9, 1, 0, 4, 0, NULL, NULL, NULL),
(108, 'Ms. Rosamond Schroeder DDS', '89847763', 109.00, 45, 45, 'Cum autem quia blanditiis praesentium. Voluptas consequatur repellat distinctio reiciendis facere. Molestiae eveniet consequuntur minus officiis officia.', '1994-02-14', 0, 10, 1, 0, 5, 0, NULL, NULL, NULL),
(109, 'Ressie O\'Kon Jr.', '78579651', 515.00, 18, 18, 'Eum et vel porro ratione. In aperiam quia et facilis aut consequatur. Sed sit ex recusandae. Modi mollitia qui suscipit ipsam eius sit.', '1971-07-20', 1, 9, 1, 0, 3, 0, NULL, NULL, NULL),
(110, 'Josiah Sawayn MD', '38488504', 316.00, 25, 25, 'Vel fuga ipsum dolore consequuntur perspiciatis. Magni officia pariatur dolore. Exercitationem ad sunt voluptatibus maiores. Non nulla deserunt et at quas hic consequatur.', '1993-05-24', 1, 1, 1, 0, 5, 0, NULL, NULL, NULL),
(111, 'Carson Lowe', '10075524', 985.00, 83, 83, 'Aspernatur exercitationem nisi dolorem repudiandae earum voluptate culpa. Molestiae veniam nisi at repudiandae. Autem dolores ipsa rerum esse rem.', '1973-07-21', 1, 3, 1, 0, 1, 0, NULL, NULL, NULL),
(112, 'Mrs. Sophia Wiegand', '44016067', 385.00, 83, 83, 'Accusantium omnis corporis sit ex cumque enim. Quos eligendi veritatis velit dolorem rerum neque rerum.', '2013-06-15', 1, 4, 1, 0, 3, 0, NULL, NULL, NULL),
(113, 'Dr. Viviane Sipes Jr.', '09709492', 86.00, 27, 27, 'Facilis maxime a et velit. Dolor ipsam adipisci odio.', '1974-12-12', 1, 6, 1, 0, 1, 0, NULL, NULL, NULL),
(114, 'Reed Lindgren', '28086857', 907.00, 74, 74, 'Temporibus suscipit minima libero sit assumenda. Nulla recusandae iusto voluptatem et in blanditiis fuga. At cupiditate ipsam voluptatum velit commodi ducimus.', '2014-10-29', 0, 1, 1, 0, 3, 0, NULL, NULL, NULL),
(115, 'Lucas Weissnat', '32237078', 798.00, 54, 54, 'Perspiciatis et sed nihil sit explicabo sed. Magnam consequuntur dolorem nihil unde nihil. Aliquid maiores ab adipisci pariatur nulla ipsum quaerat.', '2008-06-15', 1, 8, 1, 0, 1, 0, NULL, NULL, NULL),
(116, 'Camille Raynor', '36740802', 690.00, 67, 67, 'Illum et est ut atque. Eaque in voluptatem totam consequatur delectus. Voluptatem quia minima placeat nulla id voluptatum.', '1989-07-07', 0, 9, 1, 0, 2, 0, NULL, NULL, NULL),
(117, 'Wayne Schumm', '63285758', 444.00, 37, 37, 'Doloribus voluptas esse repudiandae iusto. Placeat est sed odio voluptas. Sed non sed repellendus nemo magnam qui. Facere est dicta temporibus est. Sint tenetur vitae quod voluptas similique ut quae.', '2010-10-15', 1, 9, 1, 0, 1, 0, NULL, NULL, NULL),
(118, 'Frida Brown', '33297675', 512.00, 12, 12, 'Sint mollitia omnis sunt autem at. Quia ducimus ipsa sed. Maiores sed debitis omnis voluptatem.', '1973-11-02', 1, 5, 1, 0, 4, 0, NULL, NULL, NULL),
(119, 'Albert Haag', '39760517', 444.00, 98, 98, 'Cupiditate laboriosam dignissimos et sit nobis eveniet dicta dolores. Ipsam qui vero labore ut.', '2001-07-18', 1, 8, 1, 0, 2, 0, NULL, NULL, NULL),
(120, 'Jaylon Hintz DDS', '04978121', 868.00, 19, 19, 'Dolorum tenetur adipisci similique excepturi. Cumque quas voluptatem ut sed sed. Voluptatem architecto mollitia ad autem.', '1971-09-01', 1, 10, 1, 0, 1, 0, NULL, NULL, NULL),
(121, 'Barrett Zieme', '09558519', 59.00, 80, 80, 'Iste eum reprehenderit rem. Dolorem ut recusandae reiciendis sed quod illum. Dolorem ducimus expedita dolores dolor nemo.', '1975-07-29', 1, 1, 1, 0, 2, 0, NULL, NULL, NULL),
(122, 'Camren Streich MD', '21913891', 724.00, 34, 34, 'Ut vitae rerum quasi maxime dolores ipsam qui numquam. Odit delectus rem cumque deleniti omnis ipsum. Perspiciatis quia aut pariatur amet quisquam natus occaecati.', '1992-07-20', 1, 7, 1, 0, 4, 0, NULL, NULL, NULL),
(123, 'Miss Giovanna Bosco', '99857202', 143.00, 24, 24, 'Nostrum aliquid aliquid aut molestias. Sequi omnis aut reprehenderit omnis. Vel incidunt id eaque quae molestiae. Facere nihil possimus tenetur a facere consequatur illo blanditiis.', '2006-11-11', 1, 4, 1, 0, 5, 0, NULL, NULL, NULL),
(124, 'Dedrick Tromp', '93220064', 263.00, 11, 11, 'Corporis cumque incidunt iure inventore iusto ad aperiam laudantium. Ut asperiores ex ut inventore. Dolores et accusamus ullam ut et eum ratione.', '2016-12-30', 0, 9, 1, 0, 2, 0, NULL, NULL, NULL),
(125, 'Dr. Jasmin Weissnat II', '91158420', 897.00, 42, 42, 'Sed sint aut blanditiis. Nulla neque aut et in distinctio velit. Id quia sunt cumque quibusdam dolor quia.', '2006-06-21', 0, 8, 1, 0, 2, 0, NULL, NULL, NULL),
(126, 'Kyra Lesch', '27933824', 638.00, 88, 88, 'Aperiam dolor repudiandae repudiandae rerum. Est qui autem quis aliquam quidem dignissimos quae. Reprehenderit mollitia labore debitis non sunt.', '1974-04-02', 0, 6, 1, 0, 4, 0, NULL, NULL, NULL),
(127, 'Georgette Parisian', '72395158', 614.00, 93, 93, 'Maxime aut tenetur exercitationem qui natus. Laborum ad voluptas omnis ab consequatur aliquid nam consequatur. Sint nulla iste sequi cumque iure.', '1998-04-08', 0, 10, 1, 0, 2, 0, NULL, NULL, NULL),
(128, 'Virginia Koepp', '13711955', 719.00, 64, 64, 'Et ullam fugit ea atque voluptas magnam. Cum quidem minima dolorem eveniet. Illo possimus voluptas quaerat et. Dicta molestias culpa et adipisci ex. Fugit enim sit enim minima doloremque nostrum.', '1993-12-15', 0, 2, 1, 0, 2, 0, NULL, NULL, NULL),
(129, 'Mabelle Kunde PhD', '25051926', 429.00, 57, 57, 'Ut aliquam enim omnis enim. Ad rerum et et maxime dolore reprehenderit ea. Provident tenetur velit repudiandae aut est ullam ut. Adipisci eum nostrum aut. Quasi est sed expedita molestiae rerum.', '1975-07-03', 1, 4, 1, 0, 4, 0, NULL, NULL, NULL),
(130, 'Jessika White', '12951925', 846.00, 70, 70, 'Cum voluptatem quia fuga et et consequatur. Pariatur non sed laboriosam libero. Quaerat eos laboriosam tempore nemo possimus id porro. Atque ex voluptatem perferendis iste quia.', '2015-10-21', 1, 4, 1, 0, 5, 0, NULL, NULL, NULL),
(131, 'Bernita Harvey', '34526514', 75.00, 12, 12, 'Tempora saepe omnis optio magnam accusantium accusantium at. Maxime exercitationem vero non. Earum aspernatur sed in. Recusandae id eos et et quod consequatur dolores.', '2007-01-04', 0, 6, 1, 0, 4, 0, NULL, NULL, NULL),
(132, 'Ms. Jaclyn Schaefer', '12757879', 578.00, 50, 50, 'Magni sequi id non. Hic id repellendus harum natus occaecati odio consequuntur. A incidunt soluta enim eius minus rem voluptas fugit. Vel est magni dolore quisquam voluptate sunt.', '2001-07-26', 0, 9, 1, 0, 3, 0, NULL, NULL, NULL),
(133, 'Mac Wiza', '40729602', 624.00, 9, 9, 'Ullam eum non quisquam maiores aut saepe delectus tempore. Dolores qui voluptatum neque dolores.', '2004-11-23', 1, 5, 1, 0, 5, 0, NULL, NULL, NULL),
(134, 'Tina Fadel', '48567220', 443.00, 29, 29, 'Et quia provident maiores perferendis. Aut est rem et debitis voluptatum. Amet cupiditate consectetur quia.', '1985-10-24', 1, 10, 1, 0, 4, 0, NULL, NULL, NULL),
(135, 'Oswaldo Borer MD', '14074639', 244.00, 43, 43, 'Repellat aut tempore voluptatem repellendus nulla tempora officia. Et quaerat officia rerum nobis omnis saepe. Provident quis sed culpa eum maiores adipisci.', '2006-10-20', 0, 6, 1, 0, 4, 0, NULL, NULL, NULL),
(136, 'Yvonne Kutch', '79671040', 686.00, 92, 92, 'Ratione necessitatibus illo amet nihil dolorem optio. Ullam id sit dolore dolorum soluta voluptate. Expedita voluptas natus soluta quas expedita esse.', '1998-02-05', 0, 7, 1, 0, 4, 0, NULL, NULL, NULL),
(137, 'Melvin Roberts', '17652650', 949.00, 33, 33, 'Autem qui maxime aspernatur quasi magnam. Praesentium dolore quia sapiente repellat eligendi repellat. Corrupti numquam libero deleniti natus.', '2009-01-28', 0, 3, 1, 0, 5, 0, NULL, NULL, NULL),
(138, 'Richie Gottlieb PhD', '06471491', 23.00, 60, 60, 'Accusamus autem voluptate voluptas architecto harum. Aut modi aut et est qui ab quia et. Ea fuga est laborum in illo.', '1990-07-05', 1, 8, 1, 0, 1, 0, NULL, NULL, NULL),
(139, 'Delta Prosacco', '41032220', 342.00, 94, 94, 'Blanditiis at ut provident fugit odit aspernatur voluptatem error. Quod est ipsa quos accusantium quia culpa dolorum. Illo animi voluptas ab nemo aut.', '1985-07-29', 1, 1, 1, 0, 3, 0, NULL, NULL, NULL),
(140, 'Nola Collins', '26375755', 386.00, 61, 61, 'Amet et corrupti natus pariatur. Mollitia ratione quia natus aut dolores quasi. Et et nihil minus velit. Sed rem quia fuga sit.', '2003-03-23', 1, 5, 1, 0, 5, 0, NULL, NULL, NULL),
(141, 'Henri Predovic', '23217607', 373.00, 13, 13, 'Qui eum laborum sapiente minus quidem. Error praesentium corrupti temporibus omnis consequatur voluptatem. In asperiores aut iusto dicta voluptas laborum quis.', '1970-04-28', 1, 6, 1, 0, 5, 0, NULL, NULL, NULL),
(142, 'Verla Jast', '30494800', 678.00, 10, 10, 'Ab voluptatem eveniet aut molestias commodi voluptatum sint. Necessitatibus maiores vero sed qui omnis.', '1977-06-26', 1, 6, 1, 0, 3, 0, NULL, NULL, NULL),
(143, 'Cristobal Ankunding', '05713370', 86.00, 24, 24, 'Temporibus quod est odio eligendi ad voluptatem. Debitis adipisci perspiciatis maiores sapiente officia repudiandae nihil. Consectetur voluptatem cumque quasi debitis enim.', '1970-07-25', 0, 4, 1, 0, 2, 0, NULL, NULL, NULL),
(144, 'Nigel Huel', '22651136', 521.00, 79, 79, 'Qui quibusdam quibusdam odit et saepe. Molestias sed beatae quae ea. Nulla ex quia rerum vero.', '1999-09-02', 1, 2, 1, 0, 1, 0, NULL, NULL, NULL),
(145, 'Rebeka O\'Reilly', '02416489', 366.00, 83, 83, 'Qui quidem sit dolore ipsa praesentium. Delectus fugit qui dolorum illo dolorem. Dolor et laboriosam quia ut vitae et id.', '1999-03-16', 1, 5, 1, 0, 5, 0, NULL, NULL, NULL),
(146, 'Ayana Gottlieb', '76595714', 4.00, 76, 76, 'Exercitationem rerum id fugiat natus. Consectetur et illo sequi ea reiciendis.', '1995-10-27', 0, 7, 1, 0, 1, 0, NULL, NULL, NULL),
(147, 'Mr. Dario Parker III', '55027656', 695.00, 43, 43, 'Illum sunt corrupti exercitationem corporis voluptatem. Repudiandae odio natus omnis et magni sunt. Non quia officiis et vel natus vel id.', '1999-11-22', 0, 5, 1, 0, 4, 0, NULL, NULL, NULL),
(148, 'Omer Turcotte', '79785617', 435.00, 18, 18, 'Qui repellendus vel totam eaque eveniet. Minus facilis dolore animi quas sed. Quia maxime rerum qui velit. Fugit voluptate dolore pariatur corrupti ea unde numquam voluptate.', '2004-03-18', 1, 6, 1, 0, 1, 0, NULL, NULL, NULL),
(149, 'Vesta Jacobson', '48450119', 897.00, 28, 28, 'Amet voluptatibus aut soluta quo quis. Et eveniet id expedita. Eum eius eaque expedita eum ut. Quasi laudantium magni quisquam. Mollitia illo possimus quia aut tempora ullam similique.', '1977-09-13', 1, 9, 1, 0, 5, 0, NULL, NULL, NULL),
(150, 'Prof. Laurence Runte PhD', '94950793', 93.00, 5, 5, 'Cumque consequuntur magnam qui totam officia et. Dolores sunt porro rerum delectus.', '2001-12-05', 1, 10, 1, 0, 4, 0, NULL, NULL, NULL),
(151, 'Franz Greenholt IV', '20110550', 889.00, 10, 10, 'Ratione consectetur dolores atque id inventore fugit. Id praesentium nihil ut distinctio ut. Qui omnis assumenda quis veritatis voluptatem dolor. Quo quo eaque eos consequatur amet omnis.', '2018-05-05', 1, 10, 1, 0, 4, 0, NULL, NULL, NULL),
(152, 'Christian Hill I', '20331368', 973.00, 99, 99, 'Dolore in delectus perspiciatis maiores numquam. Velit quibusdam non nesciunt culpa voluptatum. Quia maiores ut non quia recusandae.', '1994-06-03', 1, 5, 1, 0, 1, 0, NULL, NULL, NULL),
(153, 'Brain Wiegand IV', '69652998', 433.00, 83, 83, 'Recusandae fuga aut quam dolor. Et ad quis quia quibusdam necessitatibus. Magnam ipsum iusto aliquam natus omnis reprehenderit.', '1985-05-28', 0, 8, 1, 0, 1, 0, NULL, NULL, NULL),
(154, 'Martina Gusikowski', '27674857', 540.00, 95, 95, 'Eius laborum provident fuga veniam et nemo. Et iure et minus quo labore tempora laboriosam.', '1973-09-22', 0, 9, 1, 0, 2, 0, NULL, NULL, NULL),
(155, 'Marlee Grant', '75204648', 884.00, 35, 35, 'Voluptates nemo non et ipsam totam id. Beatae veritatis aspernatur qui itaque. Omnis est ea aperiam quidem.', '1972-05-22', 1, 8, 1, 0, 1, 0, NULL, NULL, NULL),
(156, 'Elmer Reynolds DDS', '49717419', 477.00, 96, 96, 'Maiores reprehenderit quasi eveniet repudiandae molestias tenetur. Vel reiciendis et ut aut omnis. Occaecati quo molestias quia autem dolore.', '1998-11-25', 0, 9, 1, 0, 5, 0, NULL, NULL, NULL),
(157, 'Sam Walter', '33600062', 943.00, 30, 30, 'Temporibus quis vel sit amet consequatur perferendis tenetur natus. Et quidem nisi iusto quidem vel porro. Odio nisi quisquam deleniti consequatur sunt aut cupiditate accusantium.', '1997-02-18', 1, 2, 1, 0, 3, 0, NULL, NULL, NULL),
(158, 'Shany Larkin', '04584452', 438.00, 91, 91, 'Voluptatem aspernatur adipisci vel voluptatibus autem aspernatur. Et eos aut rem ut illo voluptatem. Voluptas illum magnam officia qui et.', '1972-04-02', 1, 1, 1, 0, 2, 0, NULL, NULL, NULL),
(159, 'Sylvia Funk V', '05057221', 402.00, 77, 77, 'Quia voluptas distinctio nihil illo dolor omnis. Rerum doloribus nemo exercitationem dolorem. Ex vero sint minima non veniam. Ipsum dolorem qui mollitia nostrum minima repellendus repudiandae.', '1989-03-06', 1, 3, 1, 0, 5, 0, NULL, NULL, NULL),
(160, 'Brayan Brekke MD', '03596593', 792.00, 28, 28, 'Possimus amet est optio. Quaerat recusandae porro qui sed et autem architecto. Repellat praesentium aut voluptatem ut magni.', '1973-01-02', 1, 4, 1, 0, 3, 0, NULL, NULL, NULL),
(161, 'Dr. Ellen Roberts III', '97985259', 897.00, 6, 6, 'Est ut molestiae nulla qui enim ut. Hic unde ab non sequi voluptas hic cum et. Ut nihil alias harum voluptatibus dolor facilis excepturi. Est et aut perspiciatis perspiciatis illo voluptas quam.', '1979-04-28', 1, 10, 1, 0, 2, 0, NULL, NULL, NULL),
(162, 'Jo Beatty', '04457343', 380.00, 91, 91, 'At nam magni architecto accusamus. Perferendis voluptates laudantium repellendus animi velit incidunt. Itaque sit id ullam est ut molestias enim.', '1997-12-21', 0, 10, 1, 0, 4, 0, NULL, NULL, NULL),
(163, 'Leonardo Schamberger', '82832896', 660.00, 45, 45, 'Vel sequi veniam id quo. Sed culpa laborum id. Officia qui expedita et vitae. Esse nisi autem necessitatibus saepe possimus quam.', '1995-08-10', 1, 5, 1, 0, 4, 0, NULL, NULL, NULL),
(164, 'Dr. Chesley Orn', '58046227', 261.00, 70, 70, 'Et asperiores laborum id consequuntur esse error officia aliquam. Voluptatem officia illum eaque dolores. Animi dolores porro ut itaque atque. Nobis qui dolores harum eos voluptas.', '1971-01-20', 0, 1, 1, 0, 1, 0, NULL, NULL, NULL),
(165, 'Raymond Schulist', '65197134', 263.00, 94, 94, 'Nihil velit cum nam et laudantium. Velit totam aperiam consequatur rerum est et eum. Delectus dolore rerum velit nesciunt facilis est a.', '1971-07-18', 1, 1, 1, 0, 4, 0, NULL, NULL, NULL),
(166, 'Dr. Chaz Boehm II', '61010673', 462.00, 41, 41, 'In quia iure natus sit. Sit rerum ducimus repellat porro dolores ut. Necessitatibus omnis dolorem molestiae unde deserunt nostrum voluptatem. Qui quidem omnis qui earum maiores enim qui.', '1988-01-09', 0, 5, 1, 0, 1, 0, NULL, NULL, NULL),
(167, 'Briana Hane', '15304391', 376.00, 100, 100, 'Voluptatem repudiandae est autem. Voluptatibus et sapiente nemo aperiam velit. Recusandae facilis tempore perferendis modi laborum.', '2004-03-31', 0, 10, 1, 0, 3, 0, NULL, NULL, NULL),
(168, 'Mr. Myrl Mertz', '56317800', 728.00, 41, 41, 'Assumenda consequuntur veniam adipisci et eum tempora quos. Fugit voluptas nam praesentium in praesentium itaque sint. Dicta quam id similique eos est autem.', '1983-11-18', 0, 6, 1, 0, 2, 0, NULL, NULL, NULL),
(169, 'Adell Kreiger', '72730751', 510.00, 29, 29, 'Et minus ad vero adipisci neque. Laborum ab et molestias nisi sequi sed. Accusantium quia quo et provident qui eligendi voluptatem.', '2004-08-02', 1, 8, 1, 0, 3, 0, NULL, NULL, NULL),
(170, 'Nathan Lind', '37936686', 276.00, 39, 39, 'Quaerat libero dolore et debitis atque. Consequatur quasi ut rem. Occaecati sed soluta deserunt iure sequi sint eos. Aut molestiae omnis sint sit veniam voluptas.', '1982-05-27', 1, 9, 1, 0, 5, 0, NULL, NULL, NULL),
(171, 'Irving Brakus', '08536037', 771.00, 49, 49, 'Ea fugit dolores ipsum eligendi. Illum magni quia et quibusdam non a ea laborum. Quas optio vitae ea reiciendis ipsa autem velit.', '2004-10-19', 1, 6, 1, 0, 1, 0, NULL, NULL, NULL),
(172, 'Raymundo Smitham', '24821704', 537.00, 15, 15, 'Molestiae animi quasi nemo et nisi reiciendis ut. Natus facilis quo dolore molestias. Sed ab quia aut aliquid quasi. Eos vero aut vitae et adipisci et.', '2004-06-27', 1, 9, 1, 0, 2, 0, NULL, NULL, NULL),
(173, 'Bridget Abbott MD', '91614261', 607.00, 95, 95, 'Eligendi voluptatum incidunt ullam minima. Rem eum aut qui neque quis quos. Eum expedita sequi sunt est. Dignissimos fuga nihil non ut velit ea vel.', '1978-12-30', 1, 6, 1, 0, 1, 0, NULL, NULL, NULL),
(174, 'Damian Pollich', '67981007', 916.00, 31, 31, 'Rem qui adipisci tempore quia. Aut beatae ipsum laborum ab voluptatem temporibus. Illum magnam minus aut non aut ab. In eligendi ut enim animi optio nam voluptatem in.', '1973-07-26', 0, 10, 1, 0, 3, 0, NULL, NULL, NULL),
(175, 'Mr. Remington Marvin III', '95728537', 562.00, 52, 52, 'Atque placeat dolore qui. Magnam debitis excepturi dolor. Voluptatibus sed quidem ut repellendus voluptatum.', '1979-05-12', 1, 8, 1, 0, 2, 0, NULL, NULL, NULL),
(176, 'Izaiah Mraz', '00313391', 532.00, 12, 12, 'Nemo ipsam non delectus. Eum ratione recusandae optio et pariatur quia. Omnis occaecati totam quos molestiae.', '2013-06-18', 1, 8, 1, 0, 5, 0, NULL, NULL, NULL),
(177, 'Roselyn Cremin III', '45942082', 22.00, 26, 26, 'Minima nesciunt nobis perspiciatis nisi porro quasi. Et est rerum voluptate accusamus. Officiis rerum ex vel illum harum ad. Dolore delectus qui architecto animi et. Assumenda rerum quaerat quos vel.', '1992-08-20', 1, 8, 1, 0, 3, 0, NULL, NULL, NULL),
(178, 'Osbaldo Littel', '26971926', 976.00, 15, 15, 'Delectus sunt impedit architecto necessitatibus ut. Culpa reiciendis quis iste et dolores quia. A et reiciendis nam suscipit qui laboriosam ut sit.', '2014-07-20', 1, 2, 1, 0, 5, 0, NULL, NULL, NULL),
(179, 'Priscilla Beier', '50859085', 968.00, 20, 20, 'Fugiat voluptatem veniam eligendi est. Qui omnis consequatur autem quibusdam nostrum veritatis ut explicabo.', '2014-12-01', 0, 6, 1, 0, 5, 0, NULL, NULL, NULL),
(180, 'Prof. Willy Harris', '01277548', 56.00, 6, 6, 'Velit eius natus eum voluptate nihil eius nihil maxime. Et rerum est et laborum quos sequi dolor. Eum quam quis dicta odit aut voluptates.', '1978-04-10', 0, 2, 1, 0, 3, 0, NULL, NULL, NULL),
(181, 'Daisha Rath II', '69248269', 724.00, 7, 7, 'Aut sed minus assumenda provident. Quam exercitationem ipsam natus et adipisci beatae quae recusandae.', '1980-08-24', 0, 2, 1, 0, 3, 0, NULL, NULL, NULL),
(182, 'Margarett Roberts', '90988479', 549.00, 36, 36, 'Recusandae accusantium corrupti id quis amet consectetur. Saepe vel earum est ea dolor error. Officia et reiciendis occaecati debitis.', '2000-10-23', 1, 4, 1, 0, 2, 0, NULL, NULL, NULL),
(183, 'Carolyn Fritsch', '31047685', 708.00, 11, 11, 'Enim dolorem quasi voluptatibus voluptatem odit quidem ipsam. Quaerat dolorem in cupiditate fugiat. Omnis facilis ab similique illo.', '2016-01-05', 1, 5, 1, 0, 1, 0, NULL, NULL, NULL),
(184, 'Jerad Luettgen III', '23284067', 952.00, 17, 17, 'Voluptas consequuntur hic eveniet soluta dolores. Qui officia eaque sed. Libero cumque nesciunt et est. Qui qui nisi labore rerum sed.', '2016-08-09', 0, 8, 1, 0, 2, 0, NULL, NULL, NULL),
(185, 'Alicia McKenzie', '47993433', 182.00, 73, 73, 'Ipsum autem animi velit similique dolores et qui dolorum. Quis qui vero necessitatibus recusandae id in optio. Quasi eum vel nihil atque fugiat et. Iusto et voluptates et dolores a incidunt alias.', '2015-06-07', 0, 10, 1, 0, 4, 0, NULL, NULL, NULL),
(186, 'Dr. Dorian Rippin II', '91233639', 230.00, 93, 93, 'Exercitationem vero quo aut quo quis id. Sint dicta commodi sunt aut. Doloremque nostrum eum officia magni. Qui hic culpa modi saepe. Dolor velit omnis soluta modi soluta.', '1989-12-13', 1, 9, 1, 0, 2, 0, NULL, NULL, NULL),
(187, 'Josefa Cartwright', '81076154', 1.00, 34, 34, 'Dignissimos eius ullam magni explicabo ut. Maiores perferendis voluptatem unde similique quia consequuntur voluptatibus. Hic commodi dolorum ut ullam in excepturi.', '2005-10-16', 0, 5, 1, 0, 1, 0, NULL, NULL, NULL),
(188, 'Hillard Carter', '66902003', 566.00, 18, 18, 'Id qui vitae molestias nam rerum ad expedita. Aut aperiam neque provident veritatis ut ut. Iure sed quia quibusdam incidunt et enim. Eum ut molestiae ipsam cupiditate amet.', '2003-08-08', 0, 7, 1, 0, 1, 0, NULL, NULL, NULL),
(189, 'Tomas Abernathy', '24897853', 364.00, 7, 7, 'Natus inventore placeat est. Qui voluptas asperiores vel dolores saepe est. Repellat ipsa aut possimus sit. Suscipit quam itaque omnis ipsa.', '2008-11-26', 0, 8, 1, 0, 4, 0, NULL, NULL, NULL),
(190, 'Elbert Hilpert', '42292395', 112.00, 31, 31, 'Voluptate et dolor omnis. Ut qui ea velit a inventore occaecati accusamus at.', '2005-04-03', 1, 4, 1, 0, 3, 0, NULL, NULL, NULL),
(191, 'Eldred Smith', '23865501', 565.00, 49, 49, 'Laboriosam velit sed odit mollitia et qui est. Qui aperiam ut eum exercitationem provident quas dignissimos. Eveniet et aut omnis error voluptate perferendis molestias.', '2001-06-26', 0, 9, 1, 0, 3, 0, NULL, NULL, NULL),
(192, 'Ladarius Stoltenberg', '65110829', 740.00, 31, 31, 'Animi pariatur aut vel. Ut id iusto explicabo. Culpa aut excepturi mollitia velit consequuntur quasi.', '2002-01-21', 1, 4, 1, 0, 2, 0, NULL, NULL, NULL),
(193, 'Austin Oberbrunner II', '86010986', 740.00, 43, 43, 'Quidem cupiditate aut omnis ipsa optio in enim magnam. Nostrum ipsa quis quos quo illo nihil est. Ea quo et nostrum dolor ea aliquid.', '2005-08-07', 0, 10, 1, 0, 5, 0, NULL, NULL, NULL),
(194, 'Velda Anderson Sr.', '36646630', 789.00, 48, 48, 'Odit laborum illum necessitatibus. Rem et deleniti deleniti fuga qui est. Adipisci amet perspiciatis earum molestiae est quos et rem.', '1975-08-04', 1, 5, 1, 0, 4, 0, NULL, NULL, NULL),
(195, 'Mr. Gavin Miller Jr.', '81086467', 620.00, 66, 66, 'Non amet veritatis quisquam pariatur laborum suscipit. Nostrum qui exercitationem vel maiores. Dolorum suscipit sit occaecati quo.', '1971-07-31', 1, 8, 1, 0, 3, 0, NULL, NULL, NULL),
(196, 'Kelvin Frami', '79468978', 819.00, 27, 27, 'Cum dolorem iure ipsa nobis beatae id doloribus est. Labore quis ut aut. Sed ipsum doloribus suscipit nisi et. Quas et illum non sapiente.', '1975-08-30', 1, 1, 1, 0, 1, 0, NULL, NULL, NULL),
(197, 'Annabelle Bartell', '66228370', 278.00, 67, 67, 'Pariatur pariatur sit ea sapiente nemo. Doloremque non vel necessitatibus cumque dolorem iusto.', '2017-10-17', 0, 2, 1, 0, 3, 0, NULL, NULL, NULL),
(198, 'Prof. Dylan Streich Sr.', '55000567', 355.00, 91, 91, 'Pariatur quod ea cumque laboriosam. Ut fugit consequatur sit enim. Ratione iste perferendis deleniti ea eum eaque. Minima quod provident voluptas eligendi et. Maxime nemo consequuntur neque.', '2009-01-23', 0, 8, 1, 0, 4, 0, NULL, NULL, NULL);
INSERT INTO `articles` (`id`, `name`, `model`, `price`, `quantity`, `available`, `description`, `due_date`, `new`, `id_promotion`, `active`, `delete`, `rating`, `views`, `created_at`, `updated_at`, `deleted_at`) VALUES
(199, 'Ellie Jerde', '64994321', 883.00, 44, 44, 'Itaque ipsum animi autem placeat ex. Fugiat excepturi et id corrupti voluptas. Quisquam provident ut qui laudantium facere amet.', '2002-01-30', 1, 7, 1, 0, 2, 0, NULL, NULL, NULL),
(200, 'Prof. Devan Davis', '00422499', 645.00, 28, 28, 'Ut nihil quo porro iste consequatur. Repudiandae quis nulla saepe quam. Dolor unde ea eaque quaerat.', '1973-11-29', 1, 3, 1, 0, 2, 0, NULL, NULL, NULL),
(201, 'Judson Ledner', '29772346', 447.00, 13, 13, 'Repudiandae quae et ad. Error aperiam et est qui. Laborum voluptatum cumque et consectetur nulla id.', '2015-02-11', 1, 6, 1, 0, 1, 0, NULL, NULL, NULL),
(202, 'Kip Gleichner', '40810119', 403.00, 56, 56, 'Est beatae dolores vel qui vitae iste. Explicabo soluta rerum aliquid velit. Quia odit ducimus impedit nihil magnam voluptas. Ut cupiditate fuga similique facilis similique.', '2009-06-15', 0, 8, 1, 0, 1, 0, NULL, NULL, NULL),
(203, 'Elfrieda Reynolds', '28212089', 574.00, 91, 91, 'Temporibus assumenda sed consequatur dignissimos quis quibusdam velit. Voluptatem qui nostrum accusamus qui. Iure ipsa laborum aut labore est.', '1990-12-31', 1, 5, 1, 0, 3, 0, NULL, NULL, NULL),
(204, 'Mr. Kamryn Mann', '66442851', 564.00, 6, 6, 'Sequi ab dolorem ut non. Magnam alias pariatur consectetur beatae numquam. Officiis itaque et cum facere esse.', '1976-05-16', 0, 2, 1, 0, 1, 0, NULL, NULL, NULL),
(205, 'Susan Pouros', '12538560', 215.00, 65, 65, 'Asperiores repellendus ullam ratione quibusdam voluptas porro sint. Eius dolor qui odit beatae. Consequatur iure illo ut quod facere.', '1995-04-05', 0, 10, 1, 0, 2, 0, NULL, NULL, NULL),
(206, 'Itzel Vandervort', '60997098', 28.00, 91, 91, 'Esse sed deserunt officia soluta quia dolor. Quia possimus porro perspiciatis voluptas optio aperiam alias. Ut vel ut aut optio ut sapiente temporibus rerum.', '1979-04-23', 1, 3, 1, 0, 4, 0, NULL, NULL, NULL),
(207, 'Jayce Kovacek', '25174113', 412.00, 68, 68, 'Cupiditate dolorum recusandae mollitia. Et ex reiciendis deserunt fugiat est incidunt. Qui nemo in dolorem reprehenderit officia. Ut necessitatibus in sunt sapiente qui.', '1971-06-28', 1, 6, 1, 0, 2, 0, NULL, NULL, NULL),
(208, 'Kenton Weimann', '92447950', 417.00, 83, 83, 'Maxime eaque alias atque quibusdam aut commodi. Quam eveniet nulla facere soluta. Itaque nihil reprehenderit possimus aperiam ducimus debitis. Autem nostrum expedita quod quia nihil.', '2015-03-01', 1, 3, 1, 0, 2, 0, NULL, NULL, NULL),
(209, 'Miss Veda Wehner DVM', '09043398', 52.00, 57, 57, 'Fuga in fugit totam quaerat molestiae suscipit adipisci. Rerum saepe dignissimos et sequi illum eum. Qui amet in ut corporis fugit nisi.', '2008-08-05', 1, 5, 1, 0, 5, 0, NULL, NULL, NULL),
(210, 'Layne Harber', '86553148', 822.00, 91, 91, 'Accusantium dolorem corrupti hic quae cumque. Et aperiam ut consequuntur quibusdam in. Ea est accusantium et maxime. Voluptatem ullam eaque minima totam nihil.', '1997-01-05', 0, 6, 1, 0, 1, 0, NULL, NULL, NULL),
(211, 'Gilda Stamm', '07106798', 615.00, 84, 84, 'Et magni omnis deserunt et et. Ut incidunt exercitationem molestiae ducimus doloremque debitis. Eaque laboriosam fuga corrupti.', '1981-01-05', 0, 1, 1, 0, 1, 0, NULL, NULL, NULL),
(212, 'Mabelle Kiehn', '08159069', 551.00, 32, 32, 'Quas qui aliquam modi adipisci quia et vel. Fugit eum qui nesciunt ipsum sed aliquam commodi est. Voluptatibus eum rerum dolores id dicta.', '1986-11-22', 1, 7, 1, 0, 1, 0, NULL, NULL, NULL),
(213, 'Marcellus Baumbach', '80619314', 926.00, 74, 74, 'Aliquam ea cumque magni laborum necessitatibus nihil aut sit. Ratione quod quis eos. Repellat deserunt accusantium adipisci illum.', '2002-09-13', 1, 10, 1, 0, 1, 0, NULL, NULL, NULL),
(214, 'Mariela Beier', '85916357', 880.00, 5, 5, 'Quis suscipit sed error consectetur doloremque qui. Non et ut illo tempora aut maiores non. Soluta ullam et molestias eaque est aut.', '2002-11-10', 0, 7, 1, 0, 2, 0, NULL, NULL, NULL),
(215, 'Priscilla Larson', '95359380', 902.00, 98, 98, 'Incidunt et voluptate ea enim consectetur consequatur ipsam. Eos et fugiat numquam occaecati. Esse soluta quisquam dolore fugit. Dolores aperiam dolores veritatis sed perspiciatis ut.', '1999-01-08', 0, 4, 1, 0, 2, 0, NULL, NULL, NULL),
(216, 'Mortimer Kihn', '60478153', 807.00, 73, 73, 'Magni doloribus et error quo non amet. Impedit qui et aliquam tempora. Ad tenetur velit labore.', '1984-04-12', 1, 1, 1, 0, 1, 0, NULL, NULL, NULL),
(217, 'Miss Emmanuelle Hauck Jr.', '20533380', 306.00, 50, 50, 'Quam animi amet vero laudantium. Et et ipsa harum sint ex ea omnis. Consequatur modi eum doloribus voluptates.', '2015-04-06', 0, 5, 1, 0, 1, 0, NULL, NULL, NULL),
(218, 'Grayce Roberts', '10378120', 796.00, 29, 29, 'Eos et iure vel facere dolorem ducimus harum. Quia debitis perferendis eos aut est laudantium. Totam sit illum et atque quas. Aut possimus quo molestiae quis vel quasi.', '1993-06-26', 0, 7, 1, 0, 3, 0, NULL, NULL, NULL),
(219, 'Dr. Ewald Lowe', '21975042', 492.00, 20, 20, 'Est iure magnam in officia quo quia cumque. Officiis debitis quae exercitationem voluptatem aperiam non nihil.', '1991-02-15', 0, 5, 1, 0, 1, 0, NULL, NULL, NULL),
(220, 'Nikita Klein', '77617095', 530.00, 78, 78, 'Et quis architecto dolorem aut ut ullam impedit. Quo incidunt iusto qui quod laboriosam magnam. Recusandae provident est quaerat ipsa omnis ad.', '1997-10-05', 1, 4, 1, 0, 3, 0, NULL, NULL, NULL),
(221, 'Pearlie Hettinger', '51409364', 160.00, 70, 70, 'Aut ut laudantium quo pariatur nam. Consectetur et qui quos incidunt nam dicta amet. Qui est voluptatem doloribus quidem sed.', '1981-11-01', 1, 9, 1, 0, 5, 0, NULL, NULL, NULL),
(222, 'Aliyah Sipes', '69271854', 662.00, 9, 9, 'Aut libero sit dignissimos illum. Repudiandae eaque libero non sapiente rerum officia. Incidunt et enim rerum quia. Quia aut pariatur eveniet maiores ducimus quo at.', '2014-12-01', 0, 7, 1, 0, 1, 0, NULL, NULL, NULL),
(223, 'Mireille Goldner', '89642467', 103.00, 9, 9, 'Necessitatibus non sed consequatur. In ipsum tenetur qui. Quia quo ipsam dicta aut. Eum corporis illum dolorum. Consequatur quae esse numquam.', '1992-05-02', 0, 9, 1, 0, 5, 0, NULL, NULL, NULL),
(224, 'Lorna Herman DVM', '44286019', 563.00, 70, 70, 'Ut ea adipisci molestiae ea. Voluptatem quibusdam consectetur cumque libero. Et ipsa non velit eos est aliquam soluta. Aperiam placeat occaecati vel illo qui.', '2008-10-12', 1, 4, 1, 0, 2, 0, NULL, NULL, NULL),
(225, 'Jan Welch', '04970736', 958.00, 68, 68, 'Sapiente aperiam aperiam repudiandae doloribus fugiat. Voluptatem et laborum amet dolorum. Illum dolor eum ut dolorem nihil distinctio. Molestiae facere sapiente et ullam.', '1996-07-30', 1, 4, 1, 0, 2, 0, NULL, NULL, NULL),
(226, 'Prof. Elvera Davis', '90348402', 74.00, 44, 44, 'Ipsum consequuntur rerum pariatur iste magni culpa. Nam quibusdam perspiciatis minima repellendus distinctio et. Veniam totam delectus dignissimos. Quisquam illum repudiandae ut et ut.', '1998-12-16', 0, 8, 1, 0, 1, 0, NULL, NULL, NULL),
(227, 'Melba Farrell', '85197909', 955.00, 74, 74, 'Neque nobis ex recusandae ea alias. Dolorem et vero eaque dolor dolorem nulla distinctio. Quia enim qui tempora molestias vel perferendis. Eos omnis velit ea sit.', '1985-01-27', 1, 5, 1, 0, 1, 0, NULL, NULL, NULL),
(228, 'Arno McKenzie', '86608831', 251.00, 21, 21, 'Sunt maiores non perspiciatis in sit necessitatibus quo. Perspiciatis est sed accusantium.', '1977-08-17', 0, 10, 1, 0, 2, 0, NULL, NULL, NULL),
(229, 'Dr. Lessie Reichert Jr.', '06600914', 668.00, 28, 28, 'Minus iure rerum repellat et nostrum sed. Dolor fugit aut nostrum. Eos eos ipsum nihil voluptatem sunt similique.', '2011-06-24', 0, 6, 1, 0, 5, 0, NULL, NULL, NULL),
(230, 'Hardy Beier', '67038633', 124.00, 78, 78, 'Ut animi eum porro qui. Exercitationem suscipit dolorem voluptatum quia ratione et. Eos ut est est doloremque voluptatem et est. Similique ut beatae voluptatem eos quisquam et repellendus.', '1987-06-04', 0, 10, 1, 0, 5, 0, NULL, NULL, NULL),
(231, 'Katheryn Fisher IV', '52372384', 765.00, 75, 75, 'Aut voluptatem quo et ratione sint. Distinctio debitis provident enim earum maxime velit. Doloremque et in dicta quia facere.', '2015-09-09', 1, 8, 1, 0, 5, 0, NULL, NULL, NULL),
(232, 'Gretchen Hahn', '31916974', 437.00, 52, 52, 'Culpa quis odio consectetur consequuntur voluptas sit. Dolor corrupti accusamus id molestias sed rerum error. Iusto iusto odio molestiae eum omnis occaecati natus. Quae accusamus eum sunt est aut.', '2005-04-08', 1, 10, 1, 0, 4, 0, NULL, NULL, NULL),
(233, 'Marta Von Sr.', '72602294', 758.00, 84, 84, 'Est odit voluptatem cum commodi. Officiis voluptas aliquid nostrum sit consectetur. Consequatur dolorum facilis sapiente illum blanditiis facilis quia.', '1976-11-15', 1, 8, 1, 0, 3, 0, NULL, NULL, NULL),
(234, 'Mr. Johan Price II', '12626144', 406.00, 11, 11, 'Voluptas rerum necessitatibus impedit sed. Harum totam harum quidem soluta rerum tempore rerum facilis. Qui voluptatem id quae est. Reiciendis ducimus sunt officia.', '1986-05-05', 1, 6, 1, 0, 4, 0, NULL, NULL, NULL),
(235, 'Edgar Farrell', '52803949', 753.00, 21, 21, 'Odit consequatur quia dolores non est. Officia magnam neque porro eos nesciunt. Dignissimos consequatur sed accusamus est qui doloremque sed.', '1994-09-27', 1, 7, 1, 0, 5, 0, NULL, NULL, NULL),
(236, 'Everett Jast I', '63092967', 465.00, 35, 35, 'Vel adipisci ipsum necessitatibus ducimus et et qui. Occaecati expedita a neque odio distinctio fugit. Quam et ab et delectus harum eaque placeat. Omnis qui reprehenderit et aut.', '1983-07-05', 0, 4, 1, 0, 2, 0, NULL, NULL, NULL),
(237, 'Jaden Boyle', '11262329', 199.00, 22, 22, 'Est inventore cupiditate fuga et pariatur vel porro. Non possimus qui similique unde qui quia. Labore ipsum et beatae nostrum voluptas.', '1986-10-04', 0, 7, 1, 0, 2, 0, NULL, NULL, NULL),
(238, 'Mrs. Vivien Rosenbaum', '41127766', 15.00, 87, 87, 'Eligendi ullam rem aliquam eligendi modi molestiae. Fuga iure quidem perferendis et ut rerum. Maiores et enim quia in fugit amet reprehenderit est.', '2013-02-27', 0, 5, 1, 0, 3, 0, NULL, NULL, NULL),
(239, 'Herminia Davis', '26563886', 638.00, 72, 72, 'Exercitationem harum et velit non molestias. Aut aliquid explicabo consequatur nostrum id qui. Velit laboriosam est minus.', '2005-11-23', 1, 8, 1, 0, 1, 0, NULL, NULL, NULL),
(240, 'Mr. Kristofer Cassin Jr.', '30223707', 178.00, 92, 92, 'Sit sint impedit ipsa nemo nihil saepe. At sit voluptates similique enim et. Alias laudantium repellendus dicta aut veritatis magni.', '1977-06-06', 0, 7, 1, 0, 1, 0, NULL, NULL, NULL),
(241, 'Mrs. Loyce Ward', '80189473', 457.00, 75, 75, 'Incidunt beatae ea nobis doloribus aut explicabo. Eum mollitia soluta excepturi in ipsam. Rerum necessitatibus consequatur nisi ut. Vitae modi ipsum ut sed.', '2012-07-18', 0, 6, 1, 0, 2, 0, NULL, NULL, NULL),
(242, 'Lonie Muller V', '03982952', 544.00, 5, 5, 'Nam sint sit sit perferendis. Deleniti perferendis eos voluptatem dignissimos modi nulla ratione. Sit odit vitae nam et.', '2013-11-27', 1, 9, 1, 0, 4, 0, NULL, NULL, NULL),
(243, 'Sandy Aufderhar', '11163282', 638.00, 36, 36, 'Ut ea dolore esse sit facilis. Iure enim in odio laudantium et minus modi. Minus quibusdam sed a animi illo. Reprehenderit dicta sed velit optio accusantium.', '1980-08-21', 0, 10, 1, 0, 1, 0, NULL, NULL, NULL),
(244, 'Pedro Schimmel', '09782884', 921.00, 95, 95, 'Similique inventore consequuntur nam optio in ut eum. Placeat odio quam quam. Quae sit fuga numquam vero quidem.', '1994-11-08', 0, 1, 1, 0, 1, 0, NULL, NULL, NULL),
(245, 'Addie Kunde', '76654398', 503.00, 28, 28, 'Velit quam et soluta et a corporis. Voluptas laboriosam ut enim quaerat. Inventore at similique molestias molestias incidunt possimus voluptas. Beatae numquam vel et labore.', '1987-05-20', 0, 1, 1, 0, 4, 0, NULL, NULL, NULL),
(246, 'Lawrence Jacobi V', '76163555', 86.00, 11, 11, 'Rem rerum et nisi ipsa. Sunt sit deserunt laudantium velit unde. Quas eum eius molestias placeat ratione vero. Aut exercitationem asperiores nulla omnis.', '2004-10-07', 0, 8, 1, 0, 3, 0, NULL, NULL, NULL),
(247, 'Ezra Jakubowski', '59202257', 472.00, 74, 74, 'Inventore sit suscipit aspernatur adipisci amet ea. Possimus cumque omnis consequatur soluta. Veniam molestiae omnis dolor perspiciatis assumenda rerum ex non. Et dolores earum dolorem atque et et.', '2011-10-06', 1, 7, 1, 0, 2, 0, NULL, NULL, NULL),
(248, 'Johathan Jaskolski DVM', '26593814', 536.00, 65, 65, 'Qui praesentium consequatur et repellendus quo sit aliquam itaque. Quis voluptas beatae assumenda. Nesciunt omnis provident accusamus laborum earum.', '1995-06-09', 1, 2, 1, 0, 1, 0, NULL, NULL, NULL),
(249, 'Dr. Germaine Bednar', '86732697', 805.00, 15, 15, 'Temporibus magnam ad quia natus. Qui consequuntur sunt velit et corrupti asperiores. Recusandae soluta laborum et laboriosam quos ut. Est eius dolor culpa deleniti aliquam.', '2004-08-31', 0, 4, 1, 0, 2, 0, NULL, NULL, NULL),
(250, 'Judy Grant DDS', '48048644', 236.00, 2, 2, 'Temporibus quidem qui reprehenderit aut. Cum vero fugiat vel sunt. Enim nisi nobis aut ad sapiente vero. In eligendi ut temporibus culpa pariatur.', '1997-10-13', 1, 5, 1, 0, 2, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_categories`
--

CREATE TABLE `articles_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_article` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_subcategory` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `articles_categories`
--

INSERT INTO `articles_categories` (`id`, `id_article`, `id_category`, `id_subcategory`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 4, NULL, NULL),
(2, 2, 8, 4, NULL, NULL),
(3, 3, 8, 3, NULL, NULL),
(4, 4, 1, 2, NULL, NULL),
(5, 5, 8, 10, NULL, NULL),
(6, 6, 6, 1, NULL, NULL),
(7, 7, 4, 3, NULL, NULL),
(8, 8, 1, 5, NULL, NULL),
(9, 9, 3, 5, NULL, NULL),
(10, 10, 2, 7, NULL, NULL),
(11, 11, 8, 6, NULL, NULL),
(12, 12, 8, 1, NULL, NULL),
(13, 13, 7, 8, NULL, NULL),
(14, 14, 3, 9, NULL, NULL),
(15, 15, 10, 10, NULL, NULL),
(16, 16, 7, 7, NULL, NULL),
(17, 17, 6, 5, NULL, NULL),
(18, 18, 6, 6, NULL, NULL),
(19, 19, 6, 2, NULL, NULL),
(20, 20, 8, 1, NULL, NULL),
(21, 21, 8, 5, NULL, NULL),
(22, 22, 9, 4, NULL, NULL),
(23, 23, 4, 1, NULL, NULL),
(24, 24, 8, 10, NULL, NULL),
(25, 25, 1, 4, NULL, NULL),
(26, 26, 10, 1, NULL, NULL),
(27, 27, 1, 2, NULL, NULL),
(28, 28, 4, 2, NULL, NULL),
(29, 29, 2, 10, NULL, NULL),
(30, 30, 5, 4, NULL, NULL),
(31, 31, 5, 5, NULL, NULL),
(32, 32, 4, 4, NULL, NULL),
(33, 33, 8, 1, NULL, NULL),
(34, 34, 3, 2, NULL, NULL),
(35, 35, 3, 6, NULL, NULL),
(36, 36, 4, 4, NULL, NULL),
(37, 37, 7, 4, NULL, NULL),
(38, 38, 1, 1, NULL, NULL),
(39, 39, 3, 3, NULL, NULL),
(40, 40, 4, 9, NULL, NULL),
(41, 41, 7, 6, NULL, NULL),
(42, 42, 6, 8, NULL, NULL),
(43, 43, 2, 3, NULL, NULL),
(44, 44, 9, 6, NULL, NULL),
(45, 45, 5, 6, NULL, NULL),
(46, 46, 7, 3, NULL, NULL),
(47, 47, 8, 4, NULL, NULL),
(48, 48, 9, 4, NULL, NULL),
(49, 49, 9, 10, NULL, NULL),
(50, 50, 9, 1, NULL, NULL),
(51, 51, 9, 3, NULL, NULL),
(52, 52, 9, 10, NULL, NULL),
(53, 53, 5, 6, NULL, NULL),
(54, 54, 2, 1, NULL, NULL),
(55, 55, 1, 8, NULL, NULL),
(56, 56, 8, 1, NULL, NULL),
(57, 57, 2, 2, NULL, NULL),
(58, 58, 2, 10, NULL, NULL),
(59, 59, 2, 7, NULL, NULL),
(60, 60, 3, 5, NULL, NULL),
(61, 61, 7, 5, NULL, NULL),
(62, 62, 7, 5, NULL, NULL),
(63, 63, 10, 6, NULL, NULL),
(64, 64, 2, 8, NULL, NULL),
(65, 65, 8, 3, NULL, NULL),
(66, 66, 4, 5, NULL, NULL),
(67, 67, 3, 4, NULL, NULL),
(68, 68, 2, 10, NULL, NULL),
(69, 69, 2, 2, NULL, NULL),
(70, 70, 1, 5, NULL, NULL),
(71, 71, 7, 7, NULL, NULL),
(72, 72, 1, 7, NULL, NULL),
(73, 73, 6, 2, NULL, NULL),
(74, 74, 6, 5, NULL, NULL),
(75, 75, 1, 4, NULL, NULL),
(76, 76, 6, 6, NULL, NULL),
(77, 77, 9, 8, NULL, NULL),
(78, 78, 6, 2, NULL, NULL),
(79, 79, 1, 2, NULL, NULL),
(80, 80, 5, 6, NULL, NULL),
(81, 81, 6, 9, NULL, NULL),
(82, 82, 8, 2, NULL, NULL),
(83, 83, 6, 8, NULL, NULL),
(84, 84, 4, 4, NULL, NULL),
(85, 85, 7, 2, NULL, NULL),
(86, 86, 10, 4, NULL, NULL),
(87, 87, 3, 10, NULL, NULL),
(88, 88, 3, 7, NULL, NULL),
(89, 89, 9, 5, NULL, NULL),
(90, 90, 9, 4, NULL, NULL),
(91, 91, 8, 6, NULL, NULL),
(92, 92, 7, 2, NULL, NULL),
(93, 93, 8, 5, NULL, NULL),
(94, 94, 3, 1, NULL, NULL),
(95, 95, 1, 2, NULL, NULL),
(96, 96, 6, 8, NULL, NULL),
(97, 97, 3, 10, NULL, NULL),
(98, 98, 10, 3, NULL, NULL),
(99, 99, 8, 7, NULL, NULL),
(100, 100, 5, 1, NULL, NULL),
(101, 101, 2, 6, NULL, NULL),
(102, 102, 6, 5, NULL, NULL),
(103, 103, 7, 7, NULL, NULL),
(104, 104, 3, 10, NULL, NULL),
(105, 105, 10, 2, NULL, NULL),
(106, 106, 1, 7, NULL, NULL),
(107, 107, 5, 1, NULL, NULL),
(108, 108, 8, 2, NULL, NULL),
(109, 109, 7, 6, NULL, NULL),
(110, 110, 9, 5, NULL, NULL),
(111, 111, 9, 6, NULL, NULL),
(112, 112, 6, 3, NULL, NULL),
(113, 113, 9, 4, NULL, NULL),
(114, 114, 1, 7, NULL, NULL),
(115, 115, 9, 9, NULL, NULL),
(116, 116, 5, 3, NULL, NULL),
(117, 117, 5, 4, NULL, NULL),
(118, 118, 2, 1, NULL, NULL),
(119, 119, 9, 8, NULL, NULL),
(120, 120, 3, 8, NULL, NULL),
(121, 121, 10, 3, NULL, NULL),
(122, 122, 1, 4, NULL, NULL),
(123, 123, 10, 10, NULL, NULL),
(124, 124, 7, 5, NULL, NULL),
(125, 125, 4, 10, NULL, NULL),
(126, 126, 5, 3, NULL, NULL),
(127, 127, 7, 3, NULL, NULL),
(128, 128, 4, 3, NULL, NULL),
(129, 129, 4, 2, NULL, NULL),
(130, 130, 1, 6, NULL, NULL),
(131, 131, 8, 4, NULL, NULL),
(132, 132, 3, 8, NULL, NULL),
(133, 133, 6, 5, NULL, NULL),
(134, 134, 7, 10, NULL, NULL),
(135, 135, 1, 2, NULL, NULL),
(136, 136, 1, 9, NULL, NULL),
(137, 137, 9, 8, NULL, NULL),
(138, 138, 10, 4, NULL, NULL),
(139, 139, 3, 2, NULL, NULL),
(140, 140, 4, 2, NULL, NULL),
(141, 141, 10, 8, NULL, NULL),
(142, 142, 8, 4, NULL, NULL),
(143, 143, 3, 4, NULL, NULL),
(144, 144, 2, 5, NULL, NULL),
(145, 145, 7, 3, NULL, NULL),
(146, 146, 9, 8, NULL, NULL),
(147, 147, 6, 4, NULL, NULL),
(148, 148, 10, 7, NULL, NULL),
(149, 149, 7, 10, NULL, NULL),
(150, 150, 10, 3, NULL, NULL),
(151, 151, 1, 10, NULL, NULL),
(152, 152, 7, 3, NULL, NULL),
(153, 153, 7, 7, NULL, NULL),
(154, 154, 8, 8, NULL, NULL),
(155, 155, 6, 9, NULL, NULL),
(156, 156, 1, 3, NULL, NULL),
(157, 157, 5, 10, NULL, NULL),
(158, 158, 1, 9, NULL, NULL),
(159, 159, 7, 9, NULL, NULL),
(160, 160, 8, 2, NULL, NULL),
(161, 161, 9, 8, NULL, NULL),
(162, 162, 6, 4, NULL, NULL),
(163, 163, 3, 7, NULL, NULL),
(164, 164, 6, 9, NULL, NULL),
(165, 165, 6, 9, NULL, NULL),
(166, 166, 2, 10, NULL, NULL),
(167, 167, 10, 2, NULL, NULL),
(168, 168, 3, 2, NULL, NULL),
(169, 169, 3, 9, NULL, NULL),
(170, 170, 5, 6, NULL, NULL),
(171, 171, 7, 9, NULL, NULL),
(172, 172, 6, 7, NULL, NULL),
(173, 173, 9, 6, NULL, NULL),
(174, 174, 2, 7, NULL, NULL),
(175, 175, 1, 8, NULL, NULL),
(176, 176, 3, 3, NULL, NULL),
(177, 177, 10, 8, NULL, NULL),
(178, 178, 2, 3, NULL, NULL),
(179, 179, 8, 4, NULL, NULL),
(180, 180, 1, 6, NULL, NULL),
(181, 181, 4, 1, NULL, NULL),
(182, 182, 2, 1, NULL, NULL),
(183, 183, 8, 7, NULL, NULL),
(184, 184, 7, 4, NULL, NULL),
(185, 185, 8, 5, NULL, NULL),
(186, 186, 10, 7, NULL, NULL),
(187, 187, 7, 3, NULL, NULL),
(188, 188, 10, 5, NULL, NULL),
(189, 189, 1, 5, NULL, NULL),
(190, 190, 2, 10, NULL, NULL),
(191, 191, 3, 10, NULL, NULL),
(192, 192, 6, 7, NULL, NULL),
(193, 193, 9, 10, NULL, NULL),
(194, 194, 7, 7, NULL, NULL),
(195, 195, 3, 5, NULL, NULL),
(196, 196, 4, 6, NULL, NULL),
(197, 197, 1, 8, NULL, NULL),
(198, 198, 2, 5, NULL, NULL),
(199, 199, 5, 6, NULL, NULL),
(200, 200, 10, 10, NULL, NULL),
(201, 201, 2, 4, NULL, NULL),
(202, 202, 4, 2, NULL, NULL),
(203, 203, 6, 1, NULL, NULL),
(204, 204, 3, 7, NULL, NULL),
(205, 205, 2, 8, NULL, NULL),
(206, 206, 1, 6, NULL, NULL),
(207, 207, 4, 6, NULL, NULL),
(208, 208, 9, 5, NULL, NULL),
(209, 209, 8, 7, NULL, NULL),
(210, 210, 1, 4, NULL, NULL),
(211, 211, 6, 10, NULL, NULL),
(212, 212, 6, 10, NULL, NULL),
(213, 213, 6, 8, NULL, NULL),
(214, 214, 10, 3, NULL, NULL),
(215, 215, 10, 9, NULL, NULL),
(216, 216, 2, 10, NULL, NULL),
(217, 217, 5, 8, NULL, NULL),
(218, 218, 7, 1, NULL, NULL),
(219, 219, 3, 2, NULL, NULL),
(220, 220, 4, 3, NULL, NULL),
(221, 221, 2, 5, NULL, NULL),
(222, 222, 9, 6, NULL, NULL),
(223, 223, 3, 6, NULL, NULL),
(224, 224, 10, 6, NULL, NULL),
(225, 225, 5, 7, NULL, NULL),
(226, 226, 3, 5, NULL, NULL),
(227, 227, 8, 8, NULL, NULL),
(228, 228, 7, 7, NULL, NULL),
(229, 229, 10, 1, NULL, NULL),
(230, 230, 7, 5, NULL, NULL),
(231, 231, 4, 6, NULL, NULL),
(232, 232, 3, 4, NULL, NULL),
(233, 233, 6, 5, NULL, NULL),
(234, 234, 9, 4, NULL, NULL),
(235, 235, 10, 7, NULL, NULL),
(236, 236, 4, 3, NULL, NULL),
(237, 237, 1, 6, NULL, NULL),
(238, 238, 5, 10, NULL, NULL),
(239, 239, 2, 10, NULL, NULL),
(240, 240, 10, 2, NULL, NULL),
(241, 241, 10, 1, NULL, NULL),
(242, 242, 4, 9, NULL, NULL),
(243, 243, 8, 10, NULL, NULL),
(244, 244, 7, 9, NULL, NULL),
(245, 245, 3, 9, NULL, NULL),
(246, 246, 3, 1, NULL, NULL),
(247, 247, 7, 7, NULL, NULL),
(248, 248, 2, 1, NULL, NULL),
(249, 249, 7, 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_comments`
--

CREATE TABLE `articles_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_article` int(11) NOT NULL,
  `id_comment` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_index`
--

CREATE TABLE `articles_index` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_article` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `articles_index`
--

INSERT INTO `articles_index` (`id`, `id_article`, `position`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-10-03 23:49:06', '2018-10-03 23:49:06'),
(2, 2, 1, '2018-10-03 23:49:06', '2018-10-03 23:49:06'),
(3, 3, 1, '2018-10-03 23:49:06', '2018-10-03 23:49:06'),
(4, 4, 1, '2018-10-03 23:49:06', '2018-10-03 23:49:06'),
(5, 10, 1, '2018-10-03 23:49:06', '2018-10-03 23:49:06'),
(6, 11, 1, '2018-10-03 23:49:06', '2018-10-03 23:49:06'),
(7, 5, 1, '2018-10-03 23:49:06', '2018-10-03 23:49:06'),
(8, 12, 1, '2018-10-03 23:49:06', '2018-10-03 23:49:06'),
(9, 13, 1, '2018-10-03 23:49:06', '2018-10-03 23:49:06'),
(10, 6, 2, '2018-10-03 23:49:06', '2018-10-03 23:49:06'),
(11, 2, 3, '2018-10-03 23:49:06', '2018-10-03 23:49:06'),
(12, 3, 3, '2018-10-03 23:49:06', '2018-10-03 23:49:06'),
(13, 1, 3, '2018-10-03 23:49:06', '2018-10-03 23:49:06'),
(14, 9, 3, '2018-10-03 23:49:06', '2018-10-03 23:49:06'),
(15, 6, 3, '2018-10-03 23:49:06', '2018-10-03 23:49:06'),
(16, 11, 3, '2018-10-03 23:49:06', '2018-10-03 23:49:06'),
(17, 18, 3, '2018-10-03 23:49:06', '2018-10-03 23:49:06'),
(18, 20, 3, '2018-10-03 23:49:06', '2018-10-03 23:49:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_new`
--

CREATE TABLE `articles_new` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_article` int(11) NOT NULL,
  `new` tinyint(4) NOT NULL,
  `due_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_photos`
--

CREATE TABLE `articles_photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_article` int(11) NOT NULL,
  `id_photo` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_subcategories`
--

CREATE TABLE `articles_subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_article` int(11) NOT NULL,
  `id_subcategory` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `articles_subcategories`
--

INSERT INTO `articles_subcategories` (`id`, `id_article`, `id_subcategory`, `created_at`, `updated_at`) VALUES
(1, 1, 9, NULL, NULL),
(2, 2, 1, NULL, NULL),
(3, 3, 5, NULL, NULL),
(4, 4, 6, NULL, NULL),
(5, 5, 3, NULL, NULL),
(6, 6, 6, NULL, NULL),
(7, 7, 8, NULL, NULL),
(8, 8, 10, NULL, NULL),
(9, 9, 10, NULL, NULL),
(10, 10, 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bought_articles_user`
--

CREATE TABLE `bought_articles_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `active`, `delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Uriel Hane', 'Fugit pariatur distinctio vel nisi quo laboriosam eos. Quo aut eius quidem a facilis quo non. Autem occaecati facilis assumenda dolore et. Est possimus aut ullam et rerum.', 1, 0, NULL, NULL, NULL),
(2, 'Hermina Cummerata DDS', 'Odio animi vel rerum odio. Nostrum recusandae totam doloremque. Amet numquam quia optio nam dolor. Facilis ullam et quod.', 1, 0, NULL, NULL, NULL),
(3, 'Prof. Gerald Sanford II', 'Soluta porro hic velit et minima. Esse et et explicabo nisi. Voluptas cupiditate aliquid maxime adipisci ipsa sunt veniam. Consequatur velit temporibus et sit.', 1, 0, NULL, NULL, NULL),
(4, 'Fredrick Pfannerstill', 'Ut culpa cum qui architecto ut. Ut quis omnis qui dolores qui similique quo. Sequi ducimus quae mollitia sint tempore iste consequatur.', 1, 0, NULL, NULL, NULL),
(5, 'Emery Bergnaum', 'Libero consequatur officiis molestiae iusto consequuntur. Dignissimos saepe eligendi eum corrupti cupiditate error voluptas.', 1, 0, NULL, NULL, NULL),
(6, 'Phyllis West', 'Non ratione qui odio ex velit. Quisquam mollitia rerum omnis tenetur. Tempora eligendi facilis iure. Labore amet suscipit id et non dolore similique sed.', 1, 0, NULL, NULL, NULL),
(7, 'Wilma Dicki Sr.', 'Et itaque est voluptas eum corrupti est consectetur porro. Molestiae alias repellat praesentium nulla quis. Sint repudiandae voluptate voluptate suscipit corrupti fugiat aut enim.', 1, 0, NULL, NULL, NULL),
(8, 'Karlee Hahn', 'Vel et eos sed itaque. Quo doloremque soluta qui consectetur. Ipsam nulla dolor sit omnis nisi omnis sed accusamus. Et quia cumque rem pariatur quidem error.', 1, 0, NULL, NULL, NULL),
(9, 'Mrs. Annalise Tremblay', 'Magnam molestias facilis sunt. Perferendis distinctio vero earum occaecati voluptatum soluta sunt. Fugiat sapiente maxime cupiditate earum officiis reiciendis. Nemo id qui eaque natus et.', 1, 0, NULL, NULL, NULL),
(10, 'Kayleigh Klein', 'Esse non repellat adipisci laboriosam deserunt quam. Est voluptas amet enim ea. Illo dolorem voluptas maxime similique. Aperiam quis accusamus velit accusantium.', 1, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories_subcategories`
--

CREATE TABLE `categories_subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_subcategory` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categories_subcategories`
--

INSERT INTO `categories_subcategories` (`id`, `id_category`, `id_subcategory`, `created_at`, `updated_at`) VALUES
(1, 0, 9, NULL, NULL),
(2, 1, 7, NULL, NULL),
(3, 2, 9, NULL, NULL),
(4, 3, 1, NULL, NULL),
(5, 4, 5, NULL, NULL),
(6, 5, 9, NULL, NULL),
(7, 6, 4, NULL, NULL),
(8, 7, 8, NULL, NULL),
(9, 8, 8, NULL, NULL),
(10, 9, 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `commet` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `creditcard`
--

CREATE TABLE `creditcard` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `number` varchar(65) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_06_19_125311_create_users_table', 1),
(2, '2018_06_19_125401_create_articles_table', 1),
(3, '2018_06_19_125435_create_users_articles_table', 1),
(4, '2018_06_19_125503_create_categories_table', 1),
(5, '2018_06_19_125517_create_subcategories_table', 1),
(6, '2018_06_19_125543_create_categories_subcategories_table', 1),
(7, '2018_06_19_130341_create_comments_table', 1),
(8, '2018_06_19_130409_create_users_comments_table', 1),
(9, '2018_06_19_130442_create_articles_comments_table', 1),
(10, '2018_06_19_130520_create_transports_table', 1),
(11, '2018_06_19_135213_create_articles_categories_table', 1),
(12, '2018_06_19_135228_create_articles_subcategories_table', 1),
(13, '2018_06_19_150557_create_promotion_table', 1),
(14, '2018_06_19_150815_create_articles_photos_table', 1),
(15, '2018_06_19_152322_create_photos_table', 1),
(16, '2018_07_15_205733_create_shoppinglist_table', 1),
(17, '2018_07_15_210711_create_admins_table', 1),
(18, '2018_07_29_163654_create_articles_new_table', 1),
(19, '2018_08_21_151351_create_users_purchases_table', 1),
(20, '2018_08_21_151420_create_users_records_table', 1),
(21, '2018_09_02_141243_create_creditcard_table', 1),
(22, '2018_09_02_162311_create_bought_articles_user_table', 1),
(23, '2018_09_22_184850_create_orders_table', 1),
(24, '2018_10_03_135823_create_articles_index_table', 1),
(25, '2018_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordernumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'AA-0000-0000-0001',
  `id_user` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_article` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `photos`
--

INSERT INTO `photos` (`id`, `id_article`, `order`, `photo`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '5bb51f7958f511538596729.jpg', '2018-10-03 22:58:49', '2018-10-03 22:58:49'),
(2, 2, 1, '5bb51f7f068451538596735.jpg', '2018-10-03 22:58:55', '2018-10-03 22:58:55'),
(3, 3, 1, '5bb51f84e6b871538596740.jpg', '2018-10-03 22:59:01', '2018-10-03 22:59:01'),
(4, 4, 1, '5bb51f8b12a5e1538596747.jpg', '2018-10-03 22:59:07', '2018-10-03 22:59:07'),
(5, 5, 1, '5bb51f9652c521538596758.jpg', '2018-10-03 22:59:18', '2018-10-03 22:59:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promotions`
--

CREATE TABLE `promotions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `promotions`
--

INSERT INTO `promotions` (`id`, `name`, `description`, `active`, `delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '57', 'Repellendus deserunt enim aspernatur facilis sequi quo. Et quis blanditiis ab aperiam aliquam. Laboriosam sunt itaque ut sed. Eum in placeat ducimus officia nobis quaerat.', 1, 0, NULL, NULL, NULL),
(2, '28', 'Consequatur harum a sit animi esse qui eligendi. Debitis possimus velit fugiat vel qui officiis est. Ut placeat possimus quis ab voluptas. Necessitatibus facilis maiores provident recusandae vel.', 1, 0, NULL, NULL, NULL),
(3, '10', 'Minus aspernatur veritatis eos nesciunt. Est minima sed maiores a explicabo. Rerum quam non fugiat molestiae placeat.', 1, 0, NULL, NULL, NULL),
(4, '58', 'Unde et aut sapiente quia. Corrupti autem odio dolorem corrupti hic sit qui quidem. Et et recusandae officia aut reiciendis quaerat.', 1, 0, NULL, NULL, NULL),
(5, '53', 'Officiis voluptas nostrum aut eaque. Dolorem maiores illo ad voluptatem. Sit voluptatibus sequi tenetur alias est inventore quibusdam eum.', 1, 0, NULL, NULL, NULL),
(6, '48', 'Praesentium quae ullam corrupti nesciunt. Nemo rerum ab ex laudantium unde. Sunt consequatur esse optio quia incidunt. Blanditiis est vero sint incidunt.', 1, 0, NULL, NULL, NULL),
(7, '28', 'Aperiam unde et tenetur eos animi. Eum sed ipsum deleniti voluptas repellendus sunt debitis. Esse excepturi alias molestiae quis distinctio.', 1, 0, NULL, NULL, NULL),
(8, '53', 'Omnis quibusdam animi nobis itaque neque. Sit dolor nisi quos. Impedit deleniti sint et iusto quasi. Et quaerat quidem ipsum modi omnis voluptas.', 1, 0, NULL, NULL, NULL),
(9, '58', 'Molestiae error ut voluptatem blanditiis quos quia sit. Omnis perspiciatis enim natus et quo accusamus. Saepe pariatur dolores voluptates placeat accusantium quis.', 1, 0, NULL, NULL, NULL),
(10, '28', 'Sequi ab et veniam. Et blanditiis deserunt sequi magnam porro. Aut quos voluptates aut hic odio.', 1, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shoppinglist`
--

CREATE TABLE `shoppinglist` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_article` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `shoppinglist`
--

INSERT INTO `shoppinglist` (`id`, `id_article`, `id_user`, `quantity`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 2, '2018-10-03 21:42:29', '2018-10-03 21:42:29'),
(2, 6, 1, 4, '2018-10-03 21:42:29', '2018-10-03 21:42:29'),
(3, 9, 1, 6, '2018-10-03 21:42:29', '2018-10-03 21:42:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `subcategories`
--

INSERT INTO `subcategories` (`id`, `name`, `description`, `active`, `delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Randall Goldner', 'Officiis pariatur ut minima voluptates possimus deleniti possimus. Amet dolorum deserunt enim aut repellat cum eum. In similique in dolore architecto culpa et.', 1, 0, NULL, NULL, NULL),
(2, 'Edmond Schowalter', 'Dolorem voluptates officia sunt qui modi rerum culpa. Eligendi dolores consequatur temporibus deserunt et molestias. Corrupti illum harum cum nostrum molestias.', 1, 0, NULL, NULL, NULL),
(3, 'Karson Ebert', 'Exercitationem accusamus illum aliquid et unde voluptatem vel. Nobis voluptatem molestiae impedit. Rem quia sit aperiam dolor consequatur veritatis dolorum.', 1, 0, NULL, NULL, NULL),
(4, 'Ms. Amelia Schoen', 'Eum quae laboriosam nihil iste. Est aut enim dicta minus porro labore. Et et cupiditate sint sequi at harum qui. Beatae quasi exercitationem natus quia quis nam ipsum culpa.', 1, 0, NULL, NULL, NULL),
(5, 'Isai Bogan', 'Consequatur est quibusdam eligendi adipisci velit. Voluptatem fuga qui qui voluptatem est asperiores illo. Natus quia explicabo quis. Quam quia sunt iusto cumque.', 1, 0, NULL, NULL, NULL),
(6, 'Ms. Fae Purdy', 'Et voluptas et debitis incidunt vel libero qui nostrum. Vero incidunt laboriosam aut debitis et dolorem. Accusamus dolores sed dolore accusantium ab quae.', 1, 0, NULL, NULL, NULL),
(7, 'Katharina Kiehn', 'Aliquam et officia minima nobis molestias quod. Debitis officia id eaque.', 1, 0, NULL, NULL, NULL),
(8, 'Mossie Mills', 'Magni libero rerum voluptatem ratione ipsum. Blanditiis quasi sit repudiandae placeat aliquam eius. Omnis quisquam et dolore non. Sed et fuga in quaerat ducimus adipisci.', 1, 0, NULL, NULL, NULL),
(9, 'Devyn Howell', 'Doloremque expedita sed repellendus. Omnis ex dolorem mollitia voluptate et quisquam voluptates. Asperiores ipsam omnis sit et adipisci quo sapiente.', 1, 0, NULL, NULL, NULL),
(10, 'Mr. Blaise Torp Jr.', 'Perferendis sunt magnam unde aut omnis aut dicta. Sed eius ut nisi suscipit. Voluptate eum consequuntur voluptates reprehenderit qui sit voluptates.', 1, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transports`
--

CREATE TABLE `transports` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dni` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `delete` tinyint(4) NOT NULL DEFAULT '0',
  `hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `lastname`, `username`, `email`, `password`, `dni`, `photo`, `address`, `zip_code`, `birthday`, `active`, `delete`, `hash`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Alejandro', 'Jimenez', 'Albattle', 'alejandrojimenezaajr@gmail.com', '$2y$10$PVO9MagmbZkM3UHxjURnveHHnVHub65cLtrIIBoaEY5S3sF32bMDC', '95756293', NULL, 'Elias bedoya 160 4A', '1408', NULL, 1, 0, '5bb50d9430aad', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_articles`
--

CREATE TABLE `users_articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_article` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_comments`
--

CREATE TABLE `users_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_comment` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_purchases`
--

CREATE TABLE `users_purchases` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_article` int(11) NOT NULL,
  `ordernumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'AA-0000-0000-0001',
  `count` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_records`
--

CREATE TABLE `users_records` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_article` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users_records`
--

INSERT INTO `users_records` (`id`, `id_user`, `id_article`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '2018-10-03 21:42:29', '2018-10-03 21:42:29'),
(2, 1, 1, '2018-10-03 21:42:29', '2018-10-03 21:42:29'),
(3, 1, 2, '2018-10-03 21:42:29', '2018-10-03 21:42:29'),
(4, 1, 3, '2018-10-03 21:42:29', '2018-10-03 21:42:29'),
(5, 1, 4, '2018-10-03 21:42:29', '2018-10-03 21:42:29'),
(6, 1, 5, '2018-10-03 21:42:29', '2018-10-03 21:42:29'),
(7, 1, 6, '2018-10-03 21:42:29', '2018-10-03 21:42:29'),
(8, 1, 7, '2018-10-03 21:42:29', '2018-10-03 21:42:29'),
(9, 1, 8, '2018-10-03 21:42:29', '2018-10-03 21:42:29'),
(10, 1, 9, '2018-10-03 21:42:29', '2018-10-03 21:42:29'),
(11, 1, 10, '2018-10-03 21:42:29', '2018-10-03 21:42:29'),
(12, 1, 11, '2018-10-03 21:42:29', '2018-10-03 21:42:29'),
(13, 1, 12, '2018-10-03 21:42:29', '2018-10-03 21:42:29'),
(14, 1, 13, '2018-10-03 21:42:29', '2018-10-03 21:42:29'),
(15, 1, 14, '2018-10-03 21:42:29', '2018-10-03 21:42:29');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_username_unique` (`username`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indices de la tabla `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `articles_categories`
--
ALTER TABLE `articles_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `articles_comments`
--
ALTER TABLE `articles_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `articles_index`
--
ALTER TABLE `articles_index`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `articles_new`
--
ALTER TABLE `articles_new`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `articles_photos`
--
ALTER TABLE `articles_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `articles_subcategories`
--
ALTER TABLE `articles_subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `bought_articles_user`
--
ALTER TABLE `bought_articles_user`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categories_subcategories`
--
ALTER TABLE `categories_subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `creditcard`
--
ALTER TABLE `creditcard`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `shoppinglist`
--
ALTER TABLE `shoppinglist`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `transports`
--
ALTER TABLE `transports`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `users_articles`
--
ALTER TABLE `users_articles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_comments`
--
ALTER TABLE `users_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_purchases`
--
ALTER TABLE `users_purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_records`
--
ALTER TABLE `users_records`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;

--
-- AUTO_INCREMENT de la tabla `articles_categories`
--
ALTER TABLE `articles_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;

--
-- AUTO_INCREMENT de la tabla `articles_comments`
--
ALTER TABLE `articles_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `articles_index`
--
ALTER TABLE `articles_index`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `articles_new`
--
ALTER TABLE `articles_new`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `articles_photos`
--
ALTER TABLE `articles_photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `articles_subcategories`
--
ALTER TABLE `articles_subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `bought_articles_user`
--
ALTER TABLE `bought_articles_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `categories_subcategories`
--
ALTER TABLE `categories_subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `creditcard`
--
ALTER TABLE `creditcard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `promotions`
--
ALTER TABLE `promotions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `shoppinglist`
--
ALTER TABLE `shoppinglist`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `transports`
--
ALTER TABLE `transports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users_articles`
--
ALTER TABLE `users_articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users_comments`
--
ALTER TABLE `users_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users_purchases`
--
ALTER TABLE `users_purchases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users_records`
--
ALTER TABLE `users_records`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
